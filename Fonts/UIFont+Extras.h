//
//  UIFont+Extras.h
//  BarX
//
//  Created by Roman Khan on 27/02/16.
//  Copyright © 2017 Zappe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Extras)

+ (UIFont *)setFontWithType:(NSString *)type withSize:(CGFloat)fontSize;

@end
