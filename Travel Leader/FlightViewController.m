//
//  FlightViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "FlightViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HotelViewController.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"

#import "DemoMessagesViewController.h"

@interface FlightViewController ()
{
    UIScrollView *bgScrollView;
    NSArray * detailarr;
    NSString *lattitude;
    NSString *longitude;
    
    
}
@property (weak, nonatomic) IBOutlet UIView *passangerview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barbutton;
@property (weak, nonatomic) IBOutlet UILabel *departdateLB;
@property (weak, nonatomic) IBOutlet UILabel *arrivedateLB;
@property (weak, nonatomic) IBOutlet UILabel *departcityLB;
@property (weak, nonatomic) IBOutlet UILabel *arrivecityLB;
@property (weak, nonatomic) IBOutlet UILabel *departtimeLB;
@property (weak, nonatomic) IBOutlet UILabel *arrivetimeLB;
@property (weak, nonatomic) IBOutlet UILabel *flightnameLB;
@property (weak, nonatomic) IBOutlet UILabel *terminalLB;
@property (weak, nonatomic) IBOutlet UILabel *flightnoLB;
@property (weak, nonatomic) IBOutlet UILabel *seatnoLB;
@property (weak, nonatomic) IBOutlet UILabel *confirmationnoLB;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *gatelbl;





@end

@implementation FlightViewController
@synthesize departdateLB,arrivedateLB,departcityLB,arrivecityLB,departtimeLB,arrivetimeLB,flightnameLB,terminalLB,flightnoLB,seatnoLB,confirmationnoLB,headerImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    detailarr = [[NSArray alloc]init];
    //  _imageview.image = headerImage;
    
    
    
    NSDictionary *dataDic ;
    
    for (NSDictionary *dictsub in [[TravelConstantClass singleton].TripArray valueForKey:@"travelsegment"])
    {
        if ([dictsub valueForKey:@"airseg"]!= nil)
            
        {
            
            dataDic = [dictsub valueForKey:@"airseg"];
            
            
        }
        
        
    }
    
    NSString *addressStr;
    if (dataDic.allKeys!=nil) {
        
        
        if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSArray class]])
        {
            NSArray *array = [dataDic valueForKey:@"fromcity"];
            addressStr = [array lastObject];
        }
        
        if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSString class]]) {
            
            NSString *cityStr = [dataDic valueForKey:@"fromcity"];
            addressStr = cityStr;
        }
        
    }
    
    NSString *strSpaceRemove = [addressStr stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
    
    [self googleApiFor:strSpaceRemove];
    [self loadMainImage:headerImage];
    
    [self setupLeftMenuButton];
    // [self FlightDetail];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 07, 07)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
    //    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //    textButton.title = @"";
    //    // textButton.tintColor = [UIColor whiteColor];
    //    textButton.tintColor = [UIColor blackColor];
    //    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
    //
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"FLIGHT INFO."
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
    
    //  self.navigationItem.leftBarButtonItem = @[searchItem];
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}





-(void)backAction
{
    
    [self.navigationController popViewControllerAnimated:YES];
}





//  self.navigationItem.leftBarButtonItem = @[searchItem];

-(void)backButtonTapp:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}







- (void)loadMainImage : (UIImage *)bannerImage {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    // main scrollview draw
    bgScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 44, screenWidth, screenHeight -44) ];
    bgScrollView.backgroundColor = [UIColor clearColor];
    
    // imageview draw & load image
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 220) ];
    imageview.backgroundColor = [UIColor clearColor];
    if (bannerImage) {
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = bannerImage;
    }
    
    bgScrollView.delegate =self;
    [ bgScrollView addSubview:imageview];
    
    UILabel *grayBgInformationlbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 220, screenWidth, 40)];
    grayBgInformationlbl.backgroundColor = [UIColor lightGrayColor];
    grayBgInformationlbl.textColor = [UIColor blackColor];
    grayBgInformationlbl.font = [UIFont fontWithName:@"Lato-Regular" size:16];
    grayBgInformationlbl.text = @" YOUR                 INFORMATION";
    [bgScrollView addSubview:grayBgInformationlbl ];
    
    
    UIImageView *typeOfInformation_imageview = [[UIImageView alloc]initWithFrame:CGRectMake(51, -10, 60, 60) ];
    typeOfInformation_imageview.backgroundColor = [UIColor clearColor];
    typeOfInformation_imageview.image = [UIImage imageNamed:@"flight_big.png"];
    [grayBgInformationlbl addSubview:typeOfInformation_imageview];
    
    
    
    [self.view addSubview:bgScrollView];
    bgScrollView.showsVerticalScrollIndicator = NO;
    
    if( 667 ==  screenHeight ) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 150);
        
    }
    
    else if (736 ==  screenHeight) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 100);
        
    }
    
    else if (812 ==  screenHeight) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight -25);
        
        
    }
    
    else {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight -40);
        
        
    }
    
    
    
    detailarr = [TravelConstantClass singleton].TripArray;
    
    
    if (detailarr) {
        
        [self drawViewForHotelSegment:detailarr];
        
        
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenMedianheight = screenRect.size.height/2;
    
    
    
    if (scrollView.contentOffset.y < 0 ) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    }
    
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        
        
    }
    
    
}

-(void)drawViewForHotelSegment :(NSArray *)dataArray{
    
    
    
    NSDictionary *dataDic ;
    
    for (NSDictionary *dictsub in [dataArray valueForKey:@"travelsegment"])
    {
        if ([dictsub valueForKey:@"airseg"]!= nil)
            
        {
            
            dataDic = [dictsub valueForKey:@"airseg"];
            
            
        }
    }
    
    
    
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGFloat screenMedianWidth = screenRect.size.width/2;
    
    // gray verticalLine draw
    
    UILabel *graylineVerticalLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth, 260, 1, 150)];
    graylineVerticalLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineVerticalLbl ];
    
    // iamge for arrow
    UIImageView *aarowImage = [[UIImageView alloc]initWithFrame:CGRectMake(-15, 60, 30, 30) ];
    aarowImage.backgroundColor = [UIColor clearColor];
    aarowImage.image = [UIImage imageNamed:@"black_arrow_right.png"];
    [graylineVerticalLbl addSubview:aarowImage];
    
    
    
    // grayhorizontal Line draw
    UILabel *graylineHorizontalLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 410, screenWidth, 1)];
    graylineHorizontalLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineHorizontalLbl ];
    
    
    // chechin label
    
    UILabel *checkInLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 270, 150, 30)];
    checkInLbl.textAlignment = NSTextAlignmentCenter;
    //grayBgInformationlbl.backgroundColor = [UIColor lightGrayColor];
    checkInLbl.textColor = [UIColor blackColor];
    checkInLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    checkInLbl.text = @"DEPART";
    [bgScrollView addSubview:checkInLbl];
    
    // chechOUT label
    UILabel *checkOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 270, 150, 30)];
    checkOutLbl.textAlignment = NSTextAlignmentCenter;
    checkOutLbl.textColor = [UIColor blackColor];
    checkOutLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    checkOutLbl.text = @"ARRIVE";
    [bgScrollView addSubview:checkOutLbl];
    
    
    
    
    UILabel *checkInTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 , 310, 150, 30)];
    checkInTimeLbl.textAlignment = NSTextAlignmentCenter;
    checkInTimeLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    checkInTimeLbl.font = [UIFont fontWithName:@"Lato-Bold" size:22];
    checkInTimeLbl.text = [dataDic valueForKey:@"departuretime"] ;
    [bgScrollView addSubview:checkInTimeLbl];
    
    UILabel *checkOutTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 310, 150, 30)];
    checkOutTimeLbl.textAlignment = NSTextAlignmentCenter;
    checkOutTimeLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    checkOutTimeLbl.font = [UIFont fontWithName:@"Lato-Bold" size:22];
    
    checkOutTimeLbl.text =  [dataDic valueForKey:@"arrivaltime"];
    [bgScrollView addSubview:checkOutTimeLbl];
    
    
    
    UILabel *citycheckInLbl = [[UILabel alloc]initWithFrame:CGRectMake(10 , 335, 170, 30)];
    citycheckInLbl.textAlignment = NSTextAlignmentCenter;
    citycheckInLbl.textColor = [UIColor blackColor];
    citycheckInLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSArray class]])
    {
        NSArray *array = [dataDic valueForKey:@"fromcity"];
        citycheckInLbl.text = [array lastObject];
    }
    
    if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSString class]]) {
        
        NSString *cityStr = [dataDic valueForKey:@"fromcity"];
        citycheckInLbl.text = cityStr;
    }
    
    
    
    [bgScrollView addSubview:citycheckInLbl];
    
    UILabel *cityCheckOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(10 + screenMedianWidth, 335, 170, 30)];
    cityCheckOutLbl.textAlignment = NSTextAlignmentCenter;
    cityCheckOutLbl.textColor = [UIColor blackColor];
    cityCheckOutLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    if ([[dataDic valueForKey:@"tocity"] isKindOfClass:[NSArray class]])
    {
        NSArray *array = [dataDic valueForKey:@"tocity"];
        cityCheckOutLbl.text = [array lastObject];
    }
    
    if ([[dataDic valueForKey:@"tocity"] isKindOfClass:[NSString class]]) {
        
        NSString *cityStr = [dataDic valueForKey:@"tocity"];
        cityCheckOutLbl.text = cityStr;
    }
    
    [bgScrollView addSubview:cityCheckOutLbl];
    
    
    UILabel *datecheckInLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 , 360, 150, 30)];
    datecheckInLbl.textAlignment = NSTextAlignmentCenter;
    datecheckInLbl.textColor = [UIColor blackColor];
    datecheckInLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    
    NSDateFormatter *dateForm = [[NSDateFormatter alloc] init];
    [dateForm setDateFormat: @"EEEE, MM/dd/yyyy"];
    NSDate *datecheck = [dateForm dateFromString: [dataDic valueForKey:@"departuredate"]];
    //Second Conversion
    [dateForm setDateFormat: @"MMMM dd, yyyy"];
    NSString *finalDate = [dateForm stringFromDate:datecheck];
    
    datecheckInLbl.text = finalDate;
    
    
    //datecheckInLbl.text = [dataDic valueForKey:@"stdt"];
    
    [bgScrollView addSubview:datecheckInLbl];
    
    UILabel *dateCheckOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 360, 150, 30)];
    dateCheckOutLbl.textAlignment = NSTextAlignmentCenter;
    dateCheckOutLbl.textColor = [UIColor blackColor];
    dateCheckOutLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    NSDateFormatter *dateForm1 = [[NSDateFormatter alloc] init];
    [dateForm1 setDateFormat: @"EEEE, MM/dd/yyyy"];
    NSDate *datecheck1 = [dateForm1 dateFromString: [dataDic valueForKey:@"arrivaldate"]];
    //Second Conversion
    [dateForm1 setDateFormat: @"MMMM dd, yyyy"];
    NSString *finalDateForcheckout = [dateForm1 stringFromDate:datecheck1];
    
    
    dateCheckOutLbl.text = finalDateForcheckout ;
    
    [bgScrollView addSubview:dateCheckOutLbl];
    
    
    UILabel *hotelLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth - 75 , 445, 150, 30)];
    hotelLbl.textAlignment = NSTextAlignmentCenter;
    hotelLbl.textColor = [UIColor blackColor];
    hotelLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    hotelLbl.text =  [dataDic  valueForKey:@"airline"];
    [bgScrollView addSubview:hotelLbl];
    
    UILabel *hotelNameLbl = [[UILabel alloc]initWithFrame:CGRectMake( screenMedianWidth - 75, 475, 150, 30)];
    hotelNameLbl.textAlignment = NSTextAlignmentCenter;
    hotelNameLbl.lineBreakMode = NSLineBreakByWordWrapping;
    hotelNameLbl.numberOfLines = 6;
    hotelNameLbl.textColor = [UIColor blackColor];
    hotelNameLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    NSString *stringAddress = [dataDic  valueForKey:@"departingterminal"];
    hotelNameLbl.text = stringAddress;
    [bgScrollView addSubview:hotelNameLbl];
    
    
    
    UILabel *graylineHorizontalAfterHotelLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 540, screenWidth, 1)];
    graylineHorizontalAfterHotelLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineHorizontalAfterHotelLbl ];
    
    
    UIButton *uberButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [uberButton addTarget:self
                   action:@selector(uberBtnClick:)
         forControlEvents:UIControlEventTouchUpInside];
    [uberButton setTitle:@"" forState:UIControlStateNormal];
    UIImage *btnImage = [UIImage imageNamed:@"uber_ios.png"];
    [uberButton setImage:btnImage forState:UIControlStateNormal];
    
    uberButton.frame = CGRectMake(15, 570, 200, 55);
    [bgScrollView addSubview:uberButton];
    
    
    UIButton *lyftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [lyftButton addTarget:self
                   action:@selector(lyftBtnClick:)
         forControlEvents:UIControlEventTouchUpInside];
    [lyftButton setTitle:@"" forState:UIControlStateNormal];
    
    UIImage *Image = [UIImage imageNamed:@"lyft_ios.png"];
    [lyftButton setImage:Image forState:UIControlStateNormal];
    lyftButton.frame = CGRectMake(screenMedianWidth -10 , 570, 200, 55);
    [bgScrollView addSubview:lyftButton];
    
    
    
    UILabel *graylineHorizontalAfterUberLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 655, screenWidth, 1)];
    graylineHorizontalAfterUberLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineHorizontalAfterUberLbl ];
    
    
    UILabel *roomTypeLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth - 50 , 670, 100, 30)];
    roomTypeLbl.textAlignment = NSTextAlignmentCenter;
    roomTypeLbl.textColor = [UIColor blackColor];
    roomTypeLbl.font = [UIFont fontWithName:@"Lato-Regular" size:18];
    roomTypeLbl.text = @"Flight no.";
    [bgScrollView addSubview:roomTypeLbl];
    
    
    UILabel *roomType = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth - 50 , 700, 100, 30)];
    roomType.textAlignment = NSTextAlignmentCenter;
    roomType.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    roomType.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    roomType.text = [dataDic valueForKey:@"flightnumber"];
    [bgScrollView addSubview:roomType];
    
    
    
    UILabel *gateLbl = [[UILabel alloc]initWithFrame:CGRectMake(35 , 670, 100, 30)];
    gateLbl.textAlignment = NSTextAlignmentLeft;
    gateLbl.textColor = [UIColor blackColor];
    gateLbl.font = [UIFont fontWithName:@"Lato-Regular" size:18];
    gateLbl.text = @"Gate";
    [bgScrollView addSubview:gateLbl];
    
    
    UILabel *gateNo = [[UILabel alloc]initWithFrame:CGRectMake(35 , 700, 100, 30)];
    gateNo.textAlignment = NSTextAlignmentLeft;
    gateNo.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    gateNo.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *flight = [prefs objectForKey:@"flightNo"];
    
    if (flight.length != 0) {
        
        if ([[dataDic valueForKey:@"flightnumber"] isEqualToString:flight]) {
            
            NSString *gate = [prefs objectForKey:@"currentGate"];
            gateNo.text = gate;
            
        }
        
        
    }
    
    [bgScrollView addSubview:gateNo];
    
    
    UILabel *seatLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth +40 , 670, 100, 30)];
    seatLbl.textAlignment = NSTextAlignmentRight;
    seatLbl.textColor = [UIColor blackColor];
    seatLbl.font = [UIFont fontWithName:@"Lato-Regular" size:18];
    seatLbl.text = @"Seat";
    [bgScrollView addSubview:seatLbl];
    
    
    UILabel *seatNo = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth + 40 , 700, 100, 30)];
    seatNo.textAlignment = NSTextAlignmentRight;
    seatNo.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    seatNo.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    
    
    if ([dataDic valueForKey:@"seats"])  {
        
        if ([[[dataDic valueForKey:@"seats"] valueForKey:@"seat"]  isKindOfClass:[NSArray class]]) {
            
            NSArray *dataArray = [[[dataDic valueForKey:@"seats"] valueForKey:@"seat"] valueForKey:@"seat"];
            
            NSString *seatStr = [dataArray componentsJoinedByString:@","];
            
            seatNo.text = seatStr;
            
            
        }
        
        if  ([[[dataDic valueForKey:@"seats"] valueForKey:@"seat"] isKindOfClass:[NSDictionary class]])  {
            
            seatNo.text = [[[dataDic valueForKey:@"seats"] valueForKey:@"seat"] valueForKey:@"seat"];
            
        }
        
    }
    
    [bgScrollView addSubview:seatNo];
    
    
    
    
    UILabel *orangeBglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 750, screenWidth, 70)];
    orangeBglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    [bgScrollView addSubview:orangeBglbl ];
    
    UIImageView *checkImage = [[UIImageView alloc]initWithFrame:CGRectMake(50, 15, 30, 30) ];
    checkImage.backgroundColor = [UIColor clearColor];
    checkImage.image = [UIImage imageNamed:@"check.png"];
    [orangeBglbl addSubview:checkImage];
    
    
    UILabel *confirmlbl = [[UILabel alloc]initWithFrame:CGRectMake(85 , 15, 120, 30)];
    confirmlbl.textAlignment = NSTextAlignmentLeft;
    confirmlbl.textColor = [UIColor whiteColor];
    confirmlbl.font = [UIFont fontWithName:@"Lato-Bold" size:20];
    confirmlbl.text = @"Confirmation";
    [orangeBglbl addSubview:confirmlbl];
    
    
    UILabel *confirmNo = [[UILabel alloc]initWithFrame:CGRectMake(205 , 15, 140, 30)];
    confirmNo.textAlignment = NSTextAlignmentLeft;
    confirmNo.textColor = [UIColor whiteColor];
    confirmNo.font = [UIFont fontWithName:@"Lato-Bold" size:20];
    confirmNo.text = [NSString stringWithFormat:@"#%@",[dataDic valueForKey:@"confirmation"]];
    
    [orangeBglbl addSubview:confirmNo];
    
    
}


//uber://?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL&action=setPickup&pickup=my_location&dropoff[nickname]=MDW&dropoff[formatted_address]=Chicago%2FMidway%2C+IL&link_text=Chat%20with%20your%20Travel%20Agent%21&partner_deeplink=tl24%3A%2F%2Fchat


-(IBAction)uberBtnClick: (UIButton *)sender
{
    
    NSString *dataThatWasPassed = sender.titleLabel.text;
    NSString *newStr = [dataThatWasPassed stringByReplacingOccurrencesOfString:@" "
                                                                    withString:@""];
    NSDictionary *dataDic ;
    
    for (NSDictionary *dictsub in [[TravelConstantClass singleton].TripArray valueForKey:@"travelsegment"])
    {
        if ([dictsub valueForKey:@"airseg"]!= nil)
            
        {
            
            dataDic = [dictsub valueForKey:@"airseg"];
            
            
        }
        
        
    }
    
    NSString *addressStr;
    if (dataDic.allKeys!=nil) {
        
        
        if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSArray class]])
        {
            NSArray *array = [dataDic valueForKey:@"fromcity"];
            addressStr = [array lastObject];
        }
        
        if ([[dataDic valueForKey:@"fromcity"] isKindOfClass:[NSString class]]) {
            
            NSString *cityStr = [dataDic valueForKey:@"fromcity"];
            addressStr = cityStr;
        }
        
    }
    NSString *finalStr = [NSString stringWithFormat:@"%@%@",[dataDic valueForKey:@"frcty"],addressStr];
    
    NSString *strSpaceRemove = [finalStr stringByReplacingOccurrencesOfString:@" "
                                                                   withString:@""];
    
    //uber://?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL&action=setPickup&pickup=my_location&dropoff[nickname]=MDW&dropoff[formatted_address]=Chicago%2FMidway%2C+IL&link_text=Chat%20with%20your%20Travel%20Agent%21&partner_deeplink=tl24%3A%2F%2Fchat
    
    
    NSString *str = [NSString stringWithFormat:@"uber://?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL&action=setPickup&pickup=my_location&pickup[nickname]=UberHQ&pickup[formatted_address]=Newdelhi&dropoff[latitude]=%@&dropoff[longitude]=%@f&dropoff[nickname]=%@&dropoff[formatted_address]=%@&link_text=Chat%20with%20your%20Travel%20Agent%21&partner_deeplink=tl24%3A%2F%2Fchat",lattitude,longitude,[dataDic valueForKey:@"frcty"],strSpaceRemove];
    
    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]])
        
        
        
    {
        NSLog(@"uber");
    }
    else
    {
        // NSString *str = [NSString stringWithFormat:@"https://m.uber.com/ul/?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL&pickup[formatted_address]=%@",newStr];
        
        NSString *str = [NSString stringWithFormat:@"https://m.uber.com/ul/?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL&action=setPickup&pickup=my_location&pickup[nickname]=UberHQ&pickup[formatted_address]=Newdelhi&dropoff[latitude]=%@&dropoff[longitude]=%@&dropoff[nickname]=%@&dropoff[formatted_address]=%@&link_text=ChatwithyourTravelAgent&partner_deeplink=tl24chat",lattitude,longitude,[dataDic valueForKey:@"frcty"],strSpaceRemove];
        
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str ]];
    }
    
}


- (void)googleApiFor: (NSString *)addressStr{
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyD-R32AOcgOwNjY72WqDUGhWR8i9SJOnwM",addressStr ];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError *erro = nil;
        
        NSString *placeID ;
        if (data!=nil) {
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            
            if (json.count > 0) {
                
                NSLog(@"resultdata == %@",[[[json valueForKey:@"results"]lastObject]valueForKey:@"geometry"]);
                
                if([[[[json valueForKey:@"results"]lastObject]valueForKey:@"geometry"]valueForKey:@"location" ]) {
                    
                    lattitude = [[[[[json valueForKey:@"results"]lastObject]valueForKey:@"geometry"]valueForKey:@"location" ] valueForKey:@"lat"];
                    longitude = [[[[[json valueForKey:@"results"]lastObject]valueForKey:@"geometry"]valueForKey:@"location" ] valueForKey:@"lng"];
                    
                    
                }
                
            }
            
            
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            //  [self loadFirstPhotoForPlace:placeID];
            
            
        });
    }];
    
    [data resume];
    
    
}

//- (IBAction)uberBtnClick: (id)sender
//{
//    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"uber://?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL"] ])
//    {
//        NSLog(@"uber");
//    }
//    else
//    {
//
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://m.uber.com/ul/?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL"] options:@{} completionHandler:nil];
//    }
//
//}

//- (IBAction)lyftBtnClick: (id)sender
//{
//    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"lyft://?partner=14Tc17vjnLwW"] ])
//    {
//        NSLog(@"uber");
//    }
//    else
//    {
//
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.lyft.com/signup/SDKSIGNUP?clientId=14Tc17vjnLwW"] options:@{} completionHandler:nil];
//    }
//
//}
//



- (IBAction)lyftBtnClick: (id)sender
{
    
    NSString *urlstr = [NSString stringWithFormat:@"lyft://?partner=14Tc17vjnLwW&destination[longitude]=%@&destination[latitude]=%@",longitude,lattitude];
    NSString *newStr = [urlstr stringByReplacingOccurrencesOfString:@" "
                                                         withString:@""];
    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:newStr]])
    {
        NSLog(@"uber");
    }
    else
    {
        
        
        
        NSString *urlStr = [NSString stringWithFormat:@"https://www.lyft.com/signup/SDKSIGNUP?clientId=14Tc17vjnLwW&destination[longitude]=%@&destination[latitude]=%@",longitude,lattitude];
        NSString *newStr = [urlstr stringByReplacingOccurrencesOfString:@" "
                                                             withString:@""];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newStr]];
    }
    
}





@end
