//
//  CoronaPreventionVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 28/08/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "CoronaPreventionVC.h"
#import "SVProgressHUD.h"
#import "WebCheckInVC.h"
#import "Reachability.h"
#import "ValidationAndJsonVC.h"
#import "RestrictionsandStatsVC.h"
#import <QuartzCore/QuartzCore.h>

@interface CoronaPreventionVC ()
{
       NSArray *uniqueStateArray;
    NSMutableArray *uniqueStateAccrodingPnrArray;


       NSArray *airlineCodeArray;
        NSArray *hotelNameArray;
        NSArray *carNameArray;

        NSArray *airportCodeArray;
       
       NSDictionary *airlineDataDic;
       NSDictionary *hotelDataDic;
       NSDictionary *carDataDic;
       NSDictionary *completeCovidDataDic;
       NSDictionary *airportCodeGuideDic;
}
@end


typedef void(^myCompletion)(BOOL);

@implementation CoronaPreventionVC

- (void)viewDidLoad {
    
    uniqueStateAccrodingPnrArray = [[NSMutableArray alloc] init];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    [self setupLeftMenuButton];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
       NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
             
             if (networkStatus == NotReachable) {
                 [ValidationAndJsonVC displayAlert:nil HeaderMsg:@"No internet connection found!"];

             }
             else {
                 
                [self destinationArray:_listArray];
                [self apiCallingFunction];

             }
       
    [super viewDidLoad];
    
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"COVID-19 PREVENTION"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}

-(void)backAction{
    
  [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - Covid 19 safety and Policy of Vendors

-(void)apiCallForVendorSafetyPolicy:(NSString *)vendername code:(NSString *)venderCode type:(NSString *)venderType : (myCompletion) compblock {
     
   // [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    
    
    NSDictionary *parameters = @{
          @"VendorName": vendername,
          @"VendorCode": venderCode,
          @"VendorType": venderType
          
      };

      NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

      NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://testtravelapi.azurewebsites.net/v1/HealthAdvisory/GetVendorPolicies"]];
      [request setHTTPMethod:@"POST"];
      [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];

      NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
      NSURLSessionUploadTask *dataTask = [session uploadTaskWithRequest: request
              fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          NSLog(@"%@", json);
        // [SVProgressHUD dismiss];
          
          if (json) {
          
             // dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                  if ([venderType intValue] ==1) {
                      
                      hotelDataDic = json;
                  }
                  
                  else if ([venderType intValue] ==2) {
                      
                       airlineDataDic = json;
                      
                  }
              else if ([venderType intValue] ==3) {
                  
                   carDataDic = json;
                  
              }
           // });
              
              
                     
               compblock(YES);

         
                }
          }];

      [dataTask resume];
}

#pragma mark - covid prevention .

-(void)drawContainerviewForCovid {
    
    
     // [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
           CGRect screenRect = [[UIScreen mainScreen] bounds];
           CGFloat screenWidth = screenRect.size.width;
           CGFloat screenHeight = screenRect.size.height;
           
           UIScrollView   * scrollView = [[UIScrollView alloc] init];
           scrollView.frame = CGRectMake(10, 89, screenWidth-20 , screenHeight-89);
          CGFloat finalheight = 0;

    
        UILabel * mainHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight+10, scrollView.frame.size.width , 40)];
          mainHeadinglbl.text = @"CORONAVIRUS PREVENTION";
          mainHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:24.0f];
          mainHeadinglbl.adjustsFontSizeToFitWidth = YES;
          mainHeadinglbl.textAlignment = NSTextAlignmentCenter;
          mainHeadinglbl.textColor = [UIColor blackColor];
          [scrollView addSubview:mainHeadinglbl];
    
          finalheight = finalheight+5+40+10;
    
//    UILabel * headingforSource = [[UILabel alloc]initWithFrame:CGRectMake(2, finalheight, 300, 25)];
//        headingforSource.text = @"Data is sourced from Johns Hopkins CSSE";
//        headingforSource.font = [UIFont fontWithName:@"Lato-Bold" size:14.0f];
//        headingforSource.textColor = [UIColor blackColor];
//        [scrollView addSubview:headingforSource];
//
//        finalheight = finalheight+30;
//
//          UILabel * usaHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight, scrollView.frame.size.width, 50)];
//           usaHeadinglbl.text = @"  Destinations";
//          // usaHeadinglbl.text =[countryArray objectAtIndex:i];
//           usaHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
//           usaHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
//           usaHeadinglbl.textColor = [UIColor whiteColor];
//           usaHeadinglbl.layer.cornerRadius = 4;
//           usaHeadinglbl.userInteractionEnabled= YES;
//            usaHeadinglbl.layer.masksToBounds = true;
//           [scrollView addSubview:usaHeadinglbl];
//
//    UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(160, 10, 190, 30)];
//    TotalCaselbl.textAlignment = NSTextAlignmentRight;
//    TotalCaselbl.adjustsFontSizeToFitWidth = YES;
//    TotalCaselbl.text = @"Cases    Deaths    Recovered";
//    TotalCaselbl.font = [UIFont fontWithName:@"Lato-Bold" size:15.0f];
//    TotalCaselbl.textColor = [UIColor whiteColor];
//    [usaHeadinglbl addSubview:TotalCaselbl];
//
//    finalheight = finalheight+50;
//
    [self uniqueArrayOfstateAccrodingPNR];
//    NSArray *setStateArray = [[NSSet setWithArray:uniqueStateAccrodingPnrArray] allObjects];
//
//
//
//    for (int i=0; i<setStateArray.count; i++) {
//
//        NSString *stateStr = [setStateArray objectAtIndex:i];
//
//        if (stateStr.length==2) {
//            NSString *stateArrayStr = [completeCovidDataDic  valueForKey:@"StateData"];
//                   NSData *data1 = [stateArrayStr dataUsingEncoding:NSUTF8StringEncoding];
//                      id json1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
//
//                    NSArray *dataStateArray = json1;
//
//
//            for (int a=0; a<dataStateArray.count; a++) {
//                if ([stateStr isEqualToString:[[dataStateArray objectAtIndex:a]valueForKey:@"state"]]) {
//                    NSDictionary *dataStateDic = [dataStateArray objectAtIndex:a];
//                       UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 40)];
//                            grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
//                            [scrollView addSubview:grayBGFornumber];
//
//                            if([dataStateDic valueForKey:@"state"]){
//                            UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 150, 30)];
//                            namelbl.text =[dataStateDic valueForKey:@"state"];
//                            namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                            namelbl.textColor = [UIColor blackColor];
//                            [grayBGFornumber addSubview:namelbl];
//
//                            }
//                            if([dataStateDic valueForKey:@"positive"]){
//
//                            UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(155, 5, 70, 30)];
//                               TotalCaselbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"positive"]intValue]];
//                               TotalCaselbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                TotalCaselbl.adjustsFontSizeToFitWidth = YES;
//                               TotalCaselbl.textColor = [UIColor grayColor];
//                               [grayBGFornumber addSubview:TotalCaselbl];
//                            }
//
//                                if([dataStateDic valueForKey:@"death"]){
//                               UILabel * Totaldeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(225, 5, 60, 30)];
//                               Totaldeathlbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"death"]intValue]];
//                               Totaldeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                               Totaldeathlbl.textColor = [UIColor redColor];
//                                Totaldeathlbl.adjustsFontSizeToFitWidth = YES;
//                               [grayBGFornumber addSubview:Totaldeathlbl];
//
//                                }
//                                   if([dataStateDic valueForKey:@"recovered"]!=[NSNull null]){
//                               UILabel * TotalRecoveredlbl = [[UILabel alloc]initWithFrame:CGRectMake(290, 5, 60, 30)];
//                               TotalRecoveredlbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"recovered"]intValue]];
//                               TotalRecoveredlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                      //TotalRecoveredlbl.textAlignment = NSTextAlignmentRight;
//                               TotalRecoveredlbl.textColor = [UIColor greenColor];
//                                       TotalRecoveredlbl.adjustsFontSizeToFitWidth = YES;
//                               [grayBGFornumber addSubview:TotalRecoveredlbl];
//                                   }
//                            finalheight = finalheight +40;
//
//
//                }
//
//            }
//        }
//
//        else if(stateStr.length>2){
//
//            NSString *stateArrayStr = [completeCovidDataDic  valueForKey:@"CountryData"];
//                             NSData *data1 = [stateArrayStr dataUsingEncoding:NSUTF8StringEncoding];
//                                id json1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
//
//                              NSArray *dataStateArray = json1;
//
//
//                      for (int a=0; a<dataStateArray.count; a++) {
//                          if ([stateStr isEqualToString:[[dataStateArray objectAtIndex:a]valueForKey:@"Country"]]) {
//                              NSDictionary *dataStateDic = [dataStateArray objectAtIndex:a];
//                                 UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 40)];
//                                      grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
//                                      [scrollView addSubview:grayBGFornumber];
//
//                                      if([dataStateDic valueForKey:@"Country"]){
//                                      UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 150, 30)];
//                                      namelbl.text =[dataStateDic valueForKey:@"Country"];
//                                          namelbl.adjustsFontSizeToFitWidth = YES;
//                                      namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                      namelbl.textColor = [UIColor blackColor];
//                                      [grayBGFornumber addSubview:namelbl];
//
//                                      }
//                                      if([dataStateDic valueForKey:@"TotalConfirmed"]){
//
//                                      UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(155, 5, 70, 30)];
//                                         TotalCaselbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"TotalConfirmed"]intValue]];
//                                         TotalCaselbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                         TotalCaselbl.textColor = [UIColor grayColor];
//                                           TotalCaselbl.adjustsFontSizeToFitWidth = YES;
//                                         [grayBGFornumber addSubview:TotalCaselbl];
//                                      }
//
//                                          if([dataStateDic valueForKey:@"TotalDeaths"]){
//                                         UILabel * Totaldeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(225, 5, 60, 30)];
//                                         Totaldeathlbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"TotalDeaths"]intValue]];
//                                         Totaldeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                               Totaldeathlbl.adjustsFontSizeToFitWidth = YES;
//                                         Totaldeathlbl.textColor = [UIColor redColor];
//                                         [grayBGFornumber addSubview:Totaldeathlbl];
//
//                                          }
//                                             if([dataStateDic valueForKey:@"TotalRecovered"]!=[NSNull null]){
//                                         UILabel * TotalRecoveredlbl = [[UILabel alloc]initWithFrame:CGRectMake(290, 5, 60, 30)];
//                                         TotalRecoveredlbl.text = [NSString stringWithFormat:@"%d",[[dataStateDic valueForKey:@"TotalRecovered"]intValue]];
//                                         TotalRecoveredlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//                                                //TotalRecoveredlbl.textAlignment = NSTextAlignmentRight;
//                                                  TotalRecoveredlbl.adjustsFontSizeToFitWidth = YES;
//                                         TotalRecoveredlbl.textColor = [UIColor greenColor];
//                                         [grayBGFornumber addSubview:TotalRecoveredlbl];
//                                             }
//                                      finalheight = finalheight +40;
//
//                  }
//
//               }
//
//            }
//    }
//
    int xposForButton = 0;
    
    
    
    UILabel * travelHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight+10, scrollView.frame.size.width, 50)];
     travelHeadinglbl.text = @"  Travel Guidance";
    // usaHeadinglbl.text =[countryArray objectAtIndex:i];
     travelHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
     travelHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
     travelHeadinglbl.textColor = [UIColor whiteColor];
     travelHeadinglbl.layer.cornerRadius = 4;
     travelHeadinglbl.userInteractionEnabled= YES;
      travelHeadinglbl.layer.masksToBounds = true;
     [scrollView addSubview:travelHeadinglbl];

    
    
    finalheight = finalheight+60;
    
    
    
    NSArray *uniqueCodeResponseArray = [airportCodeGuideDic valueForKey:@"trips"];
    for (int c=0; c<uniqueCodeResponseArray.count; c++) {
        
        NSDictionary *dataDic = [uniqueCodeResponseArray objectAtIndex: c];
        
        UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 50)];
        grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
        [scrollView addSubview:grayBGFornumber];
        
        CGFloat heightofgrayBg = 0 ;
        
                            UIImageView *icaonImageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 8, 22, 22)];
                            icaonImageView.image = [UIImage imageNamed:@"flight1.png"];
                            [grayBGFornumber addSubview:icaonImageView];
        
        
                            UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(40, 5, 150, 30)];
                            NSString *str = [NSString stringWithFormat:@"%@ - %@",[dataDic valueForKey:@"from_airport"],[dataDic valueForKey:@"to_airport"]];
                            namelbl.text = str;
                            namelbl.font = [UIFont fontWithName:@"Lato-Bold" size:17.0f];
                            namelbl.textColor = [UIColor blackColor];
                            [grayBGFornumber addSubview:namelbl];
                            grayBGFornumber.userInteractionEnabled = YES;
        
              
         
               UIButton *restrictbutton = [UIButton buttonWithType:UIButtonTypeCustom];
               [restrictbutton addTarget:self
                                          action:@selector(btnTappedForRestrictions:)
                                forControlEvents:UIControlEventTouchUpInside];
               restrictbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
               restrictbutton.titleLabel.adjustsFontSizeToFitWidth = YES;

               [restrictbutton setFrame:CGRectMake(scrollView.frame.size.width-170,9, 90 , 25)];
               [restrictbutton setBackgroundColor:[UIColor redColor]];
               restrictbutton.titleLabel.textColor=[UIColor whiteColor];
               restrictbutton.layer.cornerRadius = 4;
               restrictbutton.userInteractionEnabled= YES;
               restrictbutton.layer.masksToBounds = true;
               restrictbutton.tag=c;
               restrictbutton.userInteractionEnabled = YES;
               [restrictbutton setTitle:@"Restrictions" forState:UIControlStateNormal];
               [grayBGFornumber addSubview:restrictbutton];
               
               
               
                       
                                        UIButton *secondbutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                         [secondbutton addTarget:self
                                                                  action:@selector(btnTapped:)
                                                        forControlEvents:UIControlEventTouchUpInside];
                                         secondbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                         secondbutton.titleLabel.adjustsFontSizeToFitWidth = YES;

                                         [secondbutton setFrame:CGRectMake(scrollView.frame.size.width-75,9, 70 , 25)];
                                         [secondbutton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
                                         secondbutton.titleLabel.textColor=[UIColor whiteColor];
                                         secondbutton.layer.cornerRadius = 4;
                                         secondbutton.userInteractionEnabled= YES;
                                         secondbutton.layer.masksToBounds = true;
                                         secondbutton.tag=c;
                                         [secondbutton setTitle:@"Guidance" forState:UIControlStateNormal];
                                         [grayBGFornumber addSubview:secondbutton];
               
        
        
        
        
        
        
        
        BOOL restrictionHave = FALSE;
        
      //  for (int f=0; f<uniqueStateArray.count; f++) {
        
            NSArray *dataArra = [completeCovidDataDic valueForKey:@"ListOfAdvisories"];
            for (int i=0; i<dataArra.count; i++) {
        if ([[uniqueStateAccrodingPnrArray objectAtIndex:c] isEqualToString:[[dataArra objectAtIndex:i]valueForKey:@"StateCode"]] || [[uniqueStateAccrodingPnrArray objectAtIndex:c] isEqualToString:[[dataArra objectAtIndex:i]valueForKey:@"CountryName"]]) {
                    
           // NSLog(@"uniquestateArray===%@ state %@ country = %@",[uniqueStateAccrodingPnrArray objectAtIndex:c],[[dataArra objectAtIndex:i]valueForKey:@"StateCode"],[[dataArra objectAtIndex:i]valueForKey:@"CountryName"]);
            NSString  * TravelRestricitonStr = [[dataArra objectAtIndex:i] valueForKey:@"TravelRestriciton"];
            
            if (TravelRestricitonStr.length !=0) {
            
            CGFloat heightOfText = [self getTextHeightByWidth:TravelRestricitonStr textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-21];
            UILabel * advicelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 43, 160, 20)];
             advicelbl.text = @"Travel Restriciton";
             advicelbl.font = [UIFont fontWithName:@"Lato-Bold" size:16.0f];
             advicelbl.textColor = [UIColor darkGrayColor];
            [grayBGFornumber addSubview:advicelbl];
            
            UITextView * adviceTextlbl = [[UITextView alloc]initWithFrame:CGRectMake(11,58, scrollView.frame.size.width-17, heightOfText+15)];
            adviceTextlbl.text = TravelRestricitonStr ;
            adviceTextlbl.scrollEnabled = NO;
            adviceTextlbl.backgroundColor = [UIColor clearColor];
            adviceTextlbl.userInteractionEnabled = YES;
            adviceTextlbl.textAlignment = NSTextAlignmentNatural;
            adviceTextlbl.selectable =YES;
            adviceTextlbl.editable = NO;
            adviceTextlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
            adviceTextlbl.textColor = [UIColor darkGrayColor];
            [grayBGFornumber addSubview:adviceTextlbl];
            
           // grayBGFornumber.frame = CGRectMake(0,finalheight, scrollView.frame.size.width, 50+heightOfText+20);
            
            //finalheight = finalheight + heightOfText+20;
             heightofgrayBg = heightofgrayBg+heightOfText+20+25;
            restrictionHave = TRUE;
                    
            }
                    
            }
                
            }
       // }
        
        if (!restrictionHave) {
            
            heightofgrayBg = heightofgrayBg +25;
          //  grayBGFornumber.frame = CGRectMake(0,finalheight, scrollView.frame.size.width, 50);
           // finalheight = finalheight + 50;

        }
        
        
        if ([[dataDic valueForKey:@"advice"]valueForKey:@"level_desc"]) {
            
            NSString *lblStr = [[dataDic valueForKey:@"advice"]valueForKey:@"level_desc"];
            
            if (lblStr.length !=0) {
                
                CGFloat heightOfText = [self getTextHeightByWidth:lblStr textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-21];
                
                UITextView * adviceTextlbl = [[UITextView alloc]initWithFrame:CGRectMake(11, heightofgrayBg+10, scrollView.frame.size.width-17, heightOfText+15)];
                adviceTextlbl.text = lblStr ;
                adviceTextlbl.scrollEnabled = NO;
                adviceTextlbl.backgroundColor = [UIColor clearColor];
                adviceTextlbl.userInteractionEnabled = YES;
                adviceTextlbl.textAlignment = NSTextAlignmentNatural;
                adviceTextlbl.selectable =YES;
                adviceTextlbl.editable = NO;
                adviceTextlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                adviceTextlbl.textColor = [UIColor darkGrayColor];
                [grayBGFornumber addSubview:adviceTextlbl];
                
                //grayBGFornumber.frame = CGRectMake(0,finalheight, scrollView.frame.size.width, heightOfText+10);

               heightofgrayBg = heightofgrayBg+ heightOfText+20+7;
                //finalheight = finalheight + heightOfText+10 ;

            }
            
            
        }
        
        if ([[dataDic valueForKey:@"advice"]valueForKey:@"recommendation"]) {

        NSString *recomndStr = [[dataDic valueForKey:@"advice"]valueForKey:@"recommendation"];
        CGFloat heightOfText = [self getTextHeightByWidth:recomndStr textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-21];
            
         UILabel * advicelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, heightofgrayBg, 160, 20)];
         advicelbl.text = @"Recommendation";
         advicelbl.font = [UIFont fontWithName:@"Lato-Bold" size:16.0f];
         advicelbl.textColor = [UIColor darkGrayColor];
        [grayBGFornumber addSubview:advicelbl];
        
        UITextView * adviceTextlbl = [[UITextView alloc]initWithFrame:CGRectMake(11, heightofgrayBg+15, scrollView.frame.size.width-17, heightOfText)];
        adviceTextlbl.text = recomndStr ;
        adviceTextlbl.scrollEnabled = NO;
        adviceTextlbl.backgroundColor = [UIColor clearColor];
        adviceTextlbl.userInteractionEnabled = YES;
        adviceTextlbl.textAlignment = NSTextAlignmentNatural;
        adviceTextlbl.selectable =YES;
        adviceTextlbl.editable = NO;
        adviceTextlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
        adviceTextlbl.textColor = [UIColor darkGrayColor];
        [grayBGFornumber addSubview:adviceTextlbl];
        
        heightofgrayBg = heightofgrayBg + heightOfText+5 ;

        
        }
        
        if ([[dataDic valueForKey:@"advice"]valueForKey:@"requirements"]) {

            NSDictionary *reqDic = [[dataDic valueForKey:@"advice"]valueForKey:@"requirements"];
            NSString *completeStr = [NSString stringWithFormat:@"Tests: %@\rQuarantine: %@\rMasks: %@",[reqDic valueForKey:@"tests"],[reqDic valueForKey:@"quarantine"],[reqDic valueForKey:@"masks"]];
               
                UILabel * advicelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, heightofgrayBg, 160, 20)];
                advicelbl.text = @"Travel Requirements";
                advicelbl.font = [UIFont fontWithName:@"Lato-Bold" size:16.0f];
                advicelbl.textColor = [UIColor darkGrayColor];
               [grayBGFornumber addSubview:advicelbl];
               
               UITextView * adviceTextlbl = [[UITextView alloc]initWithFrame:CGRectMake(11, heightofgrayBg+15, scrollView.frame.size.width-17, 70)];
               adviceTextlbl.text = completeStr ;
               adviceTextlbl.scrollEnabled = NO;
               adviceTextlbl.backgroundColor = [UIColor clearColor];
               adviceTextlbl.userInteractionEnabled = YES;
               adviceTextlbl.textAlignment = NSTextAlignmentNatural;
               adviceTextlbl.selectable =YES;
               adviceTextlbl.editable = NO;
               adviceTextlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
               adviceTextlbl.textColor = [UIColor darkGrayColor];
               [grayBGFornumber addSubview:adviceTextlbl];
               
               heightofgrayBg = heightofgrayBg + 70+25 ;

               
               }
        
        
       
        
        
        
        
        
        
        
        finalheight = finalheight + heightofgrayBg;
        
        grayBGFornumber.frame = CGRectMake(grayBGFornumber.frame.origin.x,grayBGFornumber.frame.origin.y, scrollView.frame.size.width, heightofgrayBg);
        
                            if (c!=[uniqueCodeResponseArray count]-1) {
        
                                UILabel * linelbl = [[UILabel alloc]initWithFrame:CGRectMake(5, grayBGFornumber.frame.size.height-1, grayBGFornumber.frame.size.width-10, 1)];
                                          linelbl.backgroundColor = [UIColor blackColor];
                                          [grayBGFornumber addSubview:linelbl];
                               
        }
        
                
                              //  finalheight = finalheight +100;
                
                
    
    
    }
        
// vendor Resorces -----------------------------------------------------------------------------
    
           
    UILabel * vendorHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight+20, scrollView.frame.size.width, 50)];
     vendorHeadinglbl.text = @"  Vendor Resources";
    // usaHeadinglbl.text =[countryArray objectAtIndex:i];
     vendorHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
     vendorHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
     vendorHeadinglbl.textColor = [UIColor whiteColor];
     vendorHeadinglbl.layer.cornerRadius = 4;
     vendorHeadinglbl.userInteractionEnabled= YES;
      vendorHeadinglbl.layer.masksToBounds = true;
     [scrollView addSubview:vendorHeadinglbl];
    
    finalheight = finalheight+70;
    
    for (int c=0; c<airlineCodeArray.count; c++) {
        
        for (int d=0; d<[[airlineDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            
            NSDictionary *airlineDic = [[airlineDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
            
            if ([[airlineCodeArray objectAtIndex:c]isEqualToString:[airlineDic valueForKey:@"VendorCode"]]) {
                
                
                                UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 40)];
                                grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
                                [scrollView addSubview:grayBGFornumber];
                

                                UIImageView *icaonImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 22, 22)];
                                icaonImageView.image = [UIImage imageNamed:@"flight1.png"];
                                [grayBGFornumber addSubview:icaonImageView];
                
                                if([airlineDic valueForKey:@"VendorName"]){
                                UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(38, 5, 165, 30)];
                                namelbl.text =[airlineDic valueForKey:@"VendorName"];
                                namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                                namelbl.adjustsFontSizeToFitWidth =YES;

                                namelbl.textColor = [UIColor blackColor];
                                [grayBGFornumber addSubview:namelbl];
                                grayBGFornumber.userInteractionEnabled = YES;
                
                                
                                UIButton *Safetybutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                [Safetybutton addTarget:self
                                                           action:@selector(safetybtnTapped:)
                                                 forControlEvents:UIControlEventTouchUpInside];
                                Safetybutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                [Safetybutton setFrame:CGRectMake(scrollView.frame.size.width-130,5, 60 , 25)];
                                [Safetybutton setBackgroundColor:[UIColor colorWithRed:61.0/255.0 green:164.0/255.0 blue:21.0/255.0 alpha:1]];
                                Safetybutton.titleLabel.textColor=[UIColor whiteColor];
                                Safetybutton.layer.cornerRadius = 4;
                                Safetybutton.userInteractionEnabled= YES;
                                Safetybutton.layer.masksToBounds = true;
                                Safetybutton.tag=c;
                                [Safetybutton setTitle:@"Safety" forState:UIControlStateNormal];
                                [grayBGFornumber addSubview:Safetybutton];
                                
                
                                UIButton *secondbutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                [secondbutton addTarget:self
                                                           action:@selector(policybtnTapped:)
                                                 forControlEvents:UIControlEventTouchUpInside];
                                secondbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                [secondbutton setFrame:CGRectMake(scrollView.frame.size.width-65,5, 60 , 25)];
                                [secondbutton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
                                secondbutton.titleLabel.textColor=[UIColor whiteColor];
                                secondbutton.layer.cornerRadius = 4;
                                secondbutton.userInteractionEnabled= YES;
                                secondbutton.layer.masksToBounds = true;
                                secondbutton.tag=c;
                                [secondbutton setTitle:@"Policies" forState:UIControlStateNormal];
                                [grayBGFornumber addSubview:secondbutton];
                
                                finalheight = finalheight +40;
                
            }
            
        }

    }
    
}
    
    
    
    for (int c=0; c<hotelNameArray.count; c++) {
        
        for (int d=0; d<[[hotelDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            
            NSDictionary *hotelDic = [[hotelDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
            
            NSString *hotelcompleteStr = [hotelNameArray objectAtIndex:c];
            NSString *hotelsubstrStr = [hotelDic valueForKey:@"VendorName"];

                if ([hotelcompleteStr rangeOfString:hotelsubstrStr options:NSCaseInsensitiveSearch].location != NSNotFound) {

                            UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 40)];
                            grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
                            [scrollView addSubview:grayBGFornumber];
                    

                            UIImageView *icaonImageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 14, 20, 12)];
                            icaonImageView.image = [UIImage imageNamed:@"hotel.png"];
                            [grayBGFornumber addSubview:icaonImageView];
                
                            if([hotelDic valueForKey:@"VendorName"]){
                            UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(38, 5, 170, 30)];
                            namelbl.text = [hotelcompleteStr capitalizedString];
                            namelbl.adjustsFontSizeToFitWidth =YES;
                            namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                            namelbl.textColor = [UIColor blackColor];
                            [grayBGFornumber addSubview:namelbl];
                            grayBGFornumber.userInteractionEnabled = YES;
                
                
                                
                                UIButton *Safetybutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                [Safetybutton addTarget:self
                                                 action:@selector(safetybtnHotelTapped:)
                                                   forControlEvents:UIControlEventTouchUpInside];
                                Safetybutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                [Safetybutton setFrame:CGRectMake(scrollView.frame.size.width-130,5, 60 , 25)];
                                [Safetybutton setBackgroundColor:[UIColor colorWithRed:61.0/255.0 green:164.0/255.0 blue:21.0/255.0 alpha:1]];
                                Safetybutton.titleLabel.textColor=[UIColor whiteColor];
                                Safetybutton.layer.cornerRadius = 4;
                                Safetybutton.userInteractionEnabled= YES;
                                Safetybutton.layer.masksToBounds = true;
                                Safetybutton.tag=c;
                                Safetybutton.userInteractionEnabled = YES;
                                [Safetybutton setTitle:@"Safety" forState:UIControlStateNormal];
                                [grayBGFornumber addSubview:Safetybutton];
                                
                                
                                
                                UIButton *secondbutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                  [secondbutton addTarget:self
                                                           action:@selector(policybtnHotelTapped:)
                                                 forControlEvents:UIControlEventTouchUpInside];
                                  secondbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                  [secondbutton setFrame:CGRectMake(scrollView.frame.size.width-65,5, 60 , 25)];
                                  [secondbutton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
                                  secondbutton.titleLabel.textColor=[UIColor whiteColor];
                                  secondbutton.layer.cornerRadius = 4;
                                  secondbutton.userInteractionEnabled= YES;
                                  secondbutton.layer.masksToBounds = true;
                                  secondbutton.tag=c;
                                  secondbutton.userInteractionEnabled = YES;
                                  [secondbutton setTitle:@"Policies" forState:UIControlStateNormal];
                                  [grayBGFornumber addSubview:secondbutton];
                
                                finalheight = finalheight +40;
                                
                                break;
                
            }
            
        }
        
    }
    
}
    
    
    
     for (int e=0; e<carNameArray.count; e++) {
            
            for (int f=0; f<[[carDataDic valueForKey:@"ListOfPolicies"] count]; f++) {
                
                NSDictionary *carDic = [[carDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:f];
                
                NSString *hotelcompleteStr = [carNameArray objectAtIndex:e];
                NSString *hotelsubstrStr = [carDic valueForKey:@"VendorName"];

                    if ([hotelcompleteStr rangeOfString:hotelsubstrStr options:NSCaseInsensitiveSearch].location != NSNotFound) {

                                UILabel *grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 40)];
                                grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
                                [scrollView addSubview:grayBGFornumber];
                        

                                UIImageView *icaonImageView = [[UIImageView alloc]initWithFrame:CGRectMake(11, 12, 20, 15)];
                                icaonImageView.image = [UIImage imageNamed:@"car.png"];
                                [grayBGFornumber addSubview:icaonImageView];
                    
                                if([carDic valueForKey:@"VendorName"]){
                                UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(38, 5, 170, 30)];
                                namelbl.text = [hotelsubstrStr capitalizedString];
                                namelbl.adjustsFontSizeToFitWidth =YES;
                                namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                                namelbl.textColor = [UIColor blackColor];
                                [grayBGFornumber addSubview:namelbl];
                                grayBGFornumber.userInteractionEnabled = YES;
                    
                    
                                    
                                    UIButton *Safetybutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                    [Safetybutton addTarget:self
                                                     action:@selector(car_safetybtnTapped:)
                                                       forControlEvents:UIControlEventTouchUpInside];
                                    Safetybutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                    [Safetybutton setFrame:CGRectMake(scrollView.frame.size.width-130,5, 60 , 25)];
                                    [Safetybutton setBackgroundColor:[UIColor colorWithRed:61.0/255.0 green:164.0/255.0 blue:21.0/255.0 alpha:1]];
                                    Safetybutton.titleLabel.textColor=[UIColor whiteColor];
                                    Safetybutton.layer.cornerRadius = 4;
                                    Safetybutton.userInteractionEnabled= YES;
                                    Safetybutton.layer.masksToBounds = true;
                                    Safetybutton.tag=e;
                                    Safetybutton.userInteractionEnabled = YES;
                                    [Safetybutton setTitle:@"Safety" forState:UIControlStateNormal];
                                    [grayBGFornumber addSubview:Safetybutton];
                                    
                                    
                                    
                                    UIButton *secondbutton = [UIButton buttonWithType:UIButtonTypeCustom];
                                      [secondbutton addTarget:self
                                                               action:@selector(car_policybtnTapped:)
                                                     forControlEvents:UIControlEventTouchUpInside];
                                      secondbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
                                      [secondbutton setFrame:CGRectMake(scrollView.frame.size.width-65,5, 60 , 25)];
                                      [secondbutton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
                                      secondbutton.titleLabel.textColor=[UIColor whiteColor];
                                      secondbutton.layer.cornerRadius = 4;
                                      secondbutton.userInteractionEnabled= YES;
                                      secondbutton.layer.masksToBounds = true;
                                      secondbutton.tag=e;
                                      secondbutton.userInteractionEnabled = YES;
                                      [secondbutton setTitle:@"Policies" forState:UIControlStateNormal];
                                      [grayBGFornumber addSubview:secondbutton];
                    
                                    finalheight = finalheight +40;
                                    
                                    break;
                    
                }
                
            }
            
        }
        
    }
    
    
    
    

          scrollView.showsVerticalScrollIndicator = NO;
          scrollView.backgroundColor = [UIColor whiteColor];
           
           scrollView.contentSize = CGSizeMake(screenWidth-30, finalheight+250);
          [self.view addSubview:scrollView];
    
    
}


- (IBAction)car_safetybtnTapped:(id)sender {
  int index = [sender tag] ;
    
    
          for (int d=0; d<[[carDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            NSDictionary *carDic = [[carDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
        //if ([[carNameArray objectAtIndex:index]isEqualToString:[carDic valueForKey:@"VendorName"]]) {
           
         if ([[carNameArray objectAtIndex:index] rangeOfString:[carDic valueForKey:@"VendorName"] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                 myView.headerStr = [carDic valueForKey:@"VendorName"];

                  myView.webLink = [carDic valueForKey:@"Safety"];

                  [self.navigationController pushViewController:myView animated:true];
        }
    }
}

    
    - (IBAction)car_policybtnTapped:(id)sender {
      int index = [sender tag] ;
        
        
             for (int d=0; d<[[carDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
                        NSDictionary *carDic = [[carDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
                  //  if ([[carNameArray objectAtIndex:index]isEqualToString:[carDic valueForKey:@"VendorName"]]) {
                  if ([[carNameArray objectAtIndex:index] rangeOfString:[carDic valueForKey:@"VendorName"] options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        
                        WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                             myView.headerStr = [carDic valueForKey:@"VendorName"];

                              myView.webLink = [carDic valueForKey:@"Policies"];

                              [self.navigationController pushViewController:myView animated:true];
                    }
                }
    }


- (IBAction)safetybtnTapped:(id)sender {
  int index = [sender tag] ;
    
    
          for (int d=0; d<[[airlineDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            NSDictionary *airlineDic = [[airlineDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
        if ([[airlineCodeArray objectAtIndex:index]isEqualToString:[airlineDic valueForKey:@"VendorCode"]]) {
            
            WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                 myView.headerStr = [airlineDic valueForKey:@"VendorName"];

                  myView.webLink = [airlineDic valueForKey:@"Safety"];

                  [self.navigationController pushViewController:myView animated:true];
        }
    }
}

    
    - (IBAction)policybtnTapped:(id)sender {
      int index = [sender tag] ;
        
        
              for (int d=0; d<[[airlineDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
                NSDictionary *airlineDic = [[airlineDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
            if ([[airlineCodeArray objectAtIndex:index]isEqualToString:[airlineDic valueForKey:@"VendorCode"]]) {
                
                WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                     myView.headerStr = [airlineDic valueForKey:@"VendorName"];

                      myView.webLink = [airlineDic valueForKey:@"Policies"];

                      [self.navigationController pushViewController:myView animated:true];
            }
        }
    }

- (IBAction)policybtnHotelTapped:(id)sender {
  int index = [sender tag] ;
    
    
          for (int d=0; d<[[hotelDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            NSDictionary *hotelDic = [[hotelDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
          if ([[hotelNameArray objectAtIndex:index] rangeOfString:[hotelDic valueForKey:@"VendorName"] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                  myView.headerStr = [hotelNameArray objectAtIndex:index];
                  myView.webLink = [hotelDic valueForKey:@"Policies"];

                  [self.navigationController pushViewController:myView animated:true];
        }
    }
}

- (IBAction)safetybtnHotelTapped:(id)sender {
  int index = [sender tag] ;
    
    
          for (int d=0; d<[[hotelDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
            NSDictionary *hotelDic = [[hotelDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
          if ([[hotelNameArray objectAtIndex:index] rangeOfString:[hotelDic valueForKey:@"VendorName"] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                  myView.headerStr = [hotelNameArray objectAtIndex:index];
                  myView.webLink = [hotelDic valueForKey:@"Safety"];

                  [self.navigationController pushViewController:myView animated:true];
        }
    }
}



        - (IBAction)policyhotelbtnTapped:(id)sender {
             int index = [sender tag] ;
          for (int d=0; d<[[hotelDataDic valueForKey:@"ListOfPolicies"] count]; d++) {
           
           NSDictionary *hotelDic = [[hotelDataDic valueForKey:@"ListOfPolicies"] objectAtIndex:d];
           
           if ([[hotelNameArray objectAtIndex:index]isEqualToString:[hotelDic valueForKey:@"VendorName"]]) {
               WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                           myView.webLink = [hotelDic valueForKey:@"Policies"];

                    [self.navigationController pushViewController:myView animated:true];
           }
    
            }
            
        }
    


-(NSArray *)uniqueArrayOfstateAccrodingPNR {
    
    NSArray *UniqueArr ;
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    
//         for (int i=0; i<_listArray.count; i++) {
//
//                if([[[_listArray objectAtIndex:i]lastObject] valueForKey:@"airseg"]!=[NSNull null]){
//                NSDictionary *dic = [[[_listArray objectAtIndex:i]lastObject] valueForKey:@"airseg"];
      
                //NSArray *uniqueCodeResponseArray = [airportCodeGuideDic valueForKey:@"trips"];
                  //  for (int j=0; j<uniqueCodeResponseArray.count; j++) {
                        
                         for (int i=0; i<_listArray.count; i++) {
                      
                    if([[[_listArray objectAtIndex:i]lastObject] valueForKey:@"airseg"]!=[NSNull null]){
                    NSDictionary *dic = [[[_listArray objectAtIndex:i]lastObject] valueForKey:@"airseg"];
                        
                        
                        
                  //      if ([[dic valueForKey:@"tocitycode"] isEqualToString:[[uniqueCodeResponseArray objectAtIndex:j]valueForKey:@"to_airport"]]) {
                            
                            
                            if ([dic valueForKey:@"tocity"]) {
                            [dataArray addObject:[dic valueForKey:@"tocity"]];
                            }
                                              
                        //  break;
                      //  }
            }
                    
      //  }
                   
        
        
    }
    
     for (int j=0; j<dataArray.count; j++) {
         NSString *str = [dataArray objectAtIndex:j];
         NSArray *strArray = [str componentsSeparatedByString:@","];
        
         
         NSString *strD = [strArray lastObject];
         NSString *trimmedString = [strD stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         [uniqueStateAccrodingPnrArray addObject:trimmedString];
         
     }
     
    UniqueArr = uniqueStateAccrodingPnrArray;
    // NSArray *unqueStateCodeArray = [[NSSet setWithArray:completeSubstringArray] allObjects];
    // uniqueStateArray = completeSubstringArray;
    return UniqueArr;
}
- (IBAction)btnTapped:(id)sender {
  int index = [sender tag] ;
    
    
    NSString *strOfButton ;
    
  //NSArray *uniqueStArr =  [self uniqueArrayOfstateAccrodingPNR];
  //  if (uniqueStArr.count!=0) {
        strOfButton = [uniqueStateAccrodingPnrArray objectAtIndex:index];
  //  }

    NSArray *dataArra = [completeCovidDataDic valueForKey:@"ListOfAdvisories"];
    for (int i=0; i<dataArra.count; i++) {
    if ([strOfButton isEqualToString:[[dataArra objectAtIndex:i]valueForKey:@"StateCode"]] || [strOfButton isEqualToString:[[dataArra objectAtIndex:i]valueForKey:@"CountryName"]]) {
            
        NSString   * linkStr = [[dataArra objectAtIndex:i] valueForKey:@"Link"];
            
             WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
                myView.webLink = linkStr;

                [self.navigationController pushViewController:myView animated:true];
            
            break;
        }
        
    }

}

- (IBAction)btnTappedForRestrictions:(id)sender {
  int index = [sender tag] ;
    
    
    RestrictionsandStatsVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"RestrictionsandStatsVC"];
    NSDictionary *dic = [[airportCodeGuideDic valueForKey:@"trips"]objectAtIndex:index];
    myView.dataDic = dic;
    [self.navigationController pushViewController:myView animated:true];


}




- (CGFloat)getTextHeightByWidth:(NSString*)text textFont:(UIFont*)textFont textWidth:(float)textWidth {
    
    if (!text) {
        return 0;
    }
    CGSize boundingSize = CGSizeMake(textWidth, CGFLOAT_MAX);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: textFont }];
    
    CGRect rect = [attributedText boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize requiredSize = rect.size;
    return requiredSize.height;
}


-(NSArray *)destinationArray :(NSArray *)listArrays {
    NSMutableArray *completeArray =[[NSMutableArray alloc] init] ;
    NSMutableArray *cityCodeArray =[[NSMutableArray alloc] init] ;

    NSMutableArray *completeSubstringArray =[[NSMutableArray alloc] init] ;
    NSMutableArray *airCodeArray =[[NSMutableArray alloc] init] ;
    NSMutableArray *hotelNameArr =[[NSMutableArray alloc] init] ;
    NSMutableArray *carNameArr =[[NSMutableArray alloc] init] ;

   // [_destinationdataArray removeAllObjects];
    
    for (int i=0; i<listArrays.count; i++) {
        
        if([[[listArrays objectAtIndex:i]lastObject] valueForKey:@"airseg"]!=[NSNull null]){
        NSDictionary *dic = [[[listArrays objectAtIndex:i]lastObject] valueForKey:@"airseg"];
//        if ([dic valueForKey:@"fromcity"]) {
//            [completeArray addObject:[dic valueForKey:@"fromcity"]];
//            }
        
//        if ([dic valueForKey:@"tocity"]) {
//             [completeArray addObject:[dic valueForKey:@"tocity"]];
//        }
        
        if ([dic valueForKey:@"fromcitycode"] && [dic valueForKey:@"tocitycode"])  {
            NSDictionary *codeSetDic = [NSDictionary dictionaryWithObjectsAndKeys:[dic valueForKey:@"tocitycode"],@"ArrivalAirportCode",[dic valueForKey:@"fromcitycode"],@"DepartureAirportCode", nil];

            
             [cityCodeArray addObject:codeSetDic];
                       
        
        }
        
            
            if ([dic valueForKey:@"airlinecode"]) {
                        NSString *trimmed = [[dic valueForKey:@"airlinecode"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                       [airCodeArray addObject:trimmed];
                       
                   }
            
        }
        
         if([[[listArrays objectAtIndex:i]lastObject] valueForKey:@"hotelseg"]!=[NSNull null]){
               NSDictionary *dic = [[[listArrays objectAtIndex:i]lastObject] valueForKey:@"hotelseg"];
            if ([dic valueForKey:@"propertyname"]) {
                 
                [hotelNameArr addObject:[dic valueForKey:@"propertyname"]];
                
            }
        }
        
        if([[[listArrays objectAtIndex:i]lastObject] valueForKey:@"carseg"]!=[NSNull null]){
                      NSDictionary *dic = [[[listArrays objectAtIndex:i]lastObject] valueForKey:@"carseg"];
                   if ([dic valueForKey:@"carvendor"]) {
                        
                       [carNameArr addObject:[dic valueForKey:@"carvendor"]];
                       
                   }
               }
    }
      // for airlinecode  refining Data
    airlineCodeArray = [[NSSet setWithArray:airCodeArray] allObjects];
    carNameArray = [[NSSet setWithArray:carNameArr] allObjects];


    hotelNameArray = [[NSSet setWithArray:hotelNameArr] allObjects];
    airportCodeArray = [NSArray arrayWithArray: cityCodeArray];
    
    // for state code refining Data
  //  NSArray *newArray =  [[NSSet setWithArray:completeArray] allObjects];
    NSArray *subStrArray ;
//    for (int j=0; j<completeArray.count; j++) {
//        NSString *str = [completeArray objectAtIndex:j];
//        NSArray *strArray = [str componentsSeparatedByString:@","];
//
//
//        NSString *strD = [strArray lastObject];
//        NSString *trimmedString = [strD stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        [completeSubstringArray addObject:trimmedString];
//
//    }
    
   // NSArray *unqueStateCodeArray = [[NSSet setWithArray:completeSubstringArray] allObjects];
   // uniqueStateArray = completeSubstringArray;

   // subStrArray = [NSArray arrayWithArray:completeSubstringArray];
    
    // CHACKING STATE CODE OF 2 DIGIT
    
    return subStrArray;
}
  


-(void)apiCallForCountry :(NSString *)countryCode :(NSString *)stateCode :(myCompletion) compblock {
    
    
    NSDictionary *parameters = @{
          @"CountryName": @"",
          @"CountryCode": countryCode,
          @"StateCode":stateCode
          
      };

      NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

      NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://testtravelapi.azurewebsites.net/v1/HealthAdvisory/GetPublicAdvisory"]];
      [request setHTTPMethod:@"POST"];
      [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];

      NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    

      NSURLSessionUploadTask *dataTask = [session uploadTaskWithRequest: request
              fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          NSLog(@"%@", json);
         
          
          if (json) {
        
              
              completeCovidDataDic = json ;

                      
              compblock(YES);


                        }
          }];
      [dataTask resume];
    

    
}



-(void)apiCallWIthCodesOfAirport :(NSString *)countryCode :(NSArray *)ListOfOriginDestination :(myCompletion) compblock {
    
    
    NSDictionary *parameters = @{
          @"ListOfOriginDestination": ListOfOriginDestination
          
      };

      NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

      NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://testtravelapi.azurewebsites.net/v1/HealthAdvisory/GetTravelAdvisory"]];
      [request setHTTPMethod:@"POST"];
      [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];

      NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    

      NSURLSessionUploadTask *dataTask = [session uploadTaskWithRequest: request
              fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          NSLog(@"%@", json);
         
          
          if (json) {
        
              
              airportCodeGuideDic = json ;

                      
              compblock(YES);


                        }
          }];
      [dataTask resume];
    

    
}
-(void)apiCallingFunction {

    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
       [self apiCallForCountry:@"" :@"" :^(BOOL finished)
       {
           if(finished){
               NSLog(@"success");
               [self apiCallForVendorSafetyPolicy:@"" code:@"" type:@"2" : ^(BOOL finished)
                {
                    if(finished){
                        NSLog(@"Finish");
                        
                        [self apiCallForVendorSafetyPolicy:@"" code:@"" type:@"3" : ^(BOOL finished)
                        {
                            if(finished){
                                NSLog(@"Finish");
                            [self apiCallForVendorSafetyPolicy:@"" code:@"" type:@"1" : ^(BOOL finished)
                            {
                            if(finished){
                                
                                [self apiCallWIthCodesOfAirport:@"" :airportCodeArray  : ^(BOOL finished)
                                {
                                      if(finished){
                               
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     NSLog(@"%@%@%@",completeCovidDataDic,hotelDataDic,airlineDataDic);
                                                   [self drawContainerviewForCovid];
                                                   [SVProgressHUD dismiss];
                                          //  [activityIndicatorObject stopAnimating];
                                              });
                               

                                      }
                                 
                                 }];
                            }
                        }];
                    }
                            
                    }];
                    }
                }];
           }
       }];
       
    
}



@end
