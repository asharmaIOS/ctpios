//
//  SideSeenImageCell.h
//  Travel Leader
//
//  Created by Gurpreet's on 21/09/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideSeenImageCell : UICollectionViewCell


{
    
    __weak IBOutlet UILabel *labelHeader;
    __weak IBOutlet UIImageView *imageview;
    __weak IBOutlet UICollectionView *imagecollection;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@end
