//
//  AboutusViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/18/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "AboutusViewController.h"
#import "Constant.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TableViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"
#import "Reachability.h"



@interface AboutusViewController ()

@end

@implementation AboutusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.navigationController.navigationBar setHidden: NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    [self    setupLeftMenuButton];
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        NSDictionary  *dataDic = [userDef objectForKey:@"contact"];
        
        if (dataDic.allKeys !=nil) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // _aboutUsTextview.text = [result   valueForKey:@"about"];
                
                NSString *textViewText = [dataDic   valueForKey:@"about"];
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                paragraphStyle.lineSpacing = 07;
                NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:17.0f] };
                [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
                
                _aboutUsTextview.attributedText = attributedString;
                
                
                [SVProgressHUD dismiss];
                
                
                
            });
            
        }
        
        else {
            
            [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];

        }
    }
        else {
            
            [SVProgressHUD showWithStatus:@"Processing..." maskType:SVProgressHUDMaskTypeBlack];

            [self performSelector:@selector(aboutUsApi) withObject:self afterDelay:1.0 ];
            
            //[self aboutUsApi];

        }
    
        
   

}


- (void)aboutUsApi {
    
   // [SVProgressHUD showWithStatus:@"Processing..." maskType:SVProgressHUDMaskTypeBlack];
    NSLog(@"Agencycode === %@",[TravelConstantClass singleton].Agencycode);
    
    NSDictionary *dictionary = @{@"action":@"read",@"agencyCode":[TravelConstantClass singleton].Agencycode};
    
    NSString *urlStr = @"UpdateAgency";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
    
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
            // _aboutUsTextview.text = [result   valueForKey:@"about"];
            
            NSString *textViewText = [dataDic   valueForKey:@"about"];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 07;
            NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:17.0f] };
            [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
            
            _aboutUsTextview.attributedText = attributedString;
            
            
            [SVProgressHUD dismiss];
            
        
            
      //  });
        

            
            if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
                
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
                
                
            }
        
    }
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    // for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    //    textButton.title = @"ABOUT US";
    //    // textButton.tintColor = [UIColor whiteColor];
    //    textButton.tintColor = [UIColor whiteColor];
    //    self.navigationItem.leftBarButtonItems = @[searchItem];
    //    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"ABOUT US"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}

-(void)backAction
{
    
    [self backButton:self];
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}


-(void)backButton:(id)sender {
    
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
            home = (HomeViewController*)vc;
        }
    }
    
    
//    for (UIViewController *controller in self.navigationController.viewControllers) {
//           if ([controller isKindOfClass:[SWRevealViewController class]]) {
//
//               [self.navigationController popViewControllerAnimated:YES];
//               [self.revealViewController revealToggleAnimated:YES];
//
//              // [self.navigationController popToRootViewControllerAnimated:YES];
//               //[self.revealViewController revealToggleAnimated:YES];
//
//           //   [self.navigationController popToViewController:controller animated:YES];
//
//               [self.navigationController.navigationBar setHidden: YES];
//
//               return;
//           }
//       }
              
    // HomeViewController* home;
    //=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];

    
    if (home == nil) {
        //home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    
    // [self dismissViewControllerAnimated:YES completion:nil] ;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
