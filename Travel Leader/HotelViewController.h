//
//  HotelViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *carbtn;
@property (strong, nonatomic)  UIImage *headerImage;

- (IBAction)carbtn:(id)sender;

@end
