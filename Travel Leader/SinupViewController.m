//
//  SinupViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "SinupViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"
#import "LoginViewController.h"


@interface SinupViewController ()

@end

@implementation SinupViewController

- (void)viewDidLoad {
    
    UITapGestureRecognizer* tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
       [tapBackground setNumberOfTapsRequired:1];
       [self.view addGestureRecognizer:tapBackground];
    
    [self loadAutodata ];
    [super viewDidLoad];
    
    [self    setupLeftMenuButton];
    
}

-(void) dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}
-(void)loadAutodata {
    
   // NSArray *nameArray = [ [[[_dataDict valueForKey:@"name"]objectAtIndex:0]valueForKey:@"fullname"] componentsSeparatedByString:@"/"];
    
    if (_nameArray.count==2) {
        
        _firstnametxt.text = [_nameArray firstObject];
        _lastnametxt.text = [_nameArray lastObject];
        _suffixtxt.text = @"";

    }
    else {
        
        _firstnametxt.text = [_nameArray firstObject];
        _lastnametxt.text = [_nameArray objectAtIndex:1];
         _suffixtxt.text = [_nameArray lastObject];
        
    }
    
  
    _emailtxt.text = _emailStr;
    
    
    
}

-(void)setupLeftMenuButton{
    // for Back in left side
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"REGISTER"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backButton:)];
    
    textButton1.tintColor = [UIColor whiteColor];

    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}
-(void)backButton:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)textFieldDidBeginEditingMaintainPosition:(UITextField *)textField {
    
    [UIView animateWithDuration:0.50
                     animations:^{
        CGRect frame= self.view.frame;
                         frame.origin.y-=170;
                         self.view.frame=frame;
                         
                     }];
    
}

- (IBAction)textFieldShouldReturnMaintainPosition:(UITextField *)textField {
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= self.view.frame;
                         frame.origin.y+=170;
                         self.view.frame=frame;
                         
                     }];
    
    
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framePromo=_viewsinup.frame;
            framePromo.origin.y+=320;
            _viewsinup.frame=framePromo;
        }];
    }
    return [self isEditing];;
}

-(IBAction)dismissKeyboard1:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)donebutton:(id)sender
{
    
    if ([self SinupValidation])
    {
        [self SinInRequest];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //self.view.userInteractionEnabled = NO;
        
        
        
    }
}
-(BOOL)SinupValidation
{
    UIAlertView *alert;
    NSString *regex1 =@"\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    
    
        if (_passwordtxt.text == nil || [_passwordtxt.text length] == 0 || [[_passwordtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
        else if (_phonetxt.text == nil || [_phonetxt.text length] == 0 || [[_phonetxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Phone Number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
    
        else if (_companytxt.text == nil || [_companytxt.text length] == 0 || [[_companytxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Company Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
    
    
    
    
    return TRUE;
}


- (void)SinInRequest {
    
    NSString *accountStr = @"1";
    
    [SVProgressHUD showWithStatus:@"Creating your Account.." maskType:SVProgressHUDMaskTypeBlack];
  NSDictionary *dictionary = @{@"action":@"create",@"name":@{@"first":_firstnametxt.text,@"last":_lastnametxt.text,@"suffix":_suffixtxt.text},@"email":_emailtxt.text,@"password":_passwordtxt.text,@"agencyCode":[_dataDict valueForKey:@"pcc"], @"accountNumber": [NSNumber numberWithInt:1] ,@"detail":@{@"company":_companytxt.text,@"phone":_phonetxt.text}};
    NSString *urlStr = @"UpdateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
    NSLog(@"%@DICT",dictionary);
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([dataDic valueForKey:@"fault"]) {
               
               
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Attention" message:[dataDic valueForKey:@"fault"] preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                            {
                                                
                                                LoginViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                
                                                [self.navigationController pushViewController:myView animated:NO];
                                                
                                                
                                                
                                            }
                                    ]];
              
                
                [self presentViewController:alertController animated:YES completion:nil];

            

            }
            else{
                [TravelConstantClass singleton].travelerID = [dataDic valueForKey:@"travelerID"];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                // saving an NSString
                [prefs setObject: [TravelConstantClass singleton].travelerID forKey: @"travelerID"];
                [prefs setObject: _Accounttxt.text forKey: @"Accountno"];
                [prefs synchronize];
                NSLog(@"user%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
                // [TravelConstantClass singleton].AccountNo =_Accounttxt.text ;
                
                
                SWRevealViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
                
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            
            [SVProgressHUD dismiss];
            
        });
        
        
        
        if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
            
            [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
            
            
        }
        
        
    }
    
}





@end
