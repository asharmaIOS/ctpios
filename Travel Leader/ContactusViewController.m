//
//  ContactusViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/18/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ContactusViewController.h"
#import "SWRevealViewController.h"
#import <MessageUI/MessageUI.h>
#import "SVProgressHUD.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"
#import "Reachability.h"
#import "DemoMessagesViewController.h"

#import "TravelConstantClass.h"


@interface ContactusViewController ()<UIGestureRecognizerDelegate>
{
    NSArray *toRecipents;
    NSString *email;
    NSString *call;
    
    NSMutableArray* result;
    // NSMutableArray *detail;
    NSString *account;
    NSDictionary *AccDict;
}
@property (weak, nonatomic) IBOutlet UIView *viewSecond;
@property (nonatomic, strong) MFMailComposeViewController *mailComposer;
@property (strong, nonatomic) NSMutableArray *detail;
@end

@implementation ContactusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    
    [self.navigationController.navigationBar setHidden: NO];
     [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    account = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"account"]];
    NSLog(@"acc12345------------%@",account);
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self setupLeftMenuButton];
    
    
    if (networkStatus == NotReachable) {
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
      NSDictionary  *dataDic = [userDef objectForKey:@"contact"];
        
        if (dataDic.allKeys !=nil) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *dataArray = [dataDic valueForKey:@"accounts"];
                
                for (int i=0; i< dataArray.count; i++) {
                    
                    if ([[dataArray objectAtIndex:i]valueForKey:@"number"] == account) {
                        
                        NSDictionary *dataDic1 = [[dataArray objectAtIndex:i]valueForKey:@"detail"];
                        
                        [self loadData:dataDic1 complete:dataDic];
                        
                    }
                    
                }
                
            
            });

            
        }
        else {
            
             [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];

        }
       
        
    }
    
    
    else {
        [SVProgressHUD showWithStatus:@"Processing..." maskType:SVProgressHUDMaskTypeBlack];

        [self performSelector:@selector(contactUsApi) withObject:self afterDelay:1.0 ];
        
        
       // [self contactUsApi];

        
    }
    
    
    
    // typearr = [[NSMutableArray alloc] init];
   
    
}






-(void)flipVie
{
DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    //    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    //    textButton.title = @"CONTACT US";
    //    // textButton.tintColor = [UIColor whiteColor];
    //    textButton.tintColor = [UIColor whiteColor];
    //    self.navigationItem.leftBarButtonItems = @[searchItem];
    //    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"CONTACT US"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backAction
{
    
    [self backButtonTap:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}


-(void)backButtonTap:(id)sender
{
    
    // [self dismissViewControllerAnimated:YES completion:nil] ;
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
            home = (HomeViewController*)vc;
        }
    }
    
    if (home == nil) {
        // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    
}

- (void)contactUsApi {
    
   // [SVProgressHUD showWithStatus:@"Processing..." maskType:SVProgressHUDMaskTypeBlack];
    NSLog(@"Agencycode === %@",[TravelConstantClass singleton].Agencycode);

    NSDictionary *dictionary = @{@"action":@"read",@"agencyCode":[TravelConstantClass singleton].Agencycode};

    NSString *urlStr = @"UpdateAgency";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];

    
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
    
       
            
            NSArray *dataArray = [dataDic valueForKey:@"accounts"];
            
            for (int i=0; i< dataArray.count; i++) {
                
                if ([[NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:i]valueForKey:@"number"]] isEqualToString:account]) {
                    
                    NSDictionary *dataDic1 = [[dataArray objectAtIndex:i]valueForKey:@"detail"];
                    
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                    [self loadData:dataDic1 complete:dataDic];
                         
                          });
                    
                }
                
            }
              
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setObject:dataDic forKey:@"contact"];
        [userDef synchronize];
            
        
        
        if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
                
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
                
                
            }

            
     
        
        
        
    }
    
}









-(void)loadData:(NSDictionary *)dataDic complete:(NSDictionary *)completeDic {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.frame = CGRectMake(0, 44, screenWidth , screenHeight-44);
    [self.view addSubview:scrollView];
    
    CGFloat heightForImageview = 50;
    
        
        if ([dataDic valueForKey:@"phone"]) {
            
            UIImageView *imageBackgroundView = [[UIImageView alloc] init];
            imageBackgroundView.frame = CGRectMake(10, heightForImageview, screenWidth - 20 , 80);
            imageBackgroundView.backgroundColor = [UIColor clearColor];
            imageBackgroundView.userInteractionEnabled = YES;
            
            imageBackgroundView.image = [UIImage imageNamed:@"white-bg.png"];
            //imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
            [scrollView addSubview: imageBackgroundView];
            
            UILabel *headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 9 , 300, 22)];
            headingLbl.font = [UIFont systemFontOfSize:18];
            headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
            headingLbl.textAlignment = NSTextAlignmentLeft;
            // NSString *strText = [dataDic valueForKey:@"phone"];
            headingLbl.text = @"PHONE NUMBER";
            
            
            UILabel *valueLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 35 , 300, 30)];
            valueLbl.font = [UIFont systemFontOfSize:18];
            valueLbl.textColor = [UIColor blackColor];
            valueLbl.textAlignment = NSTextAlignmentLeft;
            valueLbl.userInteractionEnabled = YES;
            valueLbl.tag =  1;

            // NSString *strText = [dataDic valueForKey:@"phone"];
            valueLbl.text = [dataDic valueForKey:@"phone"];
            
            UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneCall:)];
            lblAction.delegate = self;
            lblAction.numberOfTapsRequired = 1;
            [valueLbl addGestureRecognizer:lblAction];
            
            
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 9 , 25, 25)];
            imageView.image = [UIImage imageNamed:@"phone.png"];
            
            [imageBackgroundView addSubview:valueLbl];
            [imageBackgroundView addSubview:headingLbl];
            [imageBackgroundView addSubview:imageView];
            
            heightForImageview = heightForImageview + 78 + 20;
            
        }
        
        if ([dataDic valueForKey:@"ahphone"]){
            
            
            UIImageView *imageBackgroundView = [[UIImageView alloc] init];
            imageBackgroundView.frame = CGRectMake(10, heightForImageview, screenWidth - 20 , 80);
            imageBackgroundView.backgroundColor = [UIColor clearColor];
            imageBackgroundView.image = [UIImage imageNamed:@"white-bg.png"];
            imageBackgroundView.userInteractionEnabled = YES;
            //imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
            [scrollView addSubview: imageBackgroundView];
            
            
            UILabel *headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 9 , 300, 22)];
            headingLbl.font = [UIFont systemFontOfSize:18];
            headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
            headingLbl.textAlignment = NSTextAlignmentLeft;
            //  NSString *strText = [dataDic valueForKey:@"ahphone"];
            headingLbl.text = @"AFTER HOUR'S PHONE NUMBER";
            
            UILabel *valueLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 35 , 300, 30)];
            valueLbl.font = [UIFont systemFontOfSize:18];
            valueLbl.textColor = [UIColor blackColor];
            valueLbl.textAlignment = NSTextAlignmentLeft;
            valueLbl.tag =  1;

            valueLbl.userInteractionEnabled = YES;

            valueLbl.text = [dataDic valueForKey:@"ahphone"];
            
            UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneCall:)];
            lblAction.delegate = self;
            lblAction.numberOfTapsRequired = 1;
            [valueLbl addGestureRecognizer:lblAction];
            
            
            
            
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 9 , 25, 25)];
            imageView.image = [UIImage imageNamed:@"phone.png"];
            
             [imageBackgroundView addSubview:valueLbl];
            [imageBackgroundView addSubview:headingLbl];
            [imageBackgroundView addSubview:imageView];
            
             heightForImageview = heightForImageview + 78 + 20;
            
        }
         if ([dataDic valueForKey:@"email"]){
            
             
             UIImageView *imageBackgroundView = [[UIImageView alloc] init];
             imageBackgroundView.userInteractionEnabled = YES;
             imageBackgroundView.frame = CGRectMake(10, heightForImageview, screenWidth - 20 , 80);
             imageBackgroundView.backgroundColor = [UIColor clearColor];
             imageBackgroundView.image = [UIImage imageNamed:@"white-bg.png"];
             //imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
             [scrollView addSubview: imageBackgroundView];
             
            UILabel *headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 9 , 300, 22)];
            headingLbl.font = [UIFont systemFontOfSize:18];
             headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
            headingLbl.textAlignment = NSTextAlignmentLeft;
            // NSString *strText = [dataDic valueForKey:@"email"];
            headingLbl.text = @"EMAIL ADDRESS";
            
             
             UILabel *valueLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 40 , 300, 22)];
             valueLbl.font = [UIFont systemFontOfSize:18];
             valueLbl.textColor = [UIColor blackColor];
             valueLbl.textAlignment = NSTextAlignmentLeft;
             // NSString *strText = [dataDic valueForKey:@"phone"];
             valueLbl.text = [dataDic valueForKey:@"email"];
             valueLbl.userInteractionEnabled = YES;
             
             /// Gesture to open website
             UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(email:)];
             lblAction.delegate = self;
             lblAction.numberOfTapsRequired = 1;
             [valueLbl addGestureRecognizer:lblAction];
             
            
            
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 9 , 22, 22)];
            imageView.image = [UIImage imageNamed:@"email.png"];
            
            [imageBackgroundView addSubview:valueLbl];
            [imageBackgroundView addSubview:headingLbl];
            [imageBackgroundView addSubview:imageView];
            
             heightForImageview = heightForImageview + 78 + 20;
            
        }
         if ([dataDic valueForKey:@"company"]){
            
             UIImageView *imageBackgroundView = [[UIImageView alloc] init];
             imageBackgroundView.frame = CGRectMake(10, heightForImageview, screenWidth - 20 , 80);
             imageBackgroundView.userInteractionEnabled = YES;
             imageBackgroundView.backgroundColor = [UIColor clearColor];
             imageBackgroundView.image = [UIImage imageNamed:@"white-bg.png"];
             //imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
             [scrollView addSubview: imageBackgroundView];
             
            UILabel *headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(65, 9 , 300, 22)];
            headingLbl.font = [UIFont systemFontOfSize:18];
             headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
            headingLbl.textAlignment = NSTextAlignmentLeft;
            //NSString *strText = [dataDic valueForKey:@"company"];
            headingLbl.text = @"WEBSITE";
             
             
             UILabel *valueLbl1 = [[UILabel alloc]initWithFrame:CGRectMake(65, 35 , 300, 30)];
             valueLbl1.font = [UIFont systemFontOfSize:18];
             valueLbl1.textColor = [UIColor blackColor];
             valueLbl1.textAlignment = NSTextAlignmentLeft;
             valueLbl1.text = [completeDic valueForKey:@"website"];
             //valueLbl.backgroundColor = [UIColor blueColor];
             valueLbl1.userInteractionEnabled = YES;
             valueLbl1.tag =  1;

             
             /// Gesture to open website
             UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(websiteOpen:)];
             lblAction.delegate = self;
             lblAction.numberOfTapsRequired = 1;
             [valueLbl1 addGestureRecognizer:lblAction];
             
             
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 9 , 22, 22)];
            imageView.image = [UIImage imageNamed:@"globe.png"];
            
             
             
             [imageBackgroundView addSubview:valueLbl1];

            [imageBackgroundView addSubview:headingLbl];
            [imageBackgroundView addSubview:imageView];
             
            heightForImageview = heightForImageview + 78 + 20;
            
            
        }
    
    
    [SVProgressHUD dismiss];
    
}


- (void)phoneCall:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;

    NSString *phoneNumberStr = [label.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *phoneNumber = [phoneNumberStr stringByReplacingOccurrencesOfString:@"-" withString:@""];

    //NSString *phoneNumberStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",phoneNumberStr]];

    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }

}



- (void)websiteOpen:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    
    NSString *str = label.text;
    NSString *websiteStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![websiteStr containsString:@"http://"]|| ![websiteStr containsString:@"https://"] ) {
        websiteStr = [NSString stringWithFormat:@"http://%@",websiteStr];
    }
    
    
   // [self openScheme:websiteStr];
     if( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:websiteStr]])
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteStr] options:@{} completionHandler:nil];
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteStr]];
    
}

//- (void)openScheme:(NSString *)scheme {
//    UIApplication *application = [UIApplication sharedApplication];
//    NSURL *URL = [NSURL URLWithString:scheme];
//
//    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
//        [application openURL:URL options:@{}
//           completionHandler:^(BOOL success) {
//               NSLog(@"Open %@: %d",scheme,success);
//           }];
//    } else {
//        BOOL success = [application openURL:URL];
//        NSLog(@"Open %@: %d",scheme,success);
//    }
//}


-(void)email:(UITapGestureRecognizer *)tapGesture
{
    
    UILabel *label = (UILabel *)tapGesture.view;
    NSString *str = label.text;
    NSString *emailTitle = @"Reservation Help";
    // Email Content
    NSString *messageBody = @"";
    
    toRecipents = [NSArray arrayWithObjects:str,nil];
    //mail
    self.mailComposer = [[MFMailComposeViewController alloc] init];
    self.mailComposer.mailComposeDelegate = self;
    [self.mailComposer setSubject:emailTitle];
    [self.mailComposer setMessageBody:messageBody isHTML:NO];
    [self.mailComposer setToRecipients:toRecipents];
    
    if ([MFMailComposeViewController canSendMail])
    {
        
        [self presentViewController:self.mailComposer animated:YES completion:NULL];
        
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}

@end
