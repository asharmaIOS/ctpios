//
//  ReadMoreDescriptionOfdestinationViewController.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 05/01/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "ReadMoreDescriptionOfdestinationViewController.h"
#import "DemoMessagesViewController.h"



@interface ReadMoreDescriptionOfdestinationViewController ()

{
    UIScrollView *scrollViewForSepcificData;
}

@end

@implementation ReadMoreDescriptionOfdestinationViewController

- (void)viewDidLoad {
    
    [self.navigationController.navigationBar setHidden: NO];
  [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    
    [self setupLeftMenuButton];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)setupLeftMenuButton{
    // for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
//    textButton.title = @"";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItems = @[searchItem];
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}
   // self.navigationItem.rightBarButtonItems = @[textButton];

-(void)viewWillAppear:(BOOL)animated {
    
    
    NSLog(@"dataDic %@", _dataDic);
    // loadMainImage
    
       NSString * imageUrl ;
    if ([[_dataDic valueForKey:@"images"] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *dicImage = [[_dataDic valueForKey:@"images"] valueForKey:@"image"];
        //NSString * imageUrl = [dicImage valueForKey:@"url"];
     
        
        if ([[dicImage valueForKey:@"url"] isKindOfClass:[NSArray class]]) {
            imageUrl = [[dicImage valueForKey:@"url"] objectAtIndex:0];
        }
        else {
            imageUrl =  [dicImage valueForKey:@"url"];
            
        }
    }
    
    
    [self loadMainImage:imageUrl];
    
    [self loadDataAccordingDic];
    
}

-(void)backButton:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
     //[self dismissViewControllerAnimated:YES completion:nil] ;
    
}



- (void)loadMainImage:(NSString *)mainImageStr {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if(mainImageStr.length !=0) {
        
    UIImageView *imageview = [[UIImageView alloc]
                              initWithFrame:CGRectMake(0, 44, screenWidth, 250)];
    
    [self downloadImageWithURL:[NSURL URLWithString: mainImageStr] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            imageview.image = image;
        }
    }];
    
    // imageview.image = mainImage;
    [imageview setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imageview];
        
        
    }
    

    else {
        
        UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 170, screenWidth, 35)];
        namelabel.numberOfLines = 2;
        namelabel.font = [UIFont boldSystemFontOfSize:22];
        namelabel.textAlignment = NSTextAlignmentCenter;
        //namelabel.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
        namelabel.text = @"No image available";
        [self.view addSubview:namelabel];
        
    }
    
    UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 300, screenWidth, 35)];
    namelabel.numberOfLines = 2;
    namelabel.font = [UIFont boldSystemFontOfSize:20];
    namelabel.textAlignment = NSTextAlignmentLeft;
   //namelabel.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
    namelabel.text = [_dataDic valueForKey:@"title"];
    
    [self.view addSubview:namelabel];
    
    
}

-(void) loadDataAccordingDic {
    
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.width;

    
    CGFloat finalHeight = 0;
    
    
    if (!scrollViewForSepcificData) {
        scrollViewForSepcificData = [[UIScrollView alloc] init];
        scrollViewForSepcificData.frame = CGRectMake(0, 329, screenWidth , 500);
        [self.view  addSubview: scrollViewForSepcificData];
    }
    
    for(UIView *subview in [scrollViewForSepcificData subviews]) {
        [subview removeFromSuperview];
    }
    
    scrollViewForSepcificData.contentOffset = CGPointMake(0,0);
    
    
  // Metadata design
    
    UIView *imageBackgroundView = [[UIView alloc] init];
    imageBackgroundView.frame = CGRectMake(0, finalHeight, screenWidth , 340);
    imageBackgroundView.backgroundColor = [UIColor clearColor];
    //imageBackgroundView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.5];
    [scrollViewForSepcificData addSubview: imageBackgroundView];
    
    //NSString *trimmed = [foo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    
    if ([[_dataDic valueForKey:@"meta"] isKindOfClass:[NSArray class]]) {
         NSArray *metaArray = [_dataDic valueForKey:@"meta"];
        
        CGFloat heightOfmetaItem = 10 ;
        for (int i = 0; i < metaArray.count; i++) {
            
            NSString *trimmedStr = [[[metaArray objectAtIndex:i]valueForKey:@"meta"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

            
            if (trimmedStr.length!=0) {
                
                UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, heightOfmetaItem +2 , 20, 20)];
                imageView.image = [ self imageAccordingMetaType:[[metaArray objectAtIndex:i] valueForKey:@"type"]];
                
                [imageBackgroundView addSubview:imageView];
                
                
                
                
                UILabel *headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(40, heightOfmetaItem , 200, 22)];
                headingLbl.font = [UIFont systemFontOfSize:18];
                headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
                headingLbl.textAlignment = NSTextAlignmentLeft;
                NSString *strText = [ self stringHeadingAccordingMetaType:[[metaArray objectAtIndex:i] valueForKey:@"type"]];
                if (strText.length != 0) {
                    
                    headingLbl.text = strText;
                }
                else {
                    
                     headingLbl.text = [[metaArray objectAtIndex:i] valueForKey:@"type"];
                }
                if (![[[metaArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"googlePlaceId"]){
                   [imageBackgroundView addSubview:headingLbl];
                }
                
                

                
                UILabel *infoMetaLbl = [[UILabel alloc]initWithFrame:CGRectMake(40, heightOfmetaItem +22 , 320, 22)];
                infoMetaLbl.font = [UIFont systemFontOfSize:17];
                infoMetaLbl.textColor = [UIColor blackColor];
                infoMetaLbl.numberOfLines =3;
                infoMetaLbl.textAlignment = NSTextAlignmentLeft;
                NSString *metaText = [[[metaArray objectAtIndex:i]valueForKey:@"meta"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSString *str = [metaText stringByReplacingOccurrencesOfString:@"\t"
                                                     withString:@""];
                CGFloat height = [self getTextHeightByWidth:str textFont:[UIFont systemFontOfSize:18] textWidth:320];
                
                CGFloat heightOfmetaData = 22;
                if ( height > 22) {
                    heightOfmetaData = height;
                    if ( height > 90) {
                        heightOfmetaData = 75;
                        
                    }
                     infoMetaLbl.frame = CGRectMake(40, heightOfmetaItem + 20, 320, heightOfmetaData);
                }
                
                if (metaText.length != 0) {
                    
                    infoMetaLbl.text = str;
                }
                else {
                    
                    infoMetaLbl.text = [[metaArray objectAtIndex:i] valueForKey:@"meta"];
                }
                
                
                /// appliying gestures
                /// appliying gestures
                
                
                
                if ([[[metaArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"address"]){
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAddress:)];
                    lblAction.delegate = self;
                    lblAction.numberOfTapsRequired = 1;
                    infoMetaLbl.userInteractionEnabled = YES;
                    infoMetaLbl.tag = i ;
                    [infoMetaLbl addGestureRecognizer:lblAction];
                    
                    // [imageBackgroundView addSubview:headingLbl];
                }
                
                if ([[[metaArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"phone"]){
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneCall:)];
                    lblAction.delegate = self;
                    lblAction.numberOfTapsRequired = 1;
                    infoMetaLbl.userInteractionEnabled = YES;
                    infoMetaLbl.tag = i ;
                    [infoMetaLbl addGestureRecognizer:lblAction];
                    
                   // [imageBackgroundView addSubview:headingLbl];
                }
                
                
                
                if ([[[metaArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"www"]){
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(websiteOpen:)];
                    lblAction.delegate = self;
                    lblAction.numberOfTapsRequired = 1;
                    infoMetaLbl.userInteractionEnabled = YES;
                    infoMetaLbl.tag = i ;
                    [infoMetaLbl addGestureRecognizer:lblAction];
                    
                    // [imageBackgroundView addSubview:headingLbl];
                }
                
                
                
                
                
                
                if (![[[metaArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"googlePlaceId"]){
                    [imageBackgroundView addSubview:infoMetaLbl];

                    if ( height > 22) {
                        heightOfmetaItem = heightOfmetaItem + 90;
                    }
                    else {
                        heightOfmetaItem = heightOfmetaItem + 60;
                    }
                    
                }
                
                
            }
            
        }
        
        imageBackgroundView.frame = CGRectMake(0, finalHeight, screenWidth , heightOfmetaItem-10);
        finalHeight = heightOfmetaItem;
    }
    
    UILabel *lineLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, finalHeight + 2 ,screenWidth -20 , 1)];
    lineLbl.backgroundColor = [UIColor lightGrayColor];
    [scrollViewForSepcificData addSubview:lineLbl];
    
    

    
    if ([_dataDic valueForKey:@"description"] != nil) {

        UITextView *textView = [[UITextView alloc]init ];
        textView.userInteractionEnabled = NO;
        textView.frame = CGRectMake(7, finalHeight + 15 , screenWidth-14, 300);
        // textView.font = [UIFont fontWithName:@"Lato" size:16];
        textView.backgroundColor = [UIColor clearColor];
        textView.text = [_dataDic valueForKey:@"description"];
        
        
        NSString *textViewText = textView.text;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 07;
        NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:17.0f] };
        [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
        
        textView.attributedText = attributedString;
        textView.frame = CGRectMake(7, finalHeight + 15, screenWidth-14, textView.contentSize.height);
        textView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);

        [scrollViewForSepcificData addSubview:textView];
        finalHeight = finalHeight +textView.contentSize.height;
        
    }
    scrollViewForSepcificData.showsVerticalScrollIndicator = NO;

    if (screenHeight < 737) {
        
        scrollViewForSepcificData.contentSize = CGSizeMake(screenWidth, finalHeight+250);
        
    }
    
    else {
        
        scrollViewForSepcificData.contentSize = CGSizeMake(screenWidth, finalHeight+25);
        
        
    }
}
- (void)openAddress:(UITapGestureRecognizer *)tapGesture {
    
    UILabel *label = (UILabel* )tapGesture.view;
    // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
    NSArray *metaArray = [_dataDic valueForKey:@"meta"];
    
    NSString *str = [[metaArray objectAtIndex:label.tag]valueForKey:@"meta"];
    
    NSString *meadiumStr = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    if (meadiumStr) {
        
        
        
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
            NSString *urlString = [NSString stringWithFormat:@"comgooglemaps://?q=%@",meadiumStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        } else {
            NSString *string = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",meadiumStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
        }
        
        
    }
}

- (void)phoneCall:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
   // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
     NSArray *metaArray = [_dataDic valueForKey:@"meta"];

    NSString *str = [[metaArray objectAtIndex:label.tag]valueForKey:@"meta"];
    NSString *phoneNumberStr = [str stringByReplacingOccurrencesOfString:@" " withString:@""];

     //NSString *phoneNumberStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",phoneNumberStr]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
    
}


- (void)websiteOpen:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
    NSArray *metaArray = [_dataDic valueForKey:@"meta"];
    
    NSString *str = [[metaArray objectAtIndex:label.tag]valueForKey:@"meta"];
    NSString *websiteStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![websiteStr containsString:@"http://"]|| ![websiteStr containsString:@"https://"] ) {
        websiteStr = [NSString stringWithFormat:@"http://%@",websiteStr];
    }
    
    
    [self openScheme:websiteStr];
   // if( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:websiteStr]])
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteStr] options:@{} completionHandler:nil];

       // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteStr]];
    
}

- (void)openScheme:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{}
           completionHandler:^(BOOL success) {
               NSLog(@"Open %@: %d",scheme,success);
           }];
    } else {
        BOOL success = [application openURL:URL];
        NSLog(@"Open %@: %d",scheme,success);
    }
}
    
- (CGFloat)getTextHeightByWidth:(NSString*)text textFont:(UIFont*)textFont textWidth:(float)textWidth {
    
    if (!text) {
        return 0;
    }
    CGSize boundingSize = CGSizeMake(textWidth, CGFLOAT_MAX);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: textFont }];
    
    CGRect rect = [attributedText boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize requiredSize = rect.size;
    return requiredSize.height;
}


- (UIImage *)imageAccordingMetaType :(NSString *)str {
    UIImage *finalImage;
    
    if ([str isEqualToString:@"address"]) {
        finalImage = [UIImage imageNamed:@"locaton.png"];
        
    }
    
    else if([str isEqualToString:@"email"]) {
        finalImage = [UIImage imageNamed:@"email.png"];

    }
    else if([str isEqualToString:@"phone"]) {
        finalImage = [UIImage imageNamed:@"phone.png"];

    }
    else if([str isEqualToString:@"www"]) {
        finalImage = [UIImage imageNamed:@"website.png"];

    }
    else if([str isEqualToString:@"openinghours"]) {
        finalImage = [UIImage imageNamed:@"opening-time.png"];

    }
    else if([str isEqualToString:@"subway"]) {
        finalImage = [UIImage imageNamed:@"subway.png"];

    }
    else if([str isEqualToString:@"tickets"]) {
        finalImage = [UIImage imageNamed:@"ticket.png"];

    }
    else if([str isEqualToString:@"moreinfo"]) {
        finalImage = [UIImage imageNamed:@"locaton.png"];

    }
    
    
    
    return finalImage;
}




- (NSString *)stringHeadingAccordingMetaType :(NSString *)str {
    NSString *finalStr;
    
    if ([str isEqualToString:@"address"]) {
        finalStr = @"Address";
        
    }
    
    else if([str isEqualToString:@"email"]) {
        finalStr = @"E-Mail";
        
    }
    else if([str isEqualToString:@"phone"]) {
        finalStr = @"Phone";
        
    }
    else if([str isEqualToString:@"www"]) {
        finalStr = @"Webpage";
        
    }
    else if([str isEqualToString:@"openinghours"]) {
        finalStr = @"Opening Hours";
        
    }
    else if([str isEqualToString:@"subway"]) {
        finalStr = @"Subway";
        
    }
    else if([str isEqualToString:@"tickets"]) {
        finalStr = @"Tickets";
        
    }
    else if([str isEqualToString:@"moreinfo"]) {
        finalStr = @"More Information";
        
    }
    
    
    
    return finalStr;
}



- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    if (url!= nil ){
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if ( !error )
                                   {
                                       UIImage *image = [[UIImage alloc] initWithData:data];
                                       completionBlock(YES,image);
                                   } else{
                                       completionBlock(NO,nil);
                                   }
                               }];
    }
}




@end
