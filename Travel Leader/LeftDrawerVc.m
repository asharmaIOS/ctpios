

//
//  LeftDrawerVc.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 5/4/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "LeftDrawerVc.h"
#import "FlightViewController.h"
#import "DemoMessagesViewController.h"
#import "AboutusViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "ContactusViewController.h"
#import "ProfileVC.h"
#import "XMLReader.h"
#import "SearchDestinationVC.h"
#import "TravelConstantClass.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "NotificationVC.h"

@interface LeftDrawerVc () <UITableViewDataSource,UITableViewDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *menutbl;

@end

@implementation LeftDrawerVc


- (void)viewDidLoad {
    
    
    self.navigationController.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //Return no. of Sections
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Return no. of rows in section
    return 7;// (menu.count);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int height1;
    
    height1=50;
    NSLog(@"Clicked");
    
    return height1; // Normal height
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIImage *thumbnail;
    _menutbl.backgroundColor = [UIColor clearColor];
    _menutbl.opaque = NO;
    _menutbl.backgroundView = nil;
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.font = [UIFont systemFontOfSize:17];
    
    switch (indexPath.row) {
            
        case 0:
            cell.textLabel.text = @"Profile";
            thumbnail = [UIImage imageNamed:@"user-round.png"];
            break;
            
        case 1:
            cell.textLabel.text = @"My Trips";
            thumbnail = [UIImage imageNamed:@"map.png"];
            break;
            
        case 2:
            cell.textLabel.text = @"Live Chat";
            thumbnail = [UIImage imageNamed:@"chat.png"];
            break;
        case 3:
            cell.textLabel.text = @"About Us";
            thumbnail = [UIImage imageNamed:@"user.png"];
            break;
            
        case 4:
            cell.textLabel.text = @"Contact Us";
            thumbnail = [UIImage imageNamed:@"phone.png"];
            break;
        case 5:
            cell.textLabel.text = @"Notifications";
            thumbnail = [UIImage imageNamed:@"bell.png"];
            break;
        case 6:
            cell.textLabel.text = @"Destination Guide";
            thumbnail = [UIImage imageNamed:@"destination.png"];
            break;
        default:
            break;
    }
    CGSize itemSize = CGSizeMake(25, 25);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [thumbnail drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    //cell.imageView.image = thumbnail ;

    UIGraphicsEndImageContext();
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SWRevealViewController *revealController = self.revealViewController;

    
    if (indexPath.row==0)
    {
//        [self.revealViewController revealToggleAnimated:YES];
//        ProfileVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"profilevc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//              [self.navigationController pushViewController:myView animated:true];
//
//
//        [self presentViewController:lNavC animated:NO completion:^{
//        }];
        
        
//        ProfileVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"profilevc"];
//
//        [self.navigationController pushViewController:myView animated:true];
        
//        UINavigationController *frontVC = (UINavigationController *)self.revealViewController.frontViewController;
//        [frontVC pushViewController: myView animated:NO];
//        [self.revealViewController pushFrontViewController:frontVC animated:YES];
        
        ProfileVC *myView1 = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
               
        [self.navigationController pushViewController:myView1 animated:YES];
    }
    
    
    if (indexPath.row==1)
    {
       // SWRevealViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        HomeViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"swvc"];
        

        [self.navigationController pushViewController:myView animated:YES];
        
    }
    
    if (indexPath.row==2)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
        
        DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    
    
    if (indexPath.row==3)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        AboutusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutvc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
        
//        AboutusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutvc"];
//
//
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:myView];
//        [revealController pushFrontViewController:navigationController animated:YES];
        
//        
        AboutusViewController *myView2 = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutusViewController"];


      //  dispatch_async(dispatch_get_main_queue(), ^{
           [self.navigationController pushViewController:myView2 animated:true];
       // });
//        [self.navigationController pushViewController:myView animated:true];
        
    }
    if (indexPath.row==4)
    {
       // [self.revealViewController revealToggleAnimated:NO];
        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];

//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//
//        [self presentViewController:lNavC animated:YES completion:^{
//               }];
       // [self presentViewController:lNavC animated:YES completion:^{
       // }];

        //  dispatch_async(dispatch_get_main_queue(), ^{

        [self.navigationController pushViewController:myView animated:true];

     //   });
//        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];
//
//        [self.navigationController pushViewController:myView animated:true];
    }
    if (indexPath.row==5)
    {
        //        [self.revealViewController revealToggleAnimated:NO];
        //        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];
        //
        //        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
        //        [self presentViewController:lNavC animated:YES completion:^{
        //        }];
        //
        NotificationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    
    if (indexPath.row==6)
    {
       
        SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    
        
        
        
    
}




- (IBAction)destinationbtn:(id)sender
{
//    SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
//
//    UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//    [self presentViewController:lNavC animated:YES completion:^{
//    }];
    
    SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
    
    [self.navigationController pushViewController:myView animated:true];
}

- (IBAction)logoutbtn:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to logout" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"travelerID"];
                                     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"apitype"];
                                
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                    
                              //      [AppDelegate sharedAppDelegate].window.rootViewController = login;
                                    [self.navigationController pushViewController:login animated :NO];
                                    
                                    
                                }
                                ]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // action 2
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];

    
}
@end
