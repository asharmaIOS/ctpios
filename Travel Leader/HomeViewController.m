//
//  HomeViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "homeCell.h"
#import "FlightViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "Reachability.h"

#import <Foundation/Foundation.h>
#import "TripViewController.h"
#import "ValidationAndJsonVC.h"
#import "DemoMessagesViewController.h"






@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *pnrarr;
    NSMutableDictionary *pnrdic;
    NSString *itdate;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UIView *emptyview;
@property (weak, nonatomic) IBOutlet UILabel *NoTriplbl;
@property (strong, atomic) NSMutableArray *listArray;
@property (strong, atomic) NSMutableArray *totalArrayPky;

@property (strong, atomic) NSString *startdate;
@property (weak,nonatomic)IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITableView *triplistTabel;

@end

@implementation HomeViewController
@synthesize listArray;
@synthesize startdate;


static SEL extracted() {
    return @selector(revealToggle:);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    NSArray *dataArray  = [userDef objectForKey:@"TripList"];
    
    NSLog(@"dataArray= ==%lu ", (unsigned long)dataArray.count);
    
    NSLog(@"user%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
    
    listArray= [[NSMutableArray alloc]init];
    _totalArrayPky = [[NSMutableArray alloc]init];
    pnrarr = [[NSArray alloc]init];
    pnrdic = [[NSMutableDictionary alloc]init];
    self.parentViewController.navigationController.navigationBarHidden= YES;
    
    //[self.revealViewController rightRevealToggleAnimated:YES];

    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController)
    {
        _barButton.target = self.revealViewController;
        _barButton.action = extracted();
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self.navigationController.navigationBar setHidden: NO];
    
     [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
     textButton.tintColor = [UIColor whiteColor];
    
     [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];


    UILabel *strLabel = [[UILabel alloc]init];
    strLabel.textColor = [UIColor whiteColor];
    strLabel.text = @"MY TRIPS";
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] initWithCustomView:strLabel];
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[_barButton, textButton1];;

    
    // For Referece the table by scolling
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.triplistTabel addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [_triplistTabel setShowsVerticalScrollIndicator:NO];

    self.emptyview.hidden = YES;

    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
       NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
       
       if (networkStatus == NotReachable ||  [AppDelegate sharedAppDelegate].launchTag == 1) {
           [self offlineDatashow];
       }
       else {
           
           [SVProgressHUD showWithStatus:@"Your Trips.." maskType:SVProgressHUDMaskTypeBlack];
            [self performSelector:@selector(getListPnr) withObject:self afterDelay:0.1 ];
       }
   
    
    self.triplistTabel.tableFooterView = [UIView new];
    
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

-(void)offlineDatashow {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable ||  [AppDelegate sharedAppDelegate].launchTag == 1) {
        
        if (networkStatus == NotReachable) {
             [ValidationAndJsonVC displayAlert:@"Showing saved data" HeaderMsg:@"No internet connection found!"];
        }
       

        // [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];
        self.emptyview.hidden = YES;
        if ([listArray count]) {
             [listArray removeAllObjects];
        }
        
       
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        listArray = [userDef objectForKey:@"PkeyArray"];
        if( listArray.count ==0 ) {
            
            self.emptyview.hidden = NO;
            
        }
        [self.triplistTabel reloadData];
        
        [SVProgressHUD dismiss];
        
    }
    
    
   
}

-(void)refreshData
{
    //Put your logic here
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    
    if (networkStatus == NotReachable) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Opps" message:@"Please check your internet" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        [refreshControl endRefreshing];
                                        [self.triplistTabel reloadData];
                                    }
                                    ]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else {
        
        [self getListPnr];
        
        
        [refreshControl endRefreshing];
        [self.triplistTabel reloadData];
        
    }
}


#pragma mark -  method for GetlistPNR



- (void)getListPnr {
    
    
    
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//
//    if (networkStatus == NotReachable) {
//
//        // [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];
//        self.emptyview.hidden = YES;
//        [listArray removeAllObjects];
//
//        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
//        listArray = [userDef objectForKey:@"PkeyArray"];
//        if( listArray.count ==0 ) {
//
//            self.emptyview.hidden = NO;
//
//        }
//        [self.triplistTabel reloadData];
//
//    }
//
//
//    else {
        
        if ([listArray count]) {
             [listArray removeAllObjects];
        }
        
        if ([_totalArrayPky count]) {
                [_totalArrayPky removeAllObjects];
           }
        
        [SVProgressHUD showWithStatus:@"Your Trips.." maskType:SVProgressHUDMaskTypeBlack];
        self.emptyview.hidden = YES;
        
        NSDictionary *dictionary = @{@"travelerID":[TravelConstantClass singleton].travelerID};
        
        NSString *urlStr = @"ChatListPNRs";
        
        NSDictionary *dataDic = [ValidationAndJsonVC getParsingData:dictionary GetUrl:urlStr GetTimeinterval:30];
        
        
        
        if (dataDic.allKeys != nil) {
            // [SVProgressHUD dismiss];
            
            listArray = [dataDic valueForKey :@"data"];
            
            if (listArray.count ==0) {
                
                self.emptyview.hidden = NO;
                self.NoTriplbl.text = @"You do not have any upcoming trip yet";
                [SVProgressHUD dismiss];
                
            }
            
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.triplistTabel reloadData];
                [SVProgressHUD dismiss];
                
                
                [self getProfile];
            });
            
        }
        
        
        else {
            
            /// For server response nil
            
            self.emptyview.hidden = YES;
            [listArray removeAllObjects];
            
            NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
            listArray = [userDef objectForKey:@"PkeyArray"];
            if( listArray.count ==0 ) {
                
                self.emptyview.hidden = NO;
                
            }
            [self.triplistTabel reloadData];
            
            
           // [self getProfile];
            
            
            [SVProgressHUD dismiss];
            
        }
    }
    
//}


- (void)callBackgroundFunctionForLocaldatabase   {
    
    
    if ([AppDelegate sharedAppDelegate].launchTag == 0) {
        
        for (int i=0; i < listArray.count ; i++) {
            
            [self chatGetPNRApi:[[[listArray objectAtIndex:i]valueForKey:@"pnr"]valueForKey:@"pkey"]];
        }
        
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setObject:_totalArrayPky forKey:@"TripList"];
        // [userDef synchronize];
        
        NSLog(@"_totalArrayPky======%d",_totalArrayPky.count);
        
        [userDef setObject:listArray forKey:@"PkeyArray"];
        [userDef synchronize];
        
        [AppDelegate sharedAppDelegate].launchTag = 1;
        
    }
    
    [SVProgressHUD dismiss];
    
    
}




- (void)getProfile {
    
    NSDictionary *dictionary = @{@"action":@"read",@"travelerID":[TravelConstantClass singleton].travelerID};
    
    NSString *urlStr = @"UpdateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        //  [SVProgressHUD dismiss];
        
        
        //NSLog(@"pnr%@",listArray);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [TravelConstantClass singleton].Agencycode = [dataDic valueForKey:@"agencyCode"];
            NSLog(@"agencycode%@",[TravelConstantClass singleton].Agencycode);
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:[dataDic valueForKey:@"accountNumber"]  forKey: @"account"];
            [prefs synchronize];
            
            //  [SVProgressHUD dismiss];
            
            
            
        });
        
        
        [self callBackgroundFunctionForLocaldatabase ];
        
        if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
            
            [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
            
            
        }
        
        
    }
    
}





- (void)chatGetPNRApi :(NSString * )pkeyStr {
    
    //[SVProgressHUD showWithStatus:@"Trips.. " maskType:SVProgressHUDMaskTypeBlack];
    
    NSDictionary *dictionary = @{@"pkey":pkeyStr};
    
    NSString *urlStr = @"ChatGetPNR";
    NSDictionary *dataDic = [ValidationAndJsonVC getParsingData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
    
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
        [_totalArrayPky addObject:[dataDic valueForKey :@"pnr"]];
        NSLog(@"_totalArrayPky===== %lu %@",(unsigned long)_totalArrayPky.count,_totalArrayPky);
        
        //listArray = [[dataDic valueForKey :@"pnr"]valueForKey:@"date"];
        
        //        filterrr =  [listArray valueForKey:@"formatteddate"];
        //
        //        NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:filterrr];
        //        filterrr = [[NSMutableArray alloc] initWithArray:[mySet array]];
        //
        //        NSLog(@"filterarr%@",filterrr);
        
        
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //
        //            if (listArray  == nil)
        //
        //            {
        //                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
        //
        //            }
        //
        //        });
        
    }
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self.revealViewController revealToggleAnimated:YES];
        
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return listArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *background = nil;
    background = [UIImage imageNamed:@"grey-bg-3x.png"];
    return background;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"homecell";
    homeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[homeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
//    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
//    cellBackgroundView.image = background;
//    cell.backgroundView = cellBackgroundView;
    
    
    NSDictionary *pnrDict = listArray[indexPath.section];
    
    
    cell.flightlbl.text = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"pnr"]valueForKey:@"recordlocator"]];
    
    NSString *upperCaseStr = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"pnr"]valueForKey:@"destination"]].uppercaseString;
    cell.destinationlbl.text = upperCaseStr;
    
    
    NSString *dateString = [[pnrDict objectForKey:@"pnr"]valueForKey:@"itinstart"];
    NSArray *component = [dateString componentsSeparatedByString:@" "];
    cell.datelb.text = dateString;
    
//    cell.datelbl.text = component[0];
//    cell.monthlbl.text = component[1];
//    cell.yearlbl.text = component[2];
    
    NSString *enddateString = [[pnrDict valueForKey:@"pnr"]valueForKey:@"itinend"];
    NSArray *components = [enddateString componentsSeparatedByString:@" "];
    
    //cell.datelb.text = enddateString;
    cell.datelbl.text = enddateString;
//    cell.monthlb.text = components[1];
//    cell.yearlb.text = components[2];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController)
    {
        _barButton.target = self.revealViewController;
        _barButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    return  cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    NSDictionary *dataDict = listArray[indexPath.section];
    
    [TravelConstantClass singleton].pkey = [[dataDict valueForKey:@"pnr"]valueForKey:@"pkey"];
    
    NSLog(@"pkey%@",[TravelConstantClass singleton].pkey);
    
    TripViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mytrip"];
    
    vc.dataDic = [dataDict valueForKey:@"pnr"];
     vc.destinationString = [[vc.dataDic valueForKey:@"destination"] uppercaseString];
    [self.navigationController pushViewController:vc animated:YES];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
