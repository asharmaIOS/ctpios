//
//  AppDelegate.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 3/31/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "AppDelegate.h"
#import "Constant.h"
#import "Reachability.h"
#import "TravelConstantClass.h"
#import "SWRevealViewController.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "ATAppUpdater.h"
#import "NotificationVC.h"

@import Firebase;
@import UserNotifications;
@import Firebase;
@import FirebaseInstanceID;




@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate
@synthesize BASEAPI,BASEAPIPNR,BASEURLNOTIFDETAILS;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOption
{
    
    _launchTag = 0;
    // check version on app store
    ATAppUpdater *updater = [ATAppUpdater sharedUpdater];
    [updater setAlertTitle:NSLocalizedString(@"Corporate Travel Planners", @"Alert Title")];
    [updater setAlertMessage:NSLocalizedString(@"New version %@ available. Update required.", @"Alert Message")];
    [updater setAlertUpdateButtonTitle:@"Update"];
    [updater setAlertCancelButtonTitle:@"Later"];
    //[updater setDelegate:self]; // Optional
    [updater showUpdateWithConfirmation];
    /// code for Firebase notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefressCallback:) name:kFIRInstanceIDTokenRefreshNotification object:nil];

    [FIRApp configure];
    
    UNUserNotificationCenter.currentNotificationCenter.delegate = self;
    // request permission for notification from user
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }


    // request a device token from APNS server
    [application registerForRemoteNotifications];

    NSString *referssToken = [[FIRInstanceID instanceID] token];
    NSLog(@"launch ====%@",referssToken);
    [TravelConstantClass singleton].FirebaseToken = referssToken;




    
    
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//    
//    if (networkStatus == NotReachable) {
//        UIAlertView *alertInvoice = [[UIAlertView alloc]initWithTitle:nil message:@"Please connect to the internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
//        [alertInvoice show];
//    }
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *serverStr = [prefs objectForKey:@"apitype"];
    
   
    
    
  //  if ( [serverStr isEqualToString:@"live"]) {
//        
////        self.BASEAPI = @"http://107.1.185.118/api/v1/chat/";
////        self.BASEAPIPNR = @"http://107.1.185.18/";
        
        self.BASEAPI = @"https://kryptos.greaves.biz/api/v1/chat/";
        self.BASEAPIPNR = @"https://kryptosp.greaves.biz/";
       self. BASEURLNOTIFDETAILS = @"https://kryptos.greaves.biz/";
    
//    }
//    else {
//
//        self.BASEAPI = @"https://test-kryptos.wfares.com/api/v1/chat/";
//        self.BASEAPIPNR = @"https://test-kryptos.wfares.com/";
//        self. BASEURLNOTIFDETAILS = @"https://test-kryptos.wfares.com/";
//        
//       
//
//
//    }

    
    
    
    NSString *myString = [prefs objectForKey:@"travelerID"];
   
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    if (myString != NULL)
    {
        HomeViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:vc];
        [TravelConstantClass singleton].travelerID =[[NSUserDefaults standardUserDefaults] objectForKey:@"travelerID"] ;
        
        self.window.rootViewController=navController;
       
    }
    else
    {
   
    }
    
    
    return YES;
}


+ (AppDelegate *)sharedAppDelegate{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [FIRMessaging messaging].shouldEstablishDirectChannel = false;

}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    _launchTag = 0;

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   // [self connectTofirebase];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark -  firebase code




#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Travel_Leader"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    NSLog(@"userInfo=>%@", userInfo);
    
    NSString *jsonString = [userInfo valueForKey:@"detail"];
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"%@",[json objectForKey:@"notificationText"]);


    NSLog(@"type=======%@",[json valueForKey:@"gate"]);
    
    if ([[json valueForKey:@"type"] isEqualToString:@"Connection Information Gate Change"]) {
        
        NSDictionary *dic = [json valueForKey:@"flight"];
        
        NSString *flightnumber = [dic valueForKey:@"flightNumber"];
        NSString * currentgateStr = [[json valueForKey:@"gate"] valueForKey:@"current"];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:flightnumber forKey: @"flightNo"];
        [prefs setObject:currentgateStr forKey: @"currentGate"];
        [prefs synchronize];
        
        NSLog(@"%@- -%@",flightnumber,currentgateStr);
    }
    
    
    NotificationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NotificationVC"];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:vc];
    self.window.rootViewController=navController;

}


-(void)tokenRefressCallback:(NSNotification *)notification{
    NSString *referssToken = [[FIRInstanceID instanceID] token];
    NSLog(@"referssToken ====%@",referssToken);
    [TravelConstantClass singleton].FirebaseToken = referssToken;

    [self connectTofirebase];
}


-(void)connectTofirebase {
    [FIRMessaging messaging].shouldEstablishDirectChannel = true;

}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
