//
//  SideSeenImageCell.m
//  Travel Leader
//
//  Created by Gurpreet's on 21/09/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "SideSeenImageCell.h"
#import "ImageCell.h"

@implementation SideSeenImageCell




#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 1;//barsArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    
    //cell.layer.cornerRadius = 5.0;
    
//    cell.lblBarName.text = [[barsArray objectAtIndex:indexPath.row] objectForKey:@"bar_name"];
//    cell.lblBarName.font = [UIFont setFontWithType:kRalewayBold withSize:14];
//    
//    cell.lblBarLocation.text = [[[barsArray objectAtIndex:indexPath.row]objectForKey:@"address"] objectForKey:@"zone"];
//    cell.lblBarLocation.textColor = [UIColor lightGrayColor];
//    cell.lblBarLocation.font = [UIFont setFontWithType:kRalewayLight withSize:10];
//    
//    cell.lblBarRating.text = [NSString stringWithFormat:@"%@",[[barsArray objectAtIndex:indexPath.row] objectForKey:@"rating"]];
//    cell.lblBarRating.font = [UIFont setFontWithType:kRalewayBold withSize:14];
//    cell.lblBarRating.textColor = [UIColor colorWithRed:118.0f/255.0f green:94.0f/255.0f blue:162.0f/255.0f alpha:1.0f];
//    
//    NSArray * array = [[barsArray objectAtIndex:indexPath.row] objectForKey:@"pictures"];
//    
//    [cell.imgBar setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://s3.amazonaws.com/barx1/%@",[[array objectAtIndex:0] objectForKey:@"imageUrl"]]] placeholderImage:[UIImage imageNamed:@"facebook.png"]];
//    
    
    
    return cell;
}


/*
- (CGSize)collectionView:(UICollectionView *)collectionView                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   CGSize itemSize;

    
    itemSize = CGSizeMake(210, 170);
    //itemSize = CGSizeMake(180, 155);
   return itemSize;
}
*/

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    [_customDelegate collectionView:barsCollectionView layout:nil barSelection:[[barsArray objectAtIndex:indexPath.row] objectForKey:@"_id"] withBarName:[[barsArray objectAtIndex:indexPath.row] objectForKey:@"bar_name"] withAddress:[[barsArray objectAtIndex:indexPath.row] objectForKey:@"address"] withPictures:[[barsArray objectAtIndex:indexPath.row] objectForKey:@"pictures"]];
}

@end
