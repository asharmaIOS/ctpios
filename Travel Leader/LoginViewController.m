//
//  LoginViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "SinupViewController.h"
#import "ValidationAndJsonVC.h"
#import "AppDelegate.h"
#import "GlobalViewController.h"
#import "PreSignUpVC.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    UIToolbar *keyboardDoneButtonView;
    ValidationAndJsonVC *jsonClass;
    
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //self.emailtxt.text = @"ssati@web-fares.com";
    //************Live**********************
 // self.emailtxt.text = @"testacc2@gmail.com";
  //  self.emailtxt.text = @"icambata@greavestvl.com";
    //******TEST***********
    //self.passwordtxt.text = @"funkmeier";
    //*******Live *********
   // self.passwordtxt.text = @"thejames";
   //  self.passwordtxt.text = @"123456789";
    [self.emailtxt setReturnKeyType:UIReturnKeyDone];
    [self.emailtxt addTarget:self
                      action:@selector(textFieldFinished:)
            forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.passwordtxt setReturnKeyType:UIReturnKeyDone];
    [self.passwordtxt addTarget:self
                         action:@selector(textFieldFinished:)
               forControlEvents:UIControlEventEditingDidEndOnExit];
//
    [self.navigationController.navigationBar setHidden: YES];
    // [self.navigationController.navigationBar setHidden: NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
   
    // setting on page load testserver
    
    //[ValidationAndJsonVC getYourServerConstant:@"test"];
   
    
    [_switchBtn addTarget:self
                  action:@selector(stateChanged:)
        forControlEvents:UIControlEventValueChanged];
    

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    NSString *serverStr = [prefs objectForKey:@"apitype"];

    
    
   // if ( [serverStr isEqualToString:@"live"]) {
    
//              [AppDelegate sharedAppDelegate].BASEAPI = @"http://107.1.185.118/api/v1/chat/";
//              [AppDelegate sharedAppDelegate].BASEAPIPNR = @"http://107.1.185.18/";
    
        [AppDelegate sharedAppDelegate].BASEAPI = @"https://kryptos.greaves.biz/api/v1/chat/";
        [AppDelegate sharedAppDelegate].BASEAPIPNR = @"https://kryptosp.greaves.biz/";
        [AppDelegate sharedAppDelegate]. BASEURLNOTIFDETAILS = @"https://kryptos.greaves.biz/";

    
//    }
//    else {
//
//        [AppDelegate sharedAppDelegate].BASEAPI = @"https://test-kryptos.wfares.com/api/v1/chat/";
//        [AppDelegate sharedAppDelegate].BASEAPIPNR = @"https://test-kryptos.wfares.com/";
//        [AppDelegate sharedAppDelegate]. BASEURLNOTIFDETAILS = @"https://test-kryptos.wfares.com/";
//    }


   
}

- (IBAction)textFieldFinished:(id)sender
{
    [_emailtxt resignFirstResponder];
    [_passwordtxt resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [_switchBtn setOn:NO animated:NO];

    
}

- (void)stateChanged:(UISwitch *)switchState
{
    if ([switchState isOn]) {
        
        [AppDelegate sharedAppDelegate].BASEAPI = @"https://kryptos.greaves.biz/api/v1/chat/";
        [AppDelegate sharedAppDelegate].BASEAPIPNR = @"https://kryptosp.greaves.biz/";
        [AppDelegate sharedAppDelegate]. BASEURLNOTIFDETAILS = @"https://kryptos.greaves.biz/";

        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@"live" forKey: @"apitype"];
        [prefs synchronize];
        
        [ValidationAndJsonVC displayAlert:@"Live Server" HeaderMsg:@""];

        
    } else {
      
        [AppDelegate sharedAppDelegate].BASEAPI = @"https://test-kryptos.wfares.com/api/v1/chat/";
        [AppDelegate sharedAppDelegate].BASEAPIPNR = @"https://test-kryptos.wfares.com/";
        [AppDelegate sharedAppDelegate]. BASEURLNOTIFDETAILS = @"https://test-kryptos.wfares.com/";

        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@"test" forKey: @"apitype"];
        [prefs synchronize];
        
        [ValidationAndJsonVC displayAlert:@"Test Server" HeaderMsg:@""];

    }
}





- (IBAction)textFieldDidBeginEditingMaintainPosition:(UITextField *)textField {
    
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= _scroll.frame;
                         frame.origin.y-=170;
                         _scroll.frame=frame;
                         
                     }];
}

- (IBAction)textFieldShouldReturnMaintainPosition:(UITextField *)textField {
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= _scroll.frame;
                         frame.origin.y+=170;
                         _scroll.frame=frame;
                         
                     }];
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framePromo=_viewlogin.frame;
            framePromo.origin.y+=320;
            _viewlogin.frame=framePromo;
        }];
    }
    return [self isEditing];;
}

- (IBAction)Loginbtn:(id)sender
{
    
    if ([self LoginValidation])
    {
       // [self logInRequest];
        
        [self loginCall];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

//* change test server to lIve server


 
 
- (void)loginCall {
    
    [SVProgressHUD showWithStatus:@"Verifying User..." maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *dictionary = @{@"email":_emailtxt.text,@"password":_passwordtxt.text};
    NSString *urlStr = @"AuthenticateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
     dispatch_async(dispatch_get_main_queue(), ^{
       
            if ([dataDic valueForKey:@"fault"]!= nil) {
                if ([[dataDic valueForKey:@"fault"] isEqualToString:@"notFound"])
                {
                    [ValidationAndJsonVC displayAlert:@"User NotFound" HeaderMsg:@"Attention"];
                    
                }
                else{
                    
                    [ValidationAndJsonVC displayAlert:[dataDic valueForKey:@"fault"] HeaderMsg:@"Attention"];

                }
                
            }
            else{
                
                if ([dataDic valueForKey:@"travelerID"] != nil) {
                    [TravelConstantClass singleton].travelerID = [dataDic valueForKey:@"travelerID"];
                    
                    // calling set firebase token on server
                    
                    if ([dataDic valueForKey:@"travelerID"]) {
                        if ([TravelConstantClass singleton].FirebaseToken != nil ) {
                            [self updateTokenOnServer ];
                        }
                         
                    }
    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    // saving an NSString
                    [prefs setObject: [TravelConstantClass singleton].travelerID forKey: @"travelerID"];
                    [prefs synchronize];
                   // NSLog(@"user%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
                    
                 
                        HomeViewController* home = nil;
                        for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
                            NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
                            if([vc isKindOfClass:[HomeViewController class]]) {
                                home = (HomeViewController*)vc;
                            }
                        }
                        
                        if (home == nil) {
                            // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
                            
                            
                            home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
                            
                            [self.navigationController pushViewController:home animated:YES];
                            // Do we need to push it into navigation controller?
                        }
                    
                    
                }
                else{
                    
                     [ValidationAndJsonVC displayAlert:@"Something Wrong" HeaderMsg:@"Attention"];

                }
                
            }
            
        });

    }
    
}




- (void)updateTokenOnServer {
    
   // [SVProgressHUD showWithStatus:@"Verifying User..." maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *dictionary = @{@"firebase": [TravelConstantClass singleton].FirebaseToken,@"travelerID":[TravelConstantClass singleton].travelerID};
 
    NSString *urlStr = @"SetDevice";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
                
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
                
                
            }

        });
        
    }
    
}




-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.emailtxt.text = @"";
    self.passwordtxt.text = @"";
}

- (IBAction)Registerbtn:(id)sender
{

        PreSignUpVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"PreSignUpVC"];
    
        [self.navigationController pushViewController:myView animated:true];
}

-(BOOL)LoginValidation
{
    UIAlertView *alert;
    NSString *regex1 =@"\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    
    if (_emailtxt.text == nil || [_emailtxt.text length] == 0 || [[_emailtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your email address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return FALSE;
        
    }
    else if (![predicate1 evaluateWithObject:_emailtxt.text] == YES)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your valid email address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return FALSE;
    }
    else
        if (_passwordtxt.text == nil || [_passwordtxt.text length] == 0 || [[_passwordtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    return TRUE;
}


@end
