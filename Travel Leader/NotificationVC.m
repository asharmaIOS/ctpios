//
//  NotificationVC.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 3/27/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "ValidationAndJsonVC.h"
#import "TravelConstantClass.h"
#import "HomeViewController.h"
#import "NotifDetailsVC.h"
#import "DemoMessagesViewController.h"

@interface NotificationVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *notificationtbl;
@property (weak, nonatomic) IBOutlet UIView *emptyview;


@property (strong, atomic) NSMutableArray *listArray;
@end

@implementation NotificationVC
@synthesize listArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden: NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    self.notificationtbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    [self setupLeftMenuButton];
    // Do any additional setup after loading the view.
    
    [SVProgressHUD showWithStatus:@"Processing..." maskType:SVProgressHUDMaskTypeBlack];

           [self performSelector:@selector(ListAlert) withObject:self afterDelay:1.0 ];
   // [self ListAlert];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.emptyview.hidden = YES;

    
    [super viewWillAppear:animated];
    
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    //    textButton.title = @"NOTIFICATION";
    //    // textButton.tintColor = [UIColor whiteColor];
    //    textButton.tintColor = [UIColor whiteColor];
    //    self.navigationItem.leftBarButtonItems = @[searchItem];
    //    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"NOTIFICATION"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backAction
{
    
    [self backButtonTap:self];
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)backButtonTap:(id)sender
{
    
    // [self dismissViewControllerAnimated:YES completion:nil] ;
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
            home = (HomeViewController*)vc;
        }
    }
    
    if (home == nil) {
        // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    
}
#pragma mark -  method for LISTAlert



- (void)ListAlert {
    
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        // [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];
       // self.emptyview.hidden = YES;
        [listArray removeAllObjects];
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        listArray = [userDef objectForKey:@"PkeyArray"];
        if( listArray.count ==0 ) {
            
           // self.emptyview.hidden = NO;
            
        }
      //  [self.notificationtbl reloadData];
        
    }
    
    
    else {
        
       // [listArray removeAllObjects];
        
    //    [_totalArrayPky removeAllObjects];
        
      //  [SVProgressHUD showWithStatus:@"Your Trips.." maskType:SVProgressHUDMaskTypeBlack];
       // self.emptyview.hidden = YES;
        
        NSDictionary *dictionary = @{@"travelerID":[TravelConstantClass singleton].travelerID};
        
        NSString *urlStr = @"ListAlerts";
        NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
        
        
        if  ( dataDic.allKeys == nil) {
            
          //  self.emptyview.hidden = NO;
          //  self.NoTriplbl.text = @"You Dont Have Any Upcoming Trip Yet";
            //  NSLog(@"pnr%@",listArray);
            
            self.emptyview.hidden = NO;

            [SVProgressHUD dismiss];
            
        }
        
        
        if (dataDic.allKeys != nil) {
            // [SVProgressHUD dismiss];
            
            listArray = [dataDic valueForKey :@"data"];
            
            self.emptyview.hidden = YES;

            if (listArray.count ==0) {
                
                self.emptyview.hidden = NO;
               // self.NoTriplbl.text = @"No notifications found!";
                [SVProgressHUD dismiss];
                
            }
            
            if (listArray.count >0) {
                
            
            for (int i = listArray.count-1; i >= 0; i--) {
                
                NSDictionary *data = [[listArray objectAtIndex:i]valueForKey:@"detail"];
                
                if ([[data valueForKey:@"type"] isEqualToString:@"Connection Information Gate Change"]) {
                    
                    NSDictionary *dic = [data valueForKey:@"flight"];
                    
                    NSString *flightnumber = [dic valueForKey:@"flightNumber"];
                    NSString * currentgateStr = [[data valueForKey:@"gate"] valueForKey:@"current"];
                    
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:flightnumber forKey: @"flightNo"];
                    [prefs setObject:currentgateStr forKey: @"currentGate"];
                    [prefs synchronize];
                    
                    NSLog(@"%@- -%@",flightnumber,currentgateStr);
                }
                
            }
            
                
            }
            
           
            
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^
            {
                
                [self.notificationtbl reloadData];
                
            });
            
            
        }
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listArray.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"NotificationCell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *pnrDict = listArray[indexPath.row];
    
    
   cell.titlelb.text = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"detail"]valueForKey:@"notificationTitle"]];
   cell.datalb.text = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"detail"]valueForKey:@"notificationText"]];
    
    return  cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotifDetailsVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"notif"];
  //  vc.alertId = [dataDict valueForKey:@"pnr"];
    
     NSDictionary *dataDict = listArray[indexPath.row];
    if ([[dataDict valueForKey:@"detail"] isKindOfClass:[NSDictionary class]]) {
        vc. detailDic = dataDict;
       [self.navigationController pushViewController:vc animated:YES];
    }
    
   
    
  
    
}
@end
