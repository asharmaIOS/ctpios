//
//  TripViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ViewController.h"

@interface TripViewController : ViewController
@property (strong, atomic) NSArray *listArray;
@property (strong, atomic) NSDictionary *dataDic;
@property (strong, atomic) NSString *destinationString;
@property (strong, atomic) NSMutableArray *NotificationArray;
@property (strong, atomic) NSMutableArray *segmentArrayOfButtons;
@property (strong, atomic) NSMutableArray *dataForReadmore;

@end

