//
//  CarViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "CarViewController.h"
#import "LimnoViewController.h"
#import "SWRevealViewController.h"
#import "DemoMessagesViewController.h"



#import "TravelConstantClass.h"
@interface CarViewController ()
{
    UIScrollView *bgScrollView;
    NSArray * detailarr;
    
}
@property (weak, nonatomic) IBOutlet UIView *passangerview;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;



@end

@implementation CarViewController
@synthesize pickctylb,pickdatelb,picktimelb,dropctylb,dropdatelb,droptimelb,rentbylb,cartypelb,confirmationlb;
- (void)viewDidLoad {
    [super viewDidLoad];
    detailarr = [[NSArray alloc]init];
    //   _imageview.image = _headerImage;
    
    
    [self loadMainImage:_headerImage];
    
    [self setupLeftMenuButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"CAR INFO."
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backAction
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)backButtonTapp:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadMainImage : (UIImage *)bannerImage {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    // main scrollview draw
    bgScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 44, screenWidth, screenHeight -44) ];
    bgScrollView.backgroundColor = [UIColor clearColor];
    
    // imageview draw & load image
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 220) ];
    imageview.backgroundColor = [UIColor clearColor];
    if (bannerImage) {
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = bannerImage;
    }
    
    bgScrollView.delegate =self;
    [ bgScrollView addSubview:imageview];
    
    UILabel *grayBgInformationlbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 220, screenWidth, 40)];
    grayBgInformationlbl.backgroundColor = [UIColor lightGrayColor];
    grayBgInformationlbl.textColor = [UIColor blackColor];
    grayBgInformationlbl.font = [UIFont fontWithName:@"Lato-Regular" size:16];
    grayBgInformationlbl.text = @" YOUR                 INFORMATION";
    [bgScrollView addSubview:grayBgInformationlbl ];
    
    
    UIImageView *typeOfInformation_imageview = [[UIImageView alloc]initWithFrame:CGRectMake(51, -10, 60, 60) ];
    typeOfInformation_imageview.backgroundColor = [UIColor clearColor];
    typeOfInformation_imageview.image = [UIImage imageNamed:@"transfer_big.png"];
    [grayBgInformationlbl addSubview:typeOfInformation_imageview];
    
    
    
    [self.view addSubview:bgScrollView];
    bgScrollView.showsVerticalScrollIndicator = NO;
    
    if( 667 ==  screenHeight ) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 100);
        
    }
    
    else if (736 ==  screenHeight) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight);
        
    }
    
    else if (812 ==  screenHeight) {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight -25);
        
        
    }
    
    else {
        
        bgScrollView.contentSize = CGSizeMake(screenWidth, screenHeight -40);
        
        
    }
    
    
    
    detailarr= [TravelConstantClass singleton].TripArray;
    
    
    if (detailarr) {
        
        [self drawViewForHotelSegment:detailarr];
        
        
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenMedianheight = screenRect.size.height/2;
    
    
    
    if (scrollView.contentOffset.y < 0 ) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    }
    
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        
        
    }
    
    
}

-(void)drawViewForHotelSegment :(NSArray *)dataArray{
    
    
    
    NSDictionary *dataDic ;
    
    for (NSDictionary *dictsub in [dataArray valueForKey:@"travelsegment"])
    {
        if ([dictsub valueForKey:@"carseg"]!= nil)
            
        {
            
            dataDic = [dictsub valueForKey:@"carseg"];
            
            
        }
    }
    
    
    
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGFloat screenMedianWidth = screenRect.size.width/2;
    
    // gray verticalLine draw
    
    UILabel *graylineVerticalLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth, 260, 1, 150)];
    graylineVerticalLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineVerticalLbl ];
    
    // iamge for arrow
    UIImageView *aarowImage = [[UIImageView alloc]initWithFrame:CGRectMake(-15, 60, 30, 30) ];
    aarowImage.backgroundColor = [UIColor clearColor];
    aarowImage.image = [UIImage imageNamed:@"black_arrow_right.png"];
    [graylineVerticalLbl addSubview:aarowImage];
    
    
    
    // grayhorizontal Line draw
    UILabel *graylineHorizontalLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 410, screenWidth, 1)];
    graylineHorizontalLbl.backgroundColor = [UIColor lightGrayColor];
    [bgScrollView addSubview:graylineHorizontalLbl ];
    
    
    // chechin label
    
    UILabel *checkInLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 270, 150, 30)];
    checkInLbl.textAlignment = NSTextAlignmentCenter;
    //grayBgInformationlbl.backgroundColor = [UIColor lightGrayColor];
    checkInLbl.textColor = [UIColor blackColor];
    checkInLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    checkInLbl.text = @"PICK UP";
    [bgScrollView addSubview:checkInLbl];
    
    // chechOUT label
    UILabel *checkOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 270, 150, 30)];
    checkOutLbl.textAlignment = NSTextAlignmentCenter;
    checkOutLbl.textColor = [UIColor blackColor];
    checkOutLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    checkOutLbl.text = @"DROP OFF";
    [bgScrollView addSubview:checkOutLbl];
    
    
    
    
    UILabel *checkInTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 , 310, 150, 30)];
    checkInTimeLbl.textAlignment = NSTextAlignmentCenter;
    checkInTimeLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    checkInTimeLbl.font = [UIFont fontWithName:@"Lato-Bold" size:22];
    checkInTimeLbl.text = [dataDic valueForKey:@"starttime"] ;
    [bgScrollView addSubview:checkInTimeLbl];
    
    UILabel *checkOutTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 310, 150, 30)];
    checkOutTimeLbl.textAlignment = NSTextAlignmentCenter;
    checkOutTimeLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    checkOutTimeLbl.font = [UIFont fontWithName:@"Lato-Bold" size:22];
    checkOutTimeLbl.text =  [dataDic valueForKey:@"endtime"] ;
    [bgScrollView addSubview:checkOutTimeLbl];
    
    
    
    UILabel *citycheckInLbl = [[UILabel alloc]initWithFrame:CGRectMake(10 , 335, 170, 30)];
    citycheckInLbl.textAlignment = NSTextAlignmentCenter;
    citycheckInLbl.textColor = [UIColor blackColor];
    citycheckInLbl.font = [UIFont fontWithName:@"Lato-Bold" size:17];
    
    if ([[dataDic valueForKey:@"city"] isKindOfClass:[NSArray class]])
    {
        NSArray *array = [dataDic valueForKey:@"city"];
        citycheckInLbl.text = [array lastObject];
    }
    
    if ([[dataDic valueForKey:@"city"] isKindOfClass:[NSString class]]) {
        
        NSString *cityStr = [dataDic valueForKey:@"city"];
        citycheckInLbl.text = cityStr;
    }
    
    [bgScrollView addSubview:citycheckInLbl];
    
    
    UILabel *cityCheckOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(10 + screenMedianWidth, 335, 170, 30)];
    cityCheckOutLbl.textAlignment = NSTextAlignmentCenter;
    cityCheckOutLbl.textColor = [UIColor blackColor];
    cityCheckOutLbl.font = [UIFont fontWithName:@"Lato-Bold" size:17];
    
    if ([[dataDic valueForKey:@"dropoff"] isKindOfClass:[NSArray class]])
    {
        NSArray *array = [dataDic valueForKey:@"dropoff"];
        cityCheckOutLbl.text = [array lastObject];
    }
    
    if ([[dataDic valueForKey:@"dropoff"] isKindOfClass:[NSString class]]) {
        
        NSString *cityStr = [dataDic valueForKey:@"dropoff"];
        cityCheckOutLbl.text = cityStr;
    }
    
    [bgScrollView addSubview:cityCheckOutLbl];
    
    
    UILabel *datecheckInLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 , 360, 150, 30)];
    datecheckInLbl.textAlignment = NSTextAlignmentCenter;
    datecheckInLbl.textColor = [UIColor blackColor];
    datecheckInLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    
    NSDateFormatter *dateForm = [[NSDateFormatter alloc] init];
    [dateForm setDateFormat: @"MM/dd/yyyy"];
    NSDate *datecheck = [dateForm dateFromString: [dataDic valueForKey:@"stdt"]];
    //Second Conversion
    [dateForm setDateFormat: @"MMMM dd, yyyy"];
    NSString *finalDate = [dateForm stringFromDate:datecheck];
    
    datecheckInLbl.text = finalDate;
    
    
    //datecheckInLbl.text = [dataDic valueForKey:@"stdt"];
    
    [bgScrollView addSubview:datecheckInLbl];
    
    UILabel *dateCheckOutLbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + screenMedianWidth, 360, 150, 30)];
    dateCheckOutLbl.textAlignment = NSTextAlignmentCenter;
    dateCheckOutLbl.textColor = [UIColor blackColor];
    dateCheckOutLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    
    NSDateFormatter *dateForm1 = [[NSDateFormatter alloc] init];
    [dateForm1 setDateFormat: @"MM/dd/yyyy"];
    NSDate *datecheck1 = [dateForm1 dateFromString: [dataDic valueForKey:@"eddt"]];
    //Second Conversion
    [dateForm1 setDateFormat: @"MMMM dd, yyyy"];
    NSString *finalDateForcheckout = [dateForm1 stringFromDate:datecheck1];
    
    
    dateCheckOutLbl.text = finalDateForcheckout ;
    
    [bgScrollView addSubview:dateCheckOutLbl];
    
    
    UILabel *hotelLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth - 50 , 450, 100, 30)];
    hotelLbl.textAlignment = NSTextAlignmentCenter;
    hotelLbl.textColor = [UIColor blackColor];
    hotelLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    hotelLbl.text = @"Rented by";
    [bgScrollView addSubview:hotelLbl];
    
    UILabel *hotelNameLbl = [[UILabel alloc]initWithFrame:CGRectMake( screenMedianWidth - 120, 480, 240, 30)];
    hotelNameLbl.textAlignment = NSTextAlignmentCenter;
    hotelNameLbl.lineBreakMode = NSLineBreakByWordWrapping;
    hotelNameLbl.numberOfLines = 6;
    hotelNameLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    hotelNameLbl.font = [UIFont fontWithName:@"Lato-Bold" size:20];
    hotelNameLbl.text = [dataDic valueForKey:@"carvendor"];
    [bgScrollView addSubview:hotelNameLbl];
    
    
    
    UILabel *roomTypeLbl = [[UILabel alloc]initWithFrame:CGRectMake(screenMedianWidth - 50 , 550, 100, 30)];
    roomTypeLbl.textAlignment = NSTextAlignmentCenter;
    roomTypeLbl.textColor = [UIColor blackColor];
    roomTypeLbl.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    roomTypeLbl.text = @"Car Type";
    [bgScrollView addSubview:roomTypeLbl];
    
    
    UILabel *roomType = [[UILabel alloc]initWithFrame:CGRectMake(50 , 585, 300, 30)];
    roomType.textAlignment = NSTextAlignmentCenter;
    roomType.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    roomType.font = [UIFont fontWithName:@"Lato-Bold" size:20];
    roomType.text = [dataDic valueForKey:@"cartype"];
    [bgScrollView addSubview:roomType];
    
    
    UILabel *orangeBglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 700, screenWidth, 70)];
    orangeBglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    [bgScrollView addSubview:orangeBglbl ];
    
    UIImageView *checkImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 30, 30) ];
    checkImage.backgroundColor = [UIColor clearColor];
    checkImage.image = [UIImage imageNamed:@"check.png"];
    [orangeBglbl addSubview:checkImage];
    
    
    UILabel *confirmlbl = [[UILabel alloc]initWithFrame:CGRectMake(55 , 15, 120, 30)];
    confirmlbl.textAlignment = NSTextAlignmentLeft;
    confirmlbl.textColor = [UIColor whiteColor];
    confirmlbl.font = [UIFont fontWithName:@"Lato-Regular" size:20];
    confirmlbl.text = @"Confirmation";
    [orangeBglbl addSubview:confirmlbl];
    
    
    UILabel *confirmNo = [[UILabel alloc]initWithFrame:CGRectMake(175 , 15, 200, 30)];
    confirmNo.textAlignment = NSTextAlignmentLeft;
    confirmNo.textColor = [UIColor whiteColor];
    confirmNo.font = [UIFont fontWithName:@"Lato-Regular" size:20];
    confirmNo.text = [NSString stringWithFormat:@"#%@",[dataDic valueForKey:@"confirmation"]];
    
    [orangeBglbl addSubview:confirmNo];
    
    
}



- (IBAction)uberBtnClick: (id)sender
{
    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"uber://?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL"] ])
    {
        NSLog(@"uber");
    }
    else
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://m.uber.com/ul/?client_id=dmzqXHAtWfXKrkqPyEJ84xr1sbbSeiFL"] options:@{} completionHandler:nil];
    }
    
}

- (IBAction)lyftBtnClick: (id)sender
{
    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"lyft://?partner=14Tc17vjnLwW"] ])
    {
        NSLog(@"uber");
    }
    else
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.lyft.com/signup/SDKSIGNUP?clientId=14Tc17vjnLwW"] options:@{} completionHandler:nil];
    }
    
}














@end
