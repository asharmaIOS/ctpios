
#import "DemoMessagesViewController.h"
//#import "JSQMessagesViewAccessoryButtonDelegate.h"
#import "TravelConstantClass.h"
#import "Constant.h"
#import "JSQMessage.h"
#import <AudioToolbox/AudioServices.h>
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "ValidationAndJsonVC.h"
#import "Reachability.h"





@interface DemoMessagesViewController ()// <JSQMessagesViewAccessoryButtonDelegate>

{
    
    __block UIBackgroundTaskIdentifier bgTask;
    // UIBackgroundTaskIdentifier bgTask;
    NSMutableArray * typearr;
    NSString * author ;
    
    
}
@property (strong, atomic) NSMutableArray *jsonarr;
@property (nonatomic,assign) NSInteger  messageindex;

@property (nonatomic,assign) NSInteger  tagForApiCallreceive;

@property (strong) NSTimer * messageTimer;
@property (nonatomic, strong) NSMutableData *responseData;
@end
@implementation DemoMessagesViewController
@synthesize jsonarr ;
#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    jsonarr= [[NSMutableArray alloc]init];
    _tagForApiCallreceive = 0 ;
    [TravelConstantClass singleton].messagecount = 0;
    
    typearr = [[NSMutableArray alloc] init];
    [self.navigationController.navigationBar setHidden: NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    [self setupLeftMenuButton];
    
    
    
    // self.title = @"Travel Agent";
    // self.title = [UIColor whiteColor];
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    textButton.title = @"TRAVEL AGENT";
    // textButton.tintColor = [UIColor whiteColor];
    textButton.tintColor = [UIColor whiteColor];
    // self.navigationItem.leftBarButtonItems = @[searchItem];
    self.navigationItem.rightBarButtonItems = @[textButton];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            // [ValidationAndJsonVC displayAlert:@"Please check your internet" HeaderMsg:@"Oops"];
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops"
                                                                          message:@"Please check your internet"
                                                                   preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        
                                        {
                                            
                                            
                                            HomeViewController* home = nil;
                                            for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
                                                NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
                                                if([vc isKindOfClass:[HomeViewController class]]) {
                                                    home = (HomeViewController*)vc;
                                                }
                                            }
                                            
                                            if (home == nil) {
                                                
                                                home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
                                                
                                                [self.navigationController pushViewController:home animated:NO];
                                                // Do we need to push it into navigation controller?
                                            }
                                            
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
    }
    
    else {
        
        [self CallServiceForcaseID];
        
    }
    self.demoData = [[DemoModelData alloc] init];
    
    
    if (![NSUserDefaults incomingAvatarSetting]) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
    if (![NSUserDefaults outgoingAvatarSetting]) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    
}


/// for chat local notifications

- (void)scheduleLocalNotifications:(NSString *)msgString {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    //    UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"Snooze"
    //                                                                              title:@"Snooze" options:UNNotificationActionOptionNone];
    //    UNNotificationAction *deleteAction = [UNNotificationAction actionWithIdentifier:@"Delete"
    //                                                                              title:@"Delete" options:UNNotificationActionOptionDestructive];
    //
    //    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"UYLReminderCategory"
    //                                                                              actions:@[snoozeAction,deleteAction] intentIdentifiers:@[]
    //                                                                              options:UNNotificationCategoryOptionNone];
    //    NSSet *categories = [NSSet setWithObject:category];
    //
    //    [center setNotificationCategories:categories];
    
    
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    // content.title = @"Don't forget";
    
    if (msgString.length ==0 )
    {
        content.body = @"You have new message";
        
    }
    else {
        content.body = msgString ;
    }
    content.categoryIdentifier = @"UYLReminderCategory";
    content.sound = [UNNotificationSound defaultSound];
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:5 repeats:NO];
    
    NSString *identifier = @"UYLLocalNotification";
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Something went wrong: %@",error);
        }
    }];
}






#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"close-icon-black.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 22)];
    [backButton addTarget:self action:@selector(backButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    textButton.title = @"";
    // textButton.tintColor = [UIColor whiteColor];
    textButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
    
    //  self.navigationItem.leftBarButtonItem = @[searchItem];
}

#pragma mark - Back Button Tap



-(void )backBtnCall {
    
    
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
           // home = (HomeViewController*)vc;
        }
    }
    
    if (home == nil) {
        // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
       // [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    
    [self.navigationController pushViewController:home animated:NO];
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        [_messageTimer invalidate];
        
        [ValidationAndJsonVC displayAlert:@"Please check your internet" HeaderMsg:@"Oops"];
        
    }
    
    else {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@ChatSendMessage",[AppDelegate sharedAppDelegate].BASEAPI];
        
        //*****************************LIVE ********************************
        // NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatPollForNewMessage"];
        NSURL *url = [NSURL URLWithString:urlStr];
        //  NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatSendMessage"];
        //************TEST ***********************
        // NSURL *url = [NSURL URLWithString:@"http://107.1.185.201/api/v1/chat/ChatSendMessage"];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.timeoutIntervalForRequest = 30.0;
        config.timeoutIntervalForResource = 30.0;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSDictionary *dictionary = @{@"chatID": [TravelConstantClass singleton].caseID,@"messageType": @"2",@"messageBody": @""};
        NSLog(@"%@--------",dictionary);
        
        // 2
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        request.HTTPMethod = @"POST";
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:kNilOptions error:nil];
        request.HTTPBody = data;
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (response != nil) {
                NSArray *jsonarr = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"JSon dict === %@",jsonarr);
                
                
                if([[NSString stringWithFormat:@"%@",[jsonarr valueForKey:@"messageSent"]]  isEqualToString:@"0"]) {
                    
                    //                                                UIAlertView *toast = [[UIAlertView alloc] initWithTitle:@"OOPS" message:@"Massage Not Sent " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //
                    //                                                [toast show];
                    
                }
                else
                {
                    //[self finishSendingMessageAnimated:YES];
                }
            }
            
            
        }];
        
        [postDataTask resume];
        
        
        
        [TravelConstantClass singleton].messagecount = 0;
        
        [_messageTimer invalidate];
        
        
        
    }
    
    
}
-(void)backButtonTap:(id)sender
{
    
    
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Chat Close "
                                                                  message:@"Are you sure you want to end this chat?"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                
                                {
                                    //******************Live **********************
                                    
                                    NSString *urlStr = [NSString stringWithFormat:@"%@ChatSendMessage",[AppDelegate sharedAppDelegate].BASEAPI];
                                    
                                    //*****************************LIVE ********************************
                                    // NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatPollForNewMessage"];
                                    NSURL *url = [NSURL URLWithString:urlStr];
                                    //  NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatSendMessage"];
                                    //************TEST ***********************
                                    // NSURL *url = [NSURL URLWithString:@"http://107.1.185.201/api/v1/chat/ChatSendMessage"];
                                    
                                    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                                    config.timeoutIntervalForRequest = 30.0;
                                    config.timeoutIntervalForResource = 30.0;
                                    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
                                    
                                    NSDictionary *dictionary = @{@"chatID": [TravelConstantClass singleton].caseID,@"messageType": @"2",@"messageBody": @""};
                                    NSLog(@"%@--------",dictionary);
                                    
                                    // 2
                                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
                                    request.HTTPMethod = @"POST";
                                    
                                    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                                                   options:kNilOptions error:nil];
                                    request.HTTPBody = data;
                                    
                                    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                        
                                        if (response != nil) {
                                            NSArray *jsonarr = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                            
                                            NSLog(@"JSon dict === %@",jsonarr);
                                            
                                            
                                            if([[NSString stringWithFormat:@"%@",[jsonarr valueForKey:@"messageSent"]]  isEqualToString:@"0"]) {
                                                
                                                //                                                UIAlertView *toast = [[UIAlertView alloc] initWithTitle:@"OOPS" message:@"Massage Not Sent " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                //
                                                //                                                [toast show];
                                                
                                            }
                                            else
                                            {
                                                //[self finishSendingMessageAnimated:YES];
                                            }
                                        }
                                        
                                        
                                    }];
                                    
                                    [postDataTask resume];
                                    
                                    
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                    [self backBtnCall];

                                    
                                                });
                                   
                                    
                                    [TravelConstantClass singleton].messagecount = 0;
                                    
                                    [_messageTimer invalidate];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed No, thanks button");
                                   // call method whatever u need
                               }];
    
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    _tagForApiCallreceive = 0;
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    //  [self CallServiceForcaseID];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.collectionView.collectionViewLayout.springinessEnabled = [NSUserDefaults springinessSetting];
    
}

#pragma mark - Custom menu actions for cells

- (void)didReceiveMenuWillShowNotification:(NSNotification *)notification
{
    
    UIMenuController *menu = [notification object];
    menu.menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action" action:@selector(customAction:)] ];
    
    [super didReceiveMenuWillShowNotification:notification];
}



#pragma mark - Testing

- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}

#pragma mark - Actions ReceiveMessage

- (void)closePressed:(UIBarButtonItem *)sender
{
    [self.delegateModal didDismissJSQDemoViewController:self];
}


# pragma mark - Get ChatId method

-(void)CallServiceForcaseID
{
    //****************LIVE ********************
    
    //  NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatStartNew"];
    NSString *urlStr = [NSString stringWithFormat:@"%@ChatStartNew",[AppDelegate sharedAppDelegate].BASEAPI];
    
    //*****************************LIVE ********************************
    // NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatPollForNewMessage"];
    NSURL *url = [NSURL URLWithString:urlStr];
    //*****************TEST**************************
    //NSURL *url = [NSURL URLWithString:@"http://107.1.185.201/api/v1/chat/ChatStartNew"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30.0;
    config.timeoutIntervalForResource = 30.0;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSDictionary *dictionary = @{@"travelerID": [TravelConstantClass singleton].travelerID};
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:kNilOptions error:nil];
    request.HTTPBody = data;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        if (response != nil) {
            NSArray *jsonarr = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSLog(@"JSon dict === %@",jsonarr);
            
            [TravelConstantClass singleton].caseID = [jsonarr valueForKey:@"chatID"];
            
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                _messageTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
            //
            //
            //            });
            
            
            
            //            UIApplication *app = [UIApplication sharedApplication];
            //
            //            __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            //               // [app endBackgroundTask:bgTask];
            //                bgTask = UIBackgroundTaskInvalid;
            //            }];
            //
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            //                _messageTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
            //
            //                [[NSRunLoop currentRunLoop] addTimer:_messageTimer forMode:UITrackingRunLoopMode];
            //                [[NSRunLoop currentRunLoop] run];
            //            });
            //
            //
            
            
            
            bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            }];
            dispatch_queue_t queue = dispatch_queue_create("com.test.test1234", DISPATCH_QUEUE_SERIAL);
            dispatch_async(queue, ^{
                // My Task goes here
                
                
                _messageTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
                
                [[NSRunLoop currentRunLoop] addTimer:_messageTimer forMode:UITrackingRunLoopMode];
                [[NSRunLoop currentRunLoop] run];
            });
            
            
            
            
            
            
            
            
            
            if ([[jsonarr valueForKey:@"Success"] isEqualToString:@"false"]||[[jsonarr valueForKey:@"Success"] isEqualToString:@"0"]) {
                
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet " HeaderMsg:@"Oops"];
                
                
            }
            else
            {
                //  [self finishSendingMessageAnimated:YES];
            }
            
            
        }
        
    }];
    
    [postDataTask resume];
    
}



#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    
    if ([TravelConstantClass singleton].caseID ==  nil) {
        
        [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet " HeaderMsg:@"Oops"];
        
        
    }
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    [TravelConstantClass singleton].shareDocText = text;
    
    [self.demoData.messages addObject:message];
    //************************LIVE ********************************************
    
    NSString *urlStr = [NSString stringWithFormat:@"%@ChatSendMessage",[AppDelegate sharedAppDelegate].BASEAPI];
    
    // NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatSendMessage"];
    //*************************TEST *****************************************
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30.0;
    config.timeoutIntervalForResource = 30.0;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSDictionary *dictionary = @{@"chatID": [TravelConstantClass singleton].caseID,@"messageType": @"1",@"messageBody":  [TravelConstantClass singleton].shareDocText};
    NSLog(@"%@--------",dictionary);
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:kNilOptions error:nil];
    request.HTTPBody = data;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (response != nil) {
            NSArray *jsonarr = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSLog(@"JSon dict === %@",jsonarr);
            
            if (response != nil)
            {
                
            }
            
            
            if([[NSString stringWithFormat:@"%@",[jsonarr valueForKey:@"messageSent"]]  isEqualToString:@"0"]) {
                
                //                UIAlertView *toast = [[UIAlertView alloc] initWithTitle:@"OOPS" message:@"Massage Not Sent " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //
                //                [toast show];
                
            }
            else
            {
                //[self finishSendingMessageAnimated:YES];
            }
        }
        
        
    }];
    
    [postDataTask resume];
    
    
    
    [self finishSendingMessageAnimated:YES];
}


#pragma mark - Chat Massagepoll method

-(void) callAfterSixtySecond:(NSTimer*) t
{
    
    if (_tagForApiCallreceive == 0) {
        
        _tagForApiCallreceive = 1;
        
        NSString *urlStr = [NSString stringWithFormat:@"%@ChatPollForNewMessage",[AppDelegate sharedAppDelegate].BASEAPI];
        
        //*****************************LIVE ********************************
        // NSURL *url = [NSURL URLWithString:@"http://kryptos.greaves.biz:80/api/v1/chat/ChatPollForNewMessage"];
        NSURL *url = [NSURL URLWithString:urlStr];
        //*******************************TEST*************************************
        // NSURL *url = [NSURL URLWithString:@"http://107.1.185.201/api/v1/chat/ChatPollForNewMessage"];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.timeoutIntervalForRequest = 30.0;
        config.timeoutIntervalForResource = 30.0;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        if ([TravelConstantClass singleton].caseID ==  nil)
        {
            
            [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet " HeaderMsg:@"Oops"];
            
            
        }
        // start if
        //    NSDictionary *dictionary = @{@"chatID": [TravelConstantClass singleton].caseID,@"index":[NSString stringWithFormat:@"%ld",[TravelConstantClass singleton].messagecount],@"isTyping": @"false"};
        
        
        int indexChat = [TravelConstantClass singleton].messagecount;
        
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[TravelConstantClass singleton].caseID,@"chatID",[NSNumber numberWithInt:indexChat],@"index",
                                    @"false",@"isTyping",nil];
        
        NSLog(@"Dictionary of send msg %@",dictionary);
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        request.HTTPMethod = @"POST";
        
        NSData *data1 = [NSJSONSerialization dataWithJSONObject:dictionary
                                                        options:kNilOptions error:nil];
        request.HTTPBody = data1;
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            _responseData = [NSMutableData dataWithData:data];
            
            
            _tagForApiCallreceive = 0;
            
            
            if (response != nil) {
                
                NSMutableArray* result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"Result1---------%@",result);
                
                if ([result valueForKey:@"fault"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self backBtnCall];
                        
                        [ValidationAndJsonVC displayAlert:@"Sorry, Unfortunately Chat is Not Currently Available" HeaderMsg:@"Oops"];
                        
                        [_messageTimer invalidate];
                        
                        
                    });
                    
                    
                    
                }
                
                
                
                
                NSArray *dataArray = [result valueForKey:@"messageList"];
                
                //  NSLog(@"txtarr===%@",[TravelConstantClass singleton].txtArr);
                
                if (dataArray.count == 0)
                {
                    NSLog(@"messageindex");
                    
                }
                else
                {
                    
                    [TravelConstantClass singleton].messagecount = [TravelConstantClass singleton].messagecount + dataArray.count;
                    
                    
                    for (int i = 0 ; i <dataArray.count ; i++)
                    {
                        
                        if ([[[[result valueForKey:@"messageList"]objectAtIndex:i]valueForKey:@"type"]isEqualToString:@"staff"])
                        {
                            
                            
                            NSDictionary *messagedict = [dataArray objectAtIndex:i];
                            
                            JSQMessage *messagereciever = [[JSQMessage alloc] initWithSenderId:[NSString stringWithFormat:@"%@",[messagedict valueForKey:@"index"]]
                                                           
                                                                             senderDisplayName:[messagedict valueForKey:@"author"]
                                                           
                                                                                          date:[NSDate date]
                                                           
                                                                                          text:[messagedict valueForKey:@"text"]];
                            
                            
                            [self.demoData.messages addObject:messagereciever];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                [self.collectionView reloadData];
                                [self scrollToBottomAnimated:YES];
                                
                                
                                
                                // code for local notifications
                                
                                UIApplicationState state = [[UIApplication sharedApplication] applicationState];
                                if (state == UIApplicationStateBackground )
                                {
                                    
                                    
                                    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                                    
                                    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
                                        if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
                                            // Notifications allowed
                                            [self scheduleLocalNotifications:[messagedict valueForKey:@"text"]];
                                        } else {
                                            // Notifications not allowed
                                        }
                                    }];
                                }
                                //[self finishSendingMessageAnimated:YES];
                                
                            });
                            
                        }
                        
                        
                    }
                    
                }
            }
            
        }];
        
        [postDataTask resume];
        
        
    }
    
}
#pragma mark - Actions

- (void)receiveMessagePressed
{
    
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    [self.collectionView reloadData];
    [self finishReceivingMessageAnimated:YES];
    
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Media messages", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send photo", nil), NSLocalizedString(@"Send location", nil), NSLocalizedString(@"Send video", nil), NSLocalizedString(@"Send video thumbnail", nil), NSLocalizedString(@"Send audio", nil), nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self.demoData addPhotoMediaMessage];
            break;
            
        case 1:
        {
            __weak UICollectionView *weakView = self.collectionView;
            
            [self.demoData addLocationMediaMessageCompletion:^{
                [weakView reloadData];
            }];
        }
            break;
            
        case 2:
            [self.demoData addVideoMediaMessage];
            break;
            
        case 3:
            [self.demoData addVideoMediaMessageWithThumbnail];
            break;
            
        case 4:
            [self.demoData addAudioMediaMessage];
            break;
    }
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    //[self finishSendingMessageAnimated:YES];
}



#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return kJSQDemoAvatarIdSquires;
}

- (NSString *)senderDisplayName {
    return kJSQDemoAvatarDisplayNameSquires;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.demoData.outgoingBubbleImageData;
    }
    
    return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        if (![NSUserDefaults outgoingAvatarSetting]) {
            return nil;
        }
    }
    else {
        if (![NSUserDefaults incomingAvatarSetting]) {
            return nil;
        }
    }
    
    
    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    // cell.accessoryButton.hidden = ![self shouldShowAccessoryButtonForMessage:msg];
    
    return cell;
}

- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message
{
    return ([message isMediaMessage] && [NSUserDefaults accessoryButtonForMediaMessages]);
}


#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    
    
    
    // [self dismissViewControllerAnimated:NO completion:nil];
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        
        
        [self.demoData.messages addObject:message];
        // [self webService];
        
        
        
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}


#pragma mark - JSQMessagesViewAccessoryDelegate methods

//- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
//{
//    NSLog(@"Tapped accessory button!");
//}

@end
