//
//  RestrictionsandStatsVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 28/08/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "RestrictionsandStatsVC.h"
#import "SVProgressHUD.h"
#import "WebCheckInVC.h"

#import "Reachability.h"
#import "ValidationAndJsonVC.h"
#import <QuartzCore/QuartzCore.h>

@interface RestrictionsandStatsVC ()

@end

@implementation RestrictionsandStatsVC

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    [self setupLeftMenuButton];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
                
    if (networkStatus == NotReachable) {
                    [ValidationAndJsonVC displayAlert:nil HeaderMsg:@"No internet connection found!"];
}
    else {
        
        [self drawForNotesAndRestrictionContainerView:_dataDic];
                    //[self apiCallingFunction];
                }
    [super viewDidLoad];
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"TRAVEL RESTRICTIONS"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}

-(void)backAction{
    
  [self.navigationController popViewControllerAnimated:NO];
}



- (CGFloat)getTextHeightByWidth:(NSString*)text textFont:(UIFont*)textFont textWidth:(float)textWidth {
    
    if (!text) {
        return 0;
    }
    CGSize boundingSize = CGSizeMake(textWidth, CGFLOAT_MAX);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: textFont }];
    
    CGRect rect = [attributedText boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize requiredSize = rect.size;
    return requiredSize.height;
}

-(void)drawForNotesAndRestrictionContainerView :(NSDictionary *)dataDic {
    
    for(UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
    
             CGRect screenRect = [[UIScreen mainScreen] bounds];
             CGFloat screenWidth = screenRect.size.width;
             CGFloat screenHeight = screenRect.size.height;
    
             UIScrollView   * scrollView = [[UIScrollView alloc] init];
                scrollView.frame = CGRectMake(10, 89, screenWidth-20 , screenHeight-89);
                CGFloat finalheight = 0;
             
    
    UILabel * reponseLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, finalheight+10, scrollView.frame.size.width , 25)];
    reponseLbl.text = [NSString stringWithFormat:@"Government response index: %@",[[dataDic valueForKey:@"advice"]valueForKey:@"government_response_index"]];
    reponseLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
    reponseLbl.adjustsFontSizeToFitWidth = YES;
    //reponseLbl.textAlignment = NSTextAlignmentCenter;
    reponseLbl.textColor = [UIColor blackColor];
    [scrollView addSubview:reponseLbl];
    finalheight = finalheight+45;
    
    UILabel * healthLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, finalheight, scrollView.frame.size.width , 25)];
       healthLbl.text = [NSString stringWithFormat:@"Containment and health index: %@",[[dataDic valueForKey:@"advice"]valueForKey:@"containment_and_health_index"]];
       healthLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
       healthLbl.adjustsFontSizeToFitWidth = YES;
      // healthLbl.textAlignment = NSTextAlignmentCenter;
       healthLbl.textColor = [UIColor blackColor];
       [scrollView addSubview:healthLbl];
       finalheight = finalheight+40;

    
    
    
           
      
      
    UILabel * vendorHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight, scrollView.frame.size.width, 35)];
     vendorHeadinglbl.text = @"  Restrictions";
    // usaHeadinglbl.text =[countryArray objectAtIndex:i];
     vendorHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
     vendorHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
     vendorHeadinglbl.textColor = [UIColor whiteColor];
     vendorHeadinglbl.layer.cornerRadius = 4;
     vendorHeadinglbl.userInteractionEnabled= YES;
      vendorHeadinglbl.layer.masksToBounds = true;
     [scrollView addSubview:vendorHeadinglbl];
    

    
    finalheight = finalheight +35;
    NSDictionary *restrctDic = [[dataDic valueForKey:@"advice"]valueForKey:@"restrictions"];
    
 // Code for Different type of restrictions ******************************************************
    if([[dataDic valueForKey:@"advice"]valueForKey:@"restrictions"]) {
        
       UILabel *grayBG = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight, scrollView.frame.size.width, 165)];
       grayBG.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
       [scrollView addSubview:grayBG];
        
        CGFloat heightForGaryBG = 0;
       
        if ([restrctDic valueForKey:@"international_travel_controls"]) {
        
            UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-10, 20)];
            headingLbl.text = @"International travel controls";
            headingLbl.adjustsFontSizeToFitWidth = YES;
            headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
            headingLbl.textColor = [UIColor blackColor];
            [grayBG addSubview:headingLbl];
     
    NSString *strForTextHeight = [[restrctDic valueForKey:@"international_travel_controls"]valueForKey:@"level_desc"];
    CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-20];
            
        UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 35, scrollView.frame.size.width-20, heightOfText)];
            IntTravelCtrlbl.numberOfLines = 12;
        IntTravelCtrlbl.text = [[restrctDic valueForKey:@"international_travel_controls"]valueForKey:@"level_desc"];
        IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
        IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
        IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
        [grayBG addSubview:IntTravelCtrlbl];
    
            heightForGaryBG = heightForGaryBG +35+heightOfText+15;

        }
        
        
        if ([restrctDic valueForKey:@"restrictions_on_internal_movement"]) {
                   
            
                  UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                   headingLbl.text = @"Restrictions on internal movement";
                   headingLbl.adjustsFontSizeToFitWidth = YES;
                   headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                   headingLbl.textColor = [UIColor blackColor];
                   [grayBG addSubview:headingLbl];
            
NSString *strForTextHeight = [[restrctDic valueForKey:@"restrictions_on_internal_movement"]valueForKey:@"level_desc"];
CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                   
               UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
               IntTravelCtrlbl.text = [[restrctDic valueForKey:@"restrictions_on_internal_movement"]valueForKey:@"level_desc"];
               IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
              IntTravelCtrlbl.numberOfLines = 12;

               IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
               IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
               [grayBG addSubview:IntTravelCtrlbl];
           
            heightForGaryBG = heightForGaryBG +35+heightOfText+10;

               }
        
        if ([restrctDic valueForKey:@"close_public_transport"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"Close public transport";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"close_public_transport"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                       IntTravelCtrlbl.numberOfLines = 12;

                       IntTravelCtrlbl.text = [[restrctDic valueForKey:@"close_public_transport"]valueForKey:@"level_desc"];
                       IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
                if ([restrctDic valueForKey:@"stay_at_home_requirements"]) {
                                   
                            
                                  UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                                   headingLbl.text = @"Stay at home requirements";
                                   headingLbl.adjustsFontSizeToFitWidth = YES;
                                   headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                                   headingLbl.textColor = [UIColor blackColor];
                                   [grayBG addSubview:headingLbl];
                            
                NSString *strForTextHeight = [[restrctDic valueForKey:@"stay_at_home_requirements"]valueForKey:@"level_desc"];
                CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                                   
                               UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                               IntTravelCtrlbl.numberOfLines = 12;

                               IntTravelCtrlbl.text = [[restrctDic valueForKey:@"stay_at_home_requirements"]valueForKey:@"level_desc"];
                               IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                               IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                               IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                               [grayBG addSubview:IntTravelCtrlbl];
                           
                            heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                               }
        
        if ([restrctDic valueForKey:@"cancel_public_events"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"Cancel public events";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"cancel_public_events"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                        IntTravelCtrlbl.numberOfLines = 12;

                       IntTravelCtrlbl.text = [[restrctDic valueForKey:@"cancel_public_events"]valueForKey:@"level_desc"];
                       IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
        
        if ([restrctDic valueForKey:@"restrictions_on_gatherings"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"Restrictions on gatherings";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"restrictions_on_gatherings"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                        IntTravelCtrlbl.lineBreakMode = NSLineBreakByWordWrapping;
                        IntTravelCtrlbl.numberOfLines = 12;



                       IntTravelCtrlbl.text = [[restrctDic valueForKey:@"restrictions_on_gatherings"]valueForKey:@"level_desc"];
                       IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
        
        if ([restrctDic valueForKey:@"workplace_closing"]) {
                                  
                           
                                 UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                                  headingLbl.text = @"Workplace closing";
                                  headingLbl.adjustsFontSizeToFitWidth = YES;
                                  headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                                  headingLbl.textColor = [UIColor blackColor];
                                  [grayBG addSubview:headingLbl];
                           
               NSString *strForTextHeight = [[restrctDic valueForKey:@"workplace_closing"]valueForKey:@"level_desc"];
               CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                                  
                              UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                               IntTravelCtrlbl.numberOfLines = 12;

                              IntTravelCtrlbl.text = [[restrctDic valueForKey:@"workplace_closing"]valueForKey:@"level_desc"];
                             // IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                              IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                              IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                              [grayBG addSubview:IntTravelCtrlbl];
                          
                           heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                              }
         
        
        if ([restrctDic valueForKey:@"school_closing"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"School closing";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"school_closing"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                        IntTravelCtrlbl.numberOfLines = 12;

                       IntTravelCtrlbl.text = [[restrctDic valueForKey:@"school_closing"]valueForKey:@"level_desc"];
                      // IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
        
        if ([restrctDic valueForKey:@"testing_policy"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"Testing policy";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"testing_policy"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                        IntTravelCtrlbl.lineBreakMode = NSLineBreakByWordWrapping;
                        IntTravelCtrlbl.numberOfLines = 12;
                       IntTravelCtrlbl.text = [[restrctDic valueForKey:@"testing_policy"]valueForKey:@"level_desc"];
                       //IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
        
        if ([restrctDic valueForKey:@"contact_tracing"]) {
                           
                    
                          UILabel * headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG, scrollView.frame.size.width-10, 20)];
                           headingLbl.text = @"Contact tracing";
                           headingLbl.adjustsFontSizeToFitWidth = YES;
                           headingLbl.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
                           headingLbl.textColor = [UIColor blackColor];
                           [grayBG addSubview:headingLbl];
                    
        NSString *strForTextHeight = [[restrctDic valueForKey:@"contact_tracing"]valueForKey:@"level_desc"];
        CGFloat heightOfText = [self getTextHeightByWidth:strForTextHeight textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width-20];
                           
                       UILabel * IntTravelCtrlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, heightForGaryBG+25, scrollView.frame.size.width-20, heightOfText)];
                        IntTravelCtrlbl.lineBreakMode = NSLineBreakByWordWrapping;
                        IntTravelCtrlbl.numberOfLines = 12;
                        IntTravelCtrlbl.text = [[restrctDic valueForKey:@"contact_tracing"]valueForKey:@"level_desc"];
                      // IntTravelCtrlbl.adjustsFontSizeToFitWidth = YES;
                       IntTravelCtrlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       IntTravelCtrlbl.textColor = [UIColor darkGrayColor];
                       [grayBG addSubview:IntTravelCtrlbl];
                   
                    heightForGaryBG = heightForGaryBG +35+heightOfText+10;

                       }
        
        
        
        
       
        grayBG.frame =  CGRectMake(0,finalheight, scrollView.frame.size.width, heightForGaryBG);
        finalheight = finalheight +heightForGaryBG+10;
    }
    
 // End of Restrictions sections *************************************************
    
 //   finalheight = finalheight +165;

//    for (int i=0; i<[notesArr count]; i++) {
//
//        NSString *noteStr = [NSString stringWithFormat:@"%d. %@",i+1,[[notesArr objectAtIndex:i]valueForKey:@"note"]];
//        CGFloat heightOfText = [self getTextHeightByWidth:noteStr textFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f] textWidth:scrollView.frame.size.width];
//
//
//
//        UITextView *textView = [[UITextView alloc]init ];
//        textView.userInteractionEnabled = YES;
//        textView.scrollEnabled =NO;
//        textView.selectable =YES;
//        textView.editable = NO;
//        textView.delegate = self;
//        textView.dataDetectorTypes = UIDataDetectorTypeLink;
//        textView.textAlignment = NSTextAlignmentJustified;
//
//        textView.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
//        textView.textColor = [UIColor darkGrayColor];
//        textView.frame = CGRectMake(0,finalheight, scrollView.frame.size.width, heightOfText+20);
//        textView.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//              // textView.backgroundColor = [UIColor clearColor];
//               textView.text = noteStr;
//         [scrollView addSubview:textView];
//
//        finalheight = finalheight + heightOfText+20;
//
//    }
//
   //  CGFloat heightOfText = [self getTextHeightByWidth:noteStr textFont:[UIFont fontWithName:@"Lato-Regular" size:14.0f] textWidth:scrollView.frame.size.width];
    
    
//    NSDictionary *newsDic = [[dataDic valueForKey:@"advice"]valueForKey:@"news"];
//    NSString *noteStr = [newsDic valueForKey:@"Content"];
//
//    if (noteStr.length !=0) {
//
//    UILabel * newsHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight+20, scrollView.frame.size.width, 35)];
//     newsHeadinglbl.text = @"  News";
//    // usaHeadinglbl.text =[countryArray objectAtIndex:i];
//     newsHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
//     newsHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
//     newsHeadinglbl.textColor = [UIColor whiteColor];
//     newsHeadinglbl.layer.cornerRadius = 4;
//     newsHeadinglbl.userInteractionEnabled= YES;
//      newsHeadinglbl.layer.masksToBounds = true;
//     [scrollView addSubview:newsHeadinglbl];
//
//
//    CGFloat heightOfText = [self getTextHeightByWidth:noteStr textFont:[UIFont fontWithName:@"Lato-Regular" size:14.0f] textWidth:scrollView.frame.size.width];
//
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData: [noteStr dataUsingEncoding:NSUnicodeStringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error: nil];
//
//    [attributedString addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:15.0f],NSForegroundColorAttributeName:[UIColor darkGrayColor]} range:NSMakeRange(0, attributedString.length)];
//
//           UITextView *textView = [[UITextView alloc]init ];
//             textView.editable = NO;
//             textView.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
//             textView.textColor = [UIColor darkGrayColor];
//             CGFloat HeightText;
//
//        if (heightOfText >1070) {
//            HeightText = heightOfText-90 ;
//
//        } else if ( heightOfText < 1069 && heightOfText >500){
//            HeightText = heightOfText-40 ;
//
//        }
//        else if ( heightOfText < 500){
//        HeightText = heightOfText ;
//
//        } else {
//
//            HeightText = heightOfText-60 ;
//
//        }
//
//           // HeightText = heightOfText-60 ;
//            textView.frame = CGRectMake(0,finalheight+55, scrollView.frame.size.width,HeightText);
//           //textView.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//            textView.textAlignment = NSTextAlignmentJustified;
//            textView.text = @"";
//            textView.attributedText = attributedString;
//            [scrollView addSubview:textView];
//            finalheight = finalheight+55 + HeightText;
//
//    }
    
    
//            UILabel * usaHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, finalheight+10, scrollView.frame.size.width, 35)];
//             usaHeadinglbl.text = @"  Detailed Stats";
//            // usaHeadinglbl.text =[countryArray objectAtIndex:i];
//             usaHeadinglbl.backgroundColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
//             usaHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
//             usaHeadinglbl.textColor = [UIColor whiteColor];
//             usaHeadinglbl.layer.cornerRadius = 4;
//             usaHeadinglbl.userInteractionEnabled= YES;
//              usaHeadinglbl.layer.masksToBounds = true;
//             [scrollView addSubview:usaHeadinglbl];
//
//    UILabel *grayBG = [[UILabel alloc]initWithFrame:CGRectMake(0,finalheight+55, scrollView.frame.size.width, 440)];
//    grayBG.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
//    [scrollView addSubview:grayBG];
//
//    finalheight = finalheight+55;
//
//    UILabel * Totallbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 250, 20)];
//     Totallbl.text = [NSString stringWithFormat:@"Total Cases: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"total_cases"]];
//    Totallbl.adjustsFontSizeToFitWidth = YES;
//
//     Totallbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     Totallbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:Totallbl];
//
//    UILabel * newlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 30, 250, 20)];
//        newlbl.text = [NSString stringWithFormat:@"New Cases: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"new_cases"]];
//    newlbl.adjustsFontSizeToFitWidth = YES;
//
//        newlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//        newlbl.textColor = [UIColor darkGrayColor];
//        [grayBG addSubview:newlbl];
//
//    UILabel * TotalDeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 55, 250, 20)];
//     TotalDeathlbl.text = [NSString stringWithFormat:@"Total Deaths: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"total_deaths"]];
//      TotalDeathlbl.adjustsFontSizeToFitWidth = YES;
//      TotalDeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//      TotalDeathlbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:TotalDeathlbl];
//
//    UILabel * newDeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, 250, 20)];
//        newDeathlbl.text = [NSString stringWithFormat:@"New Deaths: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"new_deaths"]];
//        newDeathlbl.adjustsFontSizeToFitWidth = YES;
//        newDeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//        newDeathlbl.textColor = [UIColor darkGrayColor];
//        [grayBG addSubview:newDeathlbl];
//
//
//    UILabel * TotalcaseMilbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 105, 280, 20)];
//     TotalcaseMilbl.text = [NSString stringWithFormat:@"Total cases per million: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"total_cases_per_million"]];
//    TotalcaseMilbl.adjustsFontSizeToFitWidth = YES;
//     TotalcaseMilbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     TotalcaseMilbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:TotalcaseMilbl];
//
//    UILabel * newDealbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 130, 280, 20)];
//        newDealbl.text = [NSString stringWithFormat:@"Total deaths per million: %.03f",[[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"total_deaths_per_million"]floatValue]];
//        newDealbl.adjustsFontSizeToFitWidth = YES;
//        newDealbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//        newDealbl.textColor = [UIColor darkGrayColor];
//        [grayBG addSubview:newDealbl];
//
//
//
//    UILabel * populationlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 155, 250, 20)];
//        populationlbl.text = [NSString stringWithFormat:@"Population: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"population"]];
//       populationlbl.adjustsFontSizeToFitWidth = YES;
//        populationlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//        populationlbl.textColor = [UIColor darkGrayColor];
//        [grayBG addSubview:populationlbl];
//
//    UILabel * populationlDenbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 180, 250, 20)];
//     populationlDenbl.text = [NSString stringWithFormat:@"Population density: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"population_density"]];
//    populationlDenbl.adjustsFontSizeToFitWidth = YES;
//     populationlDenbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     populationlDenbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:populationlDenbl];
//
//    UILabel * medianlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 205, 250, 20)];
//        medianlbl.text = [NSString stringWithFormat:@"Median age: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"median_age"]];
//       medianlbl.adjustsFontSizeToFitWidth = YES;
//        medianlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//        medianlbl.textColor = [UIColor darkGrayColor];
//        [grayBG addSubview:medianlbl];
//
//    UILabel * ageSixtylbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 230, 250, 20)];
//     ageSixtylbl.text = [NSString stringWithFormat:@"Aged 65 older: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"aged_65_older"]];
//    ageSixtylbl.adjustsFontSizeToFitWidth = YES;
//     ageSixtylbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     ageSixtylbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:ageSixtylbl];
//
//    UILabel * agedSeventylbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 255, 250, 20)];
//     agedSeventylbl.text = [NSString stringWithFormat:@"Aged 70 older: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"aged_70_older"]];
//    agedSeventylbl.adjustsFontSizeToFitWidth = YES;
//     agedSeventylbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     agedSeventylbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:agedSeventylbl];
//
//    UILabel * gdplbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 280, 250, 20)];
//     gdplbl.text = [NSString stringWithFormat:@"GDP per capita: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"gdp_per_capita"]];
//    gdplbl.adjustsFontSizeToFitWidth = YES;
//     gdplbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     gdplbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:gdplbl];
//
//    UILabel * coviddeathrateLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 305, 250, 20)];
//     coviddeathrateLbl.text = [NSString stringWithFormat:@"Covid death rate: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"cvd_death_rate"]];
//    coviddeathrateLbl.adjustsFontSizeToFitWidth = YES;
//     coviddeathrateLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     coviddeathrateLbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:coviddeathrateLbl];
//
//    UILabel * diabeticLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 330, 250, 20)];
//     diabeticLbl.text = [NSString stringWithFormat:@"Diabetes prevalence: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"diabetes_prevalence"]];
//    diabeticLbl.adjustsFontSizeToFitWidth = YES;
//     diabeticLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     diabeticLbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:diabeticLbl];
//
//    UILabel * handwashLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 355, 250, 20)];
//     handwashLbl.text = [NSString stringWithFormat:@"Handwashing facilities: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"handwashing_facilities"]];
//    handwashLbl.adjustsFontSizeToFitWidth = YES;
//     handwashLbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//     handwashLbl.textColor = [UIColor darkGrayColor];
//     [grayBG addSubview:handwashLbl];
//
//       UILabel * bedlbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 380, 250, 20)];
//           bedlbl.text = [NSString stringWithFormat:@"Hospital beds per thousand: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"hospital_beds_per_thousand"]];
//           bedlbl.adjustsFontSizeToFitWidth = YES;
//           bedlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//           bedlbl.textColor = [UIColor darkGrayColor];
//           [grayBG addSubview:bedlbl];
//
//    UILabel * countrylbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 405, 250, 20)];
//           countrylbl.text = [NSString stringWithFormat:@"Life expectancy: %@",[[dataDic valueForKey:@"covid19_stats"]valueForKey:@"life_expectancy"]];
//          countrylbl.adjustsFontSizeToFitWidth = YES;
//           countrylbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
//           countrylbl.textColor = [UIColor darkGrayColor];
//           [grayBG addSubview:countrylbl];
//
//
    
             scrollView.showsVerticalScrollIndicator = NO;
             scrollView.backgroundColor = [UIColor whiteColor];
              
            scrollView.contentSize = CGSizeMake(screenWidth-30, finalheight+90);
           [self.view addSubview:scrollView];
}



- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
     interaction:(UITextItemInteraction)interaction  {
    
    WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
    myView.webLink = URL.absoluteString;

    [self.navigationController pushViewController:myView animated:true];
    
    return false;
}


@end
