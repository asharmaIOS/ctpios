//
//  NotifDetailsVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 4/4/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "NotifDetailsVC.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "ValidationAndJsonVC.h"
#import "DemoMessagesViewController.h"

@interface NotifDetailsVC (){
    
    NSDictionary *completeDataDic ;
}
@property (weak, nonatomic) IBOutlet UILabel *LblAirlines;
@property (weak, nonatomic) IBOutlet UILabel *lbtNotifText;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrillView;

@end

@implementation NotifDetailsVC
@synthesize alertId,detailDic;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden: NO];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    

    [self setupLeftMenuButton];
    // Do any additional setup after loading the view.
    [self ListAlertDetails];
    

    _lbtNotifText.text = [[detailDic valueForKey:@"detail"]valueForKey:@"notificationText"];
    _lblTitle.text = [[detailDic valueForKey:@"detail"]valueForKey:@"notificationTitle"];
    _LblAirlines.text = [NSString stringWithFormat:@"Flight : %@-%@",[[[detailDic valueForKey:@"detail"]valueForKey:@"flight"] valueForKey:@"bookedAirlineCode"],[[[detailDic valueForKey:@"detail"]valueForKey:@"flight"] valueForKey:@"flightNumber"]];
    
    [self alertDetailsLoad:[[detailDic valueForKey:@"detail"]valueForKey:@"type"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    
    [super viewWillAppear:animated];
    
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    //    textButton.title = @"NOTIFICATION";
    //    // textButton.tintColor = [UIColor whiteColor];
    //    textButton.tintColor = [UIColor whiteColor];
    //    self.navigationItem.leftBarButtonItems = @[searchItem];
    //    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"NOTIFICATION DETAILS"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backAction
{
    
    [self backButtonTap:self];
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

-(void)backButtonTap:(id)sender
{
    
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
    // Do we need to push it into navigation controller?
    
}
#pragma mark -  method for LISTAlert

- (void)alertDetailsLoad :(NSString *)alertType {
    
    if ([alertType isEqualToString:@"Leg Departure Info"]) {
        

        [self drawViewForAlertDeparture:completeDataDic];

        
    }
    else  if ([alertType isEqualToString:@"Scheduled Departure Update"]) {
        
        
        
        [self drawViewForAlertDeparture:completeDataDic];

        
    }
    else  if ([alertType isEqualToString:@"Estimated Connection Problem"]) {
  
        [self drawViewForAlertdetails:completeDataDic];

        
    }
    else  if ([alertType isEqualToString:@"Connection Info"]) {

        
        [self drawViewForAlertdetails:completeDataDic];
    }
    else  if ([alertType isEqualToString:@"Connection Information Gate Change"]) {
        
        [self drawViewForAlertPreviouseAndCurrent:completeDataDic :@"gate"];

       // [self drawViewForAlertdetails:completeDataDic];
       // [self drawViewForAlertDeparture:completeDataDic];


        
    }
    else  if ([alertType isEqualToString:@"Leg Baggage Change"]) {

        //[self drawViewForAlertDeparture:completeDataDic];
        [self drawViewForAlertPreviouseAndCurrent:completeDataDic :@"baggage"];

    }
    
    //NSLog(@"alertType --%@",alertType);
}





-(void)drawViewForAlertPreviouseAndCurrent:(NSDictionary *)dataDic :(NSString *)typeOfAlert {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    int legIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"legIndex"]intValue];
    int flightIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"outboundFlightIndex"]intValue];

    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.lblTitle.frame.origin.y +self.lblTitle.frame.size.height+05 , screenWidth, 550)];
    
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(20, 20, screenWidth-40, 250)];
    backgroundView.layer.cornerRadius = 10;
    backgroundView.layer.masksToBounds = YES;
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *headingLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 05, backgroundView.frame.size.width-20, 30)];
    headingLbl.textAlignment = NSTextAlignmentCenter;
    headingLbl.text = @"FLIGHT" ;
    headingLbl.font = [UIFont systemFontOfSize:22];
    headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    
    UILabel *lineLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, backgroundView.frame.size.width-20, 0.5)];
    lineLbl.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *airportLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 250, 25)];
    
    NSDictionary *flightDic;
    
    if ([typeOfAlert isEqualToString:@"baggage"]) {
        flightIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"flightIndex"]intValue];
        flightDic = [[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:flightIndex];


    }
    else {
        
     flightDic = [[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:flightIndex];

    }
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Airport Code : %@",[[[[flightDic valueForKey:@"flightStatuses"]lastObject] valueForKey:@"departure"]valueForKey:@"airportCode"]]];
    
    if ([typeOfAlert isEqualToString:@"baggage"]) {
        text = [[NSMutableAttributedString alloc]
                initWithString:[NSString stringWithFormat:@"Airport Code : %@",[[[[flightDic valueForKey:@"flightStatuses"]lastObject] valueForKey:@"arrival"]valueForKey:@"airportCode"]]];
    }
    
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 15)];
    airportLbl.attributedText = text ;
    
    
    UILabel *flightLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 250, 25)];
    NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Flight : %@-%@",[flightDic valueForKey:@"bookedAirlineCode"],[[[flightDic valueForKey:@"flightStatuses"]lastObject] valueForKey:@"flightNumber"]]];
    [text1 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 9)];
    flightLbl.attributedText = text1 ;
    
    //
    
    NSString *dateStr = [self getDateTimeFromData: [[flightDic valueForKey:@"flightStatuses"]lastObject] keyForType:@"departure"];

    if ([typeOfAlert isEqualToString:@"baggage"]) {
         dateStr = [self getDateTimeFromData: [[flightDic valueForKey:@"flightStatuses"]lastObject] keyForType:@"arrival"];

    }
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *dte = [dateFormat dateFromString:dateStr];
    //Second Conversion
    [dateFormat setDateFormat: @"MM-dd-yyyy hh:mm a"];
    NSString *finalStr = [dateFormat stringFromDate:dte];
    
    UILabel *arrivalLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 120, 290, 25)];
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Departure : %@",finalStr]];
    [text2 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 12)];
    
    if ([typeOfAlert isEqualToString:@"baggage"]) {

        text2 = [[NSMutableAttributedString alloc]
                 initWithString:[NSString stringWithFormat:@"Arrival : %@",finalStr]];
        [text2 addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                      range:NSMakeRange(0, 9)];
    }
    
    arrivalLbl.attributedText = text2 ;
    if (text2.length!=0) {
     
        [backgroundView addSubview:arrivalLbl];

    }
    
    
   
    
    
    
    if ([[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"]valueForKey:typeOfAlert]) {
        
        int lenthOfStr1 = 10 + typeOfAlert.length ;

        UILabel *gateLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 155, 290, 25)];
        NSMutableAttributedString *tex4 = [[NSMutableAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"Previous %@ : %@",typeOfAlert,[[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"]valueForKey:typeOfAlert]valueForKey:@"previous"]]];
        [tex4 addAttribute:NSForegroundColorAttributeName
                     value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                     range:NSMakeRange(0, lenthOfStr1)];
        gateLbl1.attributedText = tex4 ;
        [backgroundView addSubview:gateLbl1];
        
        
        int lenthOfStr = 9 + typeOfAlert.length ;
        UILabel *currentLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 190, 290, 25)];
        NSMutableAttributedString *tex5 = [[NSMutableAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"Current %@ : %@",typeOfAlert,[[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"]valueForKey:typeOfAlert]valueForKey:@"current"]]];
        [tex5 addAttribute:NSForegroundColorAttributeName
                     value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                     range:NSMakeRange(0, lenthOfStr)];
        currentLbl.attributedText = tex5 ;
        [backgroundView addSubview:currentLbl];
        
        
        
        
        
    }
    
    
   
    
    
    
    [backgroundView addSubview:lineLbl];
    [backgroundView addSubview:headingLbl];
    [backgroundView addSubview:airportLbl];
    [backgroundView addSubview:flightLbl];
    //[backgroundView addSubview:arrivalLbl];
    
    [scroll addSubview:backgroundView];
    
    [self.view addSubview:scroll];
    
}




-(void)drawViewForAlertDeparture :(NSDictionary *)dataDic {

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    int legIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"legIndex"]intValue];
    int flightIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"flightIndex"]intValue];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.lblTitle.frame.origin.y +self.lblTitle.frame.size.height+05 , screenWidth, 550)];

    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(20, 20, screenWidth-40, 200)];
    backgroundView.layer.cornerRadius = 10;
    backgroundView.layer.masksToBounds = YES;
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *headingLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 05, backgroundView.frame.size.width-20, 30)];
    headingLbl.textAlignment = NSTextAlignmentCenter;
    headingLbl.text = @"FLIGHT" ;
    headingLbl.font = [UIFont systemFontOfSize:22];
    headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    
    UILabel *lineLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, backgroundView.frame.size.width-20, 0.5)];
    lineLbl.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *airportLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 250, 25)];
    
    NSDictionary *flightDic = [[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:flightIndex];
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Airport Code : %@",[[[[flightDic valueForKey:@"flightStatuses"]lastObject] valueForKey:@"departure"]valueForKey:@"airportCode"]]];
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 15)];
    airportLbl.attributedText = text ;
    
    
    UILabel *flightLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 250, 25)];
    NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Flight : %@-%@",[flightDic valueForKey:@"bookedAirlineCode"],[[[flightDic valueForKey:@"flightStatuses"]lastObject] valueForKey:@"flightNumber"]]];
    [text1 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 9)];
    flightLbl.attributedText = text1 ;
    
    //
    
   
    
    NSString *dateStr = [self getDateTimeFromData: [[flightDic valueForKey:@"flightStatuses"]lastObject] keyForType:@"departure"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *dte = [dateFormat dateFromString:dateStr];
    //Second Conversion
    [dateFormat setDateFormat: @"MM-dd-yyyy hh:mm a"];
    NSString *finalStr = [dateFormat stringFromDate:dte];
    
    UILabel *arrivalLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 120, 290, 25)];
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Departure : %@",finalStr]];
    [text2 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 12)];
    arrivalLbl.attributedText = text2 ;
    
    
    if ([[[[flightDic valueForKey:@"flightStatuses"]lastObject]valueForKey:@"departure"]valueForKey:@"gate"]) {
        UILabel *gateLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 155, 290, 25)];
        NSMutableAttributedString *tex4 = [[NSMutableAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"Gate : %@",[[[[flightDic valueForKey:@"flightStatuses"]lastObject]valueForKey:@"departure"]valueForKey:@"gate"]]];
        [tex4 addAttribute:NSForegroundColorAttributeName
                     value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                     range:NSMakeRange(0, 6)];
        gateLbl1.attributedText = tex4 ;
        [backgroundView addSubview:gateLbl1];
        
    }
    
    
    
    [backgroundView addSubview:lineLbl];
    [backgroundView addSubview:headingLbl];
    [backgroundView addSubview:airportLbl];
    [backgroundView addSubview:flightLbl];
    [backgroundView addSubview:arrivalLbl];
    
    [scroll addSubview:backgroundView];
    
    [self.view addSubview:scroll];
    
}

-(void)drawViewForAlertdetails :(NSDictionary *)dataDic {
   
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    int legIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"legIndex"]intValue];
    int inboundIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"inboundFlightIndex"]intValue];
    int outboundIndex = [[[[completeDataDic valueForKey:@"data"]valueForKey:@"alertDetails"] valueForKey:@"outboundFlightIndex"]intValue];
    
    
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.lblTitle.frame.origin.y +self.lblTitle.frame.size.height+05 , screenWidth, 550)];
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(20, 10, screenWidth-40, 235)];
    backgroundView.layer.cornerRadius = 10;
    backgroundView.layer.masksToBounds = YES;
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *headingLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 05, backgroundView.frame.size.width-20, 30)];
    headingLbl.textAlignment = NSTextAlignmentCenter;
    headingLbl.text = @"ARRIVING FLIGHT" ;
    headingLbl.font = [UIFont systemFontOfSize:22];
    headingLbl.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    
    UILabel *lineLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, backgroundView.frame.size.width-20, 0.5)];
    lineLbl.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *airportLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 250, 25)];
    
    NSDictionary *flightDicInbound = [[[[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex: inboundIndex ]valueForKey:@"flightStatuses"]lastObject ];
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Airport Code : %@",[[flightDicInbound valueForKey:@"arrival"]valueForKey:@"airportCode"]]];
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 15)];
    airportLbl.attributedText = text ;
    
    
    UILabel *flightLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 250, 25)];
    NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Flight : %@-%@",[[[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:inboundIndex]  valueForKey:@"bookedAirlineCode"],[flightDicInbound valueForKey:@"flightNumber"]]];
    [text1 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 9)];
    flightLbl.attributedText = text1 ;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss"];
    
    

    //NSDate *dte = [dateFormat dateFromString: [[flightDicInbound valueForKey:@"arrival"]valueForKey:@"scheduledGateDateTime"]];
    
    NSDate *dte = [dateFormat dateFromString: [self getDateTimeFromData:flightDicInbound keyForType:@"arrival"]];
    //Second Conversion
    [dateFormat setDateFormat: @"MM-dd-yyyy hh:mm a"];
    NSString *finalStr = [dateFormat stringFromDate:dte];
    
    UILabel *arrivalLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 120, 290, 25)];
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc]
                                        initWithString:[NSString stringWithFormat:@"Arrival : %@",finalStr]];
    [text2 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                  range:NSMakeRange(0, 10)];
    arrivalLbl.attributedText = text2 ;
    
    
    UILabel *gateLbl;
    if ([[flightDicInbound valueForKey:@"arrival"]valueForKey:@"gate"]) {
        gateLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 155, 290, 25)];
        NSMutableAttributedString *text3 = [[NSMutableAttributedString alloc]
                                            initWithString:[NSString stringWithFormat:@"Gate : %@",[[flightDicInbound valueForKey:@"arrival"]valueForKey:@"gate"]]];
        [text3 addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                      range:NSMakeRange(0, 6)];
        gateLbl.attributedText = text3 ;
        [backgroundView addSubview:gateLbl];

    }
    
    
    
    UILabel *terminalLbl;
    if ([[flightDicInbound valueForKey:@"arrival"]valueForKey:@"terminal"]) {
        terminalLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 190, 290, 25)];
        NSMutableAttributedString *text4 = [[NSMutableAttributedString alloc]
                                            initWithString:[NSString stringWithFormat:@"Terminal : %@",[[flightDicInbound valueForKey:@"arrival"]valueForKey:@"terminal"]]];
        [text4 addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                      range:NSMakeRange(0, 11)];
        terminalLbl.attributedText = text4 ;
        [backgroundView addSubview:terminalLbl];

    }
    
    
    
    
    [backgroundView addSubview:lineLbl];
    [backgroundView addSubview:headingLbl];
    [backgroundView addSubview:airportLbl];
    [backgroundView addSubview:flightLbl];
    [backgroundView addSubview:arrivalLbl];
    
    /// outbound view design
    
    
    UIView *backgroundView1 = [[UIView alloc]initWithFrame:CGRectMake(20, backgroundView.frame.origin.y + backgroundView.frame.size.height+10 , screenWidth-40, 235)];
    backgroundView1.layer.cornerRadius = 10;
    backgroundView1.layer.masksToBounds = YES;
    backgroundView1.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *headingLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 05, backgroundView1.frame.size.width-20, 30)];
    headingLbl1.textAlignment = NSTextAlignmentCenter;
    headingLbl1.text = @"CONNECTING FLIGHT" ;
    headingLbl1.font = [UIFont systemFontOfSize:22];
    headingLbl1.textColor = [UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0];
    
    UILabel *lineLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, backgroundView1.frame.size.width-20, 0.5)];
    lineLbl1.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *airportLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 250, 25)];
    
    NSDictionary *flightDicInbound1 = [[[[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:outboundIndex ]valueForKey:@"flightStatuses"]lastObject ];
    
    
    NSMutableAttributedString *tex1 = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Airport Code : %@",[[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"airportCode"]]];
    [tex1 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 15)];
    airportLbl1.attributedText = tex1 ;
    
    
    UILabel *flightLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 250, 25)];
    NSMutableAttributedString *tex2 = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Flight : %@-%@",[[[[[[[completeDataDic valueForKey:@"data"]valueForKey:@"trip"]valueForKey:@"legs"]objectAtIndex:legIndex] valueForKey:@"flights"]objectAtIndex:outboundIndex  ] valueForKey:@"bookedAirlineCode"],[flightDicInbound1 valueForKey:@"flightNumber"]]];
    [tex2 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 9)];
    flightLbl1.attributedText = tex2 ;
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss"];
   // NSDate *dte1 = [dateFormat1 dateFromString: [[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"scheduledGateDateTime"]];
    
    NSDate *dte1 = [dateFormat1 dateFromString: [self getDateTimeFromData:flightDicInbound1 keyForType:@"departure"]];

    //Second Conversion
    [dateFormat1 setDateFormat: @"MM-dd-yyyy hh:mm a"];
    NSString *finalStr1 = [dateFormat1 stringFromDate:dte1];
    
    UILabel *arrivalLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 120, 290, 25)];
    NSMutableAttributedString *tex3 = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Departure : %@",finalStr1]];
    [tex3 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 12)];
    arrivalLbl1.attributedText = tex3 ;
    
    
    
     if ([[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"gate"]) {
    UILabel *gateLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 155, 290, 25)];
    NSMutableAttributedString *tex4 = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Gate : %@",[[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"gate"]]];
    [tex4 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 6)];
    gateLbl1.attributedText = tex4 ;
         [backgroundView1 addSubview:gateLbl1];

     }
    
     if ([[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"terminal"]) {
    UILabel *terminalLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 190, 290, 25)];
    NSMutableAttributedString *tex5 = [[NSMutableAttributedString alloc]
                                       initWithString:[NSString stringWithFormat:@"Terminal : %@",[[flightDicInbound1 valueForKey:@"departure"]valueForKey:@"terminal"]]];
    [tex5 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]
                 range:NSMakeRange(0, 11)];
    terminalLbl1.attributedText = tex5 ;
    [backgroundView1 addSubview:terminalLbl1];

     }
    
    [backgroundView1 addSubview:lineLbl1];
    [backgroundView1 addSubview:headingLbl1];
    [backgroundView1 addSubview:airportLbl1];
    [backgroundView1 addSubview:flightLbl1];
    [backgroundView1 addSubview:arrivalLbl1];
    
    
    [scroll addSubview:backgroundView1];
    [scroll addSubview:backgroundView];
    
    scroll.contentSize = CGSizeMake(screenWidth, 600);
    
    [self.view addSubview:scroll];
    
}


- (NSString *)getDateTimeFromData:(NSDictionary *)dataDic keyForType:(NSString *)TripType{
    NSString *dateStr ;
    
    
    if ([[dataDic  valueForKey:TripType]valueForKey:@"actualGateDateTime"]) {
        
        dateStr = [[dataDic  valueForKey:TripType]valueForKey:@"actualGateDateTime"];
    }
    
//    else if ([[dataDic valueForKey:TripType]valueForKey:@"estimatedGateDateTime"])  {
//        
//        dateStr = [[dataDic valueForKey:TripType]valueForKey:@"estimatedGateDateTime"];
//        
//        
//    }
    else if ([[dataDic  valueForKey:TripType]valueForKey:@"scheduledGateDateTime"]) {
        
        dateStr = [[dataDic  valueForKey:TripType]valueForKey:@"scheduledGateDateTime"];
        
        
    }
    

    return dateStr;
}




- (void)ListAlertDetails {
    
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
         [ValidationAndJsonVC displayAlert:@"No internet connected.!" HeaderMsg:@"OOPS"];
       
    }
    
    
    else {
        
        // [listArray removeAllObjects];
        
        //    [_totalArrayPky removeAllObjects];
        
        [SVProgressHUD showWithStatus:@"Trip details.." maskType:SVProgressHUDMaskTypeBlack];
        // self.emptyview.hidden = YES;
        
        NSDictionary *dictionary = @{@"alertID":[detailDic valueForKey:@"alertID"]};
                
        NSString *urlStr = @"api/v1/alert/get";
        completeDataDic = [ValidationAndJsonVC getParsingData:dictionary GetUrl:urlStr GetTimeinterval:30];
        
        
        
        
        
        if  ( completeDataDic.allKeys == nil) {
            
            
            [ValidationAndJsonVC displayAlert:@"Server problem.!" HeaderMsg:@"OOPS"];

            //  self.emptyview.hidden = NO;
            //  self.NoTriplbl.text = @"You Dont Have Any Upcoming Trip Yet";
            //  NSLog(@"pnr%@",listArray);
            
            [SVProgressHUD dismiss];
            
        }
        
        [SVProgressHUD dismiss];

    }
    
}


@end
