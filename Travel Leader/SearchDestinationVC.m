//
//  SearchDestinationVC.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 9/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "SearchDestinationVC.h"
#import "XMLReader.h"
#import "TravelConstantClass.h"
#import "SVProgressHUD.h"
#import "CityCell.h"
#import "CityInfoBySearchVC.h"
#import "HomeViewController.h"
#import "DemoMessagesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>




@interface SearchDestinationVC ()<UISearchBarDelegate,UISearchResultsUpdating,UISearchControllerDelegate>
{
    NSMutableArray* cityarr;
    NSArray *finalData;
    NSArray *fillerarr;
    
    
}

@property (nonatomic, strong) UISearchController *searchController;

//@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UIView *searchBgView;

@property (weak, nonatomic) IBOutlet UITableView *citytabel;
@property (nonatomic, strong) NSMutableArray *searchResult;
@end

@implementation SearchDestinationVC

- (void)viewDidLoad {
   // _searchbar.delegate = self;
    
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden: NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];
    
    cityarr = [[NSMutableArray alloc]init];
     fillerarr = [[NSMutableArray alloc]init];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];

    // Use the current view controller to update the search results.
     //_searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    CGRect newFrame = _searchBgView.frame;

    newFrame.size.width = screenWidth;
    [self.searchBgView setFrame:newFrame];
    
    self.searchController.searchBar.frame = CGRectMake(0,0, _searchBgView.frame.size.width, 48.0);
    [_searchBgView addSubview:[_searchController searchBar]];
    _searchController.obscuresBackgroundDuringPresentation = false; // The default is true.

   // self.citytabel.tableHeaderView = self.searchController.searchBar;
    //_searchController.searchBar.searchTextField.backgroundColor = [UIColor clearColor];
   // _searchController.searchBar.barTintColor = [UIColor clearColor];
    _searchController.delegate = self;
    _searchController.searchBar.searchTextField.placeholder = @"Explore Destination";
    _searchController.searchBar.delegate = self;
    self.definesPresentationContext = YES;
    
         CGFloat screenHeight = screenRect.size.height;
        
        
        UIScrollView *scroll;
        if (screenHeight == 926) {
        _citytabel.frame = CGRectMake(0, 320, screenWidth, 256 );
            
            
    }
        else if(screenHeight > 844 && screenHeight < 926) {
            
            _citytabel.frame = CGRectMake(0, 308, screenWidth, 256 );

        }
        
        else if(screenHeight == 812 || screenHeight == 844 ) {
            
            _citytabel.frame = CGRectMake(0, 290, screenWidth, 256 );

        }
    
    else if(screenHeight == 736) {
        
        _citytabel.frame = CGRectMake(0, 257, screenWidth, 256 );

    }
           else {
               _citytabel.frame = CGRectMake(0, 235, screenWidth, 256 );
           }
    
    
    
    
    
    [self setupLeftMenuButton];
    _citytabel.hidden = YES;
    [self GETCity];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    
    _citytabel.hidden = YES;
    _searchController.searchBar.text = @"";
    [_searchController.searchBar resignFirstResponder];
   
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    // [self GETCity];
    
}
#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
//    textButton.title = @"SEARCH";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItems = @[searchItem];
//    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"SEARCH DESTINATION"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}


-(void)backAction
{
    
    [self backButtonTapp:self];
}
-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)backButtonTapp:(id)sender {
    
   // [self dismissViewControllerAnimated:YES completion:nil] ;
    
    HomeViewController* home = nil;
//    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
//        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
//        if([vc isKindOfClass:[HomeViewController class]]) {
//            home = (HomeViewController*)vc;
//        }
//    }
    
    
    home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
             
             [self.navigationController pushViewController:home animated:NO];
    
    
    if (home == nil) {
        // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    // [self dismissViewControllerAnimated:YES completion:nil] ;
    
    
}





-(void)GETCity

{
  //  dispatch_async(dispatch_get_main_queue(), ^{

     [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
    dispatch_async(dispatch_get_main_queue(), ^{

    NSURL *url = [NSURL URLWithString:@"https:api.arrivalguides.com/api/xml/GetAllDestinations?auth=057bbab0770837e2364596fb21d6cf0e35ac9e44&v=13"];
    NSData *xmlData = [[NSData alloc] initWithContentsOfURL:url];
         NSError *parseError = nil;
    
            //  [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:xmlData error:&parseError];
    NSLog(@"%@", xmlDictionary);
    
    cityarr = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
   // NSLog(@"%@",filterarr);
   finalData = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
        fillerarr = [finalData valueForKey:@"iso"];
   // NSLog(@"%@",arr);
   // dispatch_async(dispatch_get_main_queue(), ^{
        [_citytabel reloadData];\
        [SVProgressHUD dismiss];
        
        
        if ([[NSString stringWithFormat:@"%@", [[[xmlDictionary valueForKey:@"Errors"] valueForKey:@"ErrorDescription"] objectAtIndex:0]] isEqualToString:@"No Data Found!"]) {
            //xmlDictionary = nil;
        }
        
        
    });
    

    
}
#pragma mark - Search Bar
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _citytabel.hidden = NO;
    if([searchText length]>0){
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"iso CONTAINS[cd] %@",searchText];
       // searchBar.showsCancelButton=TRUE;
        NSArray *arraySearchedData=[finalData filteredArrayUsingPredicate:predicate];
         NSLog( @"searchdata%@",arraySearchedData);
        cityarr=[[NSMutableArray alloc]initWithArray:arraySearchedData];
       
        [_citytabel reloadData];
        
}
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    cityarr=[[NSMutableArray alloc]initWithArray:finalData];
      NSLog( @"city%@",cityarr);
    NSLog( @"Final%@",finalData);
  
    [_citytabel reloadData];
   

   // [_searchController.searchBar resignFirstResponder];
    _citytabel.hidden = YES;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
       CGFloat screenWidth = screenRect.size.width;
//    _searchBgView.frame = CGRectMake(50, 165,screenWidth-100 , 40);
//    self.searchController.searchBar.frame = CGRectMake(0,0, _searchBgView.frame.size.width, 44.0);


  //  self.searchController.searchBar.frame = CGRectMake(0,0, _searchBgView.frame.size.width, 44.0);


}

- (IBAction)popularCityBtnClick:(id)sender  {
    
    UIButton* btn = (UIButton*)sender;
    
   // CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
   // CityInfoBySearchVC *vc  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
    CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];

    
     myView.strCity = btn.titleLabel.text;
      [self.navigationController pushViewController:myView animated:YES];

}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cityarr.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CityCell";
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *cityDict = cityarr[indexPath.row];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[cityDict  valueForKey:@"iso"]];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor whiteColor];
   // cell.citylbl.text = [NSString stringWithFormat:@"%@",[cityDict  valueForKey:@"iso"]];
    
    return  cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSDictionary *dataDict = cityarr[indexPath.row];
    
    [TravelConstantClass singleton].City = [dataDict valueForKey:@"iso"];
    
   NSLog(@"pkey%@",[TravelConstantClass singleton].City);
     //  CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
    
    
    CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];
    
    
   // CityInfoBySearchVC *myView  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
    myView.strCity = [dataDict valueForKey:@"iso"];
   

    [self.navigationController pushViewController:myView animated:YES];
    // [_searchController.searchBar resignFirstResponder];
    //_citytabel.hidden = YES;

    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
}
@end
