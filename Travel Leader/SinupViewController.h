//
//  SinupViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinupViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *firstnametxt;
@property (weak, nonatomic) IBOutlet UITextField *lastnametxt;
@property (weak, nonatomic) IBOutlet UITextField *suffixtxt;
@property (weak, nonatomic) IBOutlet UITextField *emailtxt;
@property (weak, nonatomic) IBOutlet UITextField *phonetxt;
@property (weak, nonatomic) IBOutlet UITextField *companytxt;
@property (weak, nonatomic) IBOutlet UITextField *Accounttxt;
@property (weak, nonatomic) IBOutlet UITextField *agencytxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordtxt;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *viewsinup;
@property (strong, nonatomic) NSDictionary *dataDict;
@property (strong, nonatomic) NSString *emailStr;
@property (strong, nonatomic) NSMutableArray *nameArray;

- (IBAction)donebutton:(id)sender;
@end
