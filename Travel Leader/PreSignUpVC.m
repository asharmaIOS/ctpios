//
//  PreSignUpVC.m
//  Travel Leader
//
//  Created by wFares Dev on 7/8/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

#import "PreSignUpVC.h"
#import "SVProgressHUD.h"
#import "ValidationAndJsonVC.h"
#import "SinupViewController.h"


@interface PreSignUpVC ()<UITextFieldDelegate>{
 
    NSMutableArray *listArray;
}

@end

@implementation PreSignUpVC

- (void)viewDidLoad {
    

    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.0 green:121/255.0 blue:193/255.0 alpha:1.0]];

    UITapGestureRecognizer* tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [tapBackground setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapBackground];
    
    
    [self setupLeftMenuButton];

     listArray= [[NSMutableArray alloc]init];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

-(IBAction)dismissKeyboard1:(id)sender
{
    [self.view endEditing:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}


-(BOOL)SinupValidation
{
    UIAlertView *alert;
    NSString *regex1 =@"\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    
    if (_firstnameTxt.text.length == 0)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your First name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return false;
        
    }
    
    else if (_lastnameTxt.text.length == 0)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your last name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return false;
        
    }
    
    else if (_emailtxt.text == nil || [_emailtxt.text length] == 0 || [[_emailtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Email Address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return false;
        
    }
    else if (![predicate1 evaluateWithObject:_emailtxt.text] == YES)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your valid Email Address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return false;

    }
    
    else if (_recordLocatortxt.text.length == 0)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter record locator" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return false;

    }
    else {
        
        return true;
    }
}


-(IBAction)getPnr:(id)sender{
    
    if ([self SinupValidation]) {
        
        
        [self getListPnr];
    }
    
    
}


#pragma mark -  method for GetlistPNR


-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"SEARCH PNR"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:NO];

  
}


-(NSMutableArray *)nameOfpassangesr :(NSDictionary *)dataDict{
    NSMutableArray *finalpassangersArray = [[NSMutableArray alloc]init ] ;
    
     NSArray *dateArray = [dataDict valueForKey:@"name"];
    
    for (int i =0; i<dateArray.count; i++) {
        
        NSDictionary *dataDic = [dateArray objectAtIndex:i];
        [finalpassangersArray addObject:[dataDic valueForKey:@"fullname"]];
        
    }
    
    
    return finalpassangersArray;
}


- (void)getListPnr {
    
        

    [SVProgressHUD showWithStatus:@"Your Trips.." maskType:SVProgressHUDMaskTypeBlack];
    
    NSString *namestr;
    if (_suffixTxt.text.length==0) {
        
          namestr = [NSString stringWithFormat:@"%@/%@",_lastnameTxt.text,_firstnameTxt.text];
    }
    
    else {
        
        namestr = [NSString stringWithFormat:@"%@/%@ %@",_lastnameTxt.text,_firstnameTxt.text,_suffixTxt.text];
    }
    
    NSString *emailString = [_emailtxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *recordString = [_recordLocatortxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];


        
        NSDictionary *dictionary = @{@"name":namestr, @"email":emailString, @"recordlocator":recordString};
        
        NSString *urlStr = @"ChatGetPNR";
        
        NSDictionary *dataDic = [ValidationAndJsonVC getParsingData:dictionary GetUrl:urlStr GetTimeinterval:30];
        
        
        
        if (dataDic.allKeys != nil) {
            // [SVProgressHUD dismiss];
            if ([dataDic valueForKey:@"fault"]) {
                
                 [ValidationAndJsonVC displayAlert:[dataDic valueForKey:@"fault"] HeaderMsg:@"OOPS"];

            }
            
            
            
            NSDictionary *dataDictionary = [dataDic valueForKey :@"pnr"];
            
            
            
    
         [SVProgressHUD dismiss];
         
            
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                  if (dataDictionary.allKeys != nil) {
                      
                      NSString *reservationStr = [dataDictionary valueForKey:@"recordlocator"];
                       NSString *startDateStr = [dataDictionary valueForKey:@"itinstart"];
                       NSString *enddateStr = [dataDictionary valueForKey:@"itinend"];
                       NSString *destinationStr = [dataDictionary valueForKey:@"destination"];
                      
//                      NSArray *nameArray = [ [[[dataDictionary valueForKey:@"name"]objectAtIndex:0]valueForKey:@"fullname"] componentsSeparatedByString:@"/"];
//                      NSString *firstStr = [nameArray lastObject];
//                      NSString *lastStr = [nameArray firstObject];
                      
                      
                 NSArray *dataA =  [self nameOfpassangesr:dataDictionary] ;
                      
                    //  NSArray *dataA =  [dataDictionary valueForKey:@"name"];

                      NSString * uniquenamestr;
                      NSArray * uniqueStates;
                      if (dataA.count !=0) {
                          
                        uniqueStates = [dataA valueForKeyPath:@"@distinctUnionOfObjects.self"];
                        uniquenamestr = [[[uniqueStates valueForKey:@"description"] componentsJoinedByString:@","] capitalizedString];
                      }
                      
            

                      NSString *msgStr;
                    
                      if (uniquenamestr.length == 0) {
                          
                           msgStr = [NSString stringWithFormat:@"Destination - %@      Startdate - %@,  Enddate - %@",destinationStr,startDateStr,enddateStr];
                          
                      } else {
                           msgStr = [NSString stringWithFormat:@"Passenger Name - %@, Destination - %@      Startdate - %@,  Enddate - %@",uniquenamestr,destinationStr,startDateStr,enddateStr];
                      
                      }
                      
                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:msgStr message:@"Please confirm your PNR details." preferredStyle:UIAlertControllerStyleAlert];
                      [alertController addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                  {
                                                     
                                                      SinupViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SinupViewController"];
                                                      myView.dataDict = dataDictionary;
                          NSString *emailStr = [_emailtxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];

                          
                                                      myView.emailStr = emailStr;
                                                      if (_suffixTxt.text==0)
                                                      {
                                                          myView.nameArray = [[NSMutableArray alloc]initWithObjects:_firstnameTxt.text,_lastnameTxt.text, nil];

                                                          
                                                      }
                                                      else {
                                                          
                                                          myView.nameArray = [[NSMutableArray alloc]initWithObjects:_firstnameTxt.text,_lastnameTxt.text,_suffixTxt.text, nil];

                                                      }
                                                      
                                                      [self.navigationController pushViewController:myView animated:NO];
                                                   
                                                      
                                                      
                                                  }
                                                  ]];
                      [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                          
                          // action 2
                      }]];
                      
                      [self presentViewController:alertController animated:YES completion:nil];

                      
                      
                      
                      
                      
                      
                      
                  }
                
                
               // [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];

                [SVProgressHUD dismiss];
                
                
            });
            
        }
        
          [SVProgressHUD dismiss];
    
    }
    




- (IBAction)textFieldDidBeginEditingMaintainPosition:(UITextField *)textField {
    
    [UIView animateWithDuration:0.50
                     animations:^{
        CGRect frame= self.view.frame;
                         frame.origin.y-=170;
                         self.view.frame=frame;
                         
                     }];
}

- (IBAction)textFieldShouldReturnMaintainPosition:(UITextField *)textField {
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= self.view.frame;
                         frame.origin.y+=170;
                         self.view.frame=frame;
                         
                     }];
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framePromo=self.view.frame;
            framePromo.origin.y+=320;
            self.view.frame=framePromo;
        }];
    }
    return [self isEditing];;
}

@end
